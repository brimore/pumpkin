<?php

namespace Database\Factories;

use App\Domain\AudienceList\Model\AudienceList;
use Illuminate\Database\Eloquent\Factories\Factory;

class AudienceListFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AudienceList::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => [
                'en' => $this->faker->unique()->name(),
                'ar' => $this->faker->unique()->name()
            ],
            'config' => [
                [
                    "type" => "filter",
                    "event" => "SIGNED_UP",
                    "from" => null,
                    "to" => null,
                    "conditions" => [
                        [
                            "type" => "condition",
                            "attribute" => "gender",
                            "operator" => "=",
                            "value" => "female"
                        ],
                        [
                            "type" => "logical-operator",
                            "value" => "AND"
                        ],
                        [
                            "type" => "condition",
                            "attribute" => "age",
                            "operator" => ">",
                            "value" => 30
                        ]
                    ]
                ],
                [
                    "type" => "logical-operator",
                    "value" => "AND"
                ],
                [
                    "type" => "filter-group",
                    "config" => [
                        [
                            "type" => "filter",
                            "event" => "PLACED_ORDER",
                            "from" => null,
                            "to" => null,
                            "conditions" => [
                                [
                                    "type" => "condition",
                                    "attribute" => "amount",
                                    "operator" => ">",
                                    "value" => 500
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }
}
