<?php

namespace Database\Factories;

use App\Domain\Offer\Model\Offer;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class OfferFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Offer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'code' => Str::random(4),
            'title' => $this->faker->sentence,
            'starts_at' => now()->addDays($this->faker->numberBetween(1, 5)),
            'ends_at' => now()->addDays($this->faker->numberBetween(6, 30)),
            'is_active' => $this->faker->boolean
        ];
    }
}
