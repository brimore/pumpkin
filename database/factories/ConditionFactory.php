<?php

namespace Database\Factories;

use App\Domain\Condition\Model\Condition;
use Illuminate\Database\Eloquent\Factories\Factory;
use JetBrains\PhpStorm\ArrayShape;

class ConditionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Condition::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    #[ArrayShape(['criteria' => "array"])] public function definition(): array
    {
        return [
            'criteria' => [
                [
                    "type" => "achieve_points",
                    "amount" => [
                        "max" => $this->faker->numberBetween(400, 1000),
                        "min" => $this->faker->numberBetween(100, 399)
                    ],
                    "placing" => [
                        "type" => "per_order",
                        "start_date" => null,
                        "end_date" => null
                    ],
                    "SKUs" => null
                ],
                [
                    "type" => "logical-operator",
                    "operator" => $this->faker->randomElement(['AND', 'OR'])
                ],
                [
                    "type" => "buy_SKU",
                    "quantity" => $this->faker->numberBetween(1, 10),
                    "bundle" => $this->faker->boolean,
                    "list_type" => "SKUs",
                    "list_values" => []
                ]
            ]
        ];
    }
}
