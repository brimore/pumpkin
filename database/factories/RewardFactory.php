<?php

namespace Database\Factories;

use App\Domain\Reward\Model\Reward;
use Illuminate\Database\Eloquent\Factories\Factory;
use JetBrains\PhpStorm\ArrayShape;

class RewardFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Reward::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    #[ArrayShape(['name' => "array",'filters' => "array"])] public function definition(): array
    {
        return [
            'name' => [
                'en' => $this->faker->unique()->name(),
                'ar' => $this->faker->unique()->name()
            ],
            'filters' => null
        ];
    }
}
