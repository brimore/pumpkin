<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberOfferConsumptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_offer_consumption', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('member_id');
            $table->char('offer_id', 36);
            $table->unsignedBigInteger('order_id');
            $table->integer('status');
            $table->integer('quantity');
            $table->double('total_points')->default(0);
            $table->double('total_price')->default(0);
            $table->timestamps();

            $table->foreign('member_id')->on('members')->references('id')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_offer_consumption');
    }
}
