<?php

use App\Domain\AccumulationGroup\Model\AccumulationGroup;
use Illuminate\Database\Migrations\Migration;

class AddProgramGiftAccumulationGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        AccumulationGroup::query()->create(['name' => 'Program Gift']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        AccumulationGroup::query()->where('name', 'Program Gift')->delete();
    }
}
