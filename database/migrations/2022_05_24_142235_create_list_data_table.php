<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_data', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sheet_id');
            $table->unsignedBigInteger('offer_id');
            $table->unsignedBigInteger('module_id');
            $table->json('extra_attributes')->nullable();
            $table->enum('module_type', ['variant', 'member'])->default('member');
            $table->timestamps();


            $table->unique(['offer_id', 'module_id', 'module_type']);
            $table->foreign('sheet_id')->references('id')->on('sheets')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('offer_id')->references('id')->on('offers')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_data');
    }
}
