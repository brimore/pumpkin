<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddGeneratedColumnsToEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            ALTER TABLE events
            ADD COLUMN order_source varchar(255)
            GENERATED ALWAYS AS ( JSON_VALUE(attributes, '$.source') ) VIRTUAL AFTER attributes;
        ");

        DB::statement("
            CREATE INDEX events_order_source_index
            ON events(order_source ASC);
        ");

        DB::statement("
            ALTER TABLE events
            ADD COLUMN order_creation_date varchar(255)
            GENERATED ALWAYS AS ( JSON_VALUE(attributes, '$.created_at') ) VIRTUAL AFTER order_source;
        ");

        DB::statement("
            CREATE INDEX events_order_creation_date_index
            ON events(order_creation_date ASC);
        ");

        DB::statement("
            ALTER TABLE events
            ADD COLUMN order_total_price double
            GENERATED ALWAYS AS ( JSON_VALUE(attributes, '$.total_price') ) VIRTUAL AFTER order_creation_date;
        ");

        DB::statement("
            CREATE INDEX events_order_total_price_index
            ON events(order_total_price ASC);
        ");

        DB::statement("
            ALTER TABLE events
            ADD COLUMN order_total_points double
            GENERATED ALWAYS AS ( JSON_VALUE(attributes, '$.total_points') ) VIRTUAL AFTER order_total_price;
        ");

        DB::statement("
            CREATE INDEX events_order_total_points_index
            ON events(order_total_points ASC);
        ");

        DB::statement("
            ALTER TABLE events
            ADD COLUMN member_age int
            GENERATED ALWAYS AS ( JSON_VALUE(attributes, '$.age') ) VIRTUAL AFTER order_total_points;
        ");

        DB::statement("
            CREATE INDEX events_member_age_index
            ON events(member_age ASC);
        ");

        DB::statement("
            ALTER TABLE events
            ADD COLUMN member_gender varchar(255)
            GENERATED ALWAYS AS ( JSON_VALUE(attributes, '$.gender') ) VIRTUAL AFTER member_age;
        ");

        DB::statement("
            CREATE INDEX events_member_gender_index
            ON events(member_gender ASC);
        ");

        DB::statement("
            ALTER TABLE events
            ADD COLUMN member_creation_date varchar(255)
            GENERATED ALWAYS AS ( JSON_VALUE(attributes, '$.created_at') ) VIRTUAL AFTER member_gender;
        ");

        DB::statement("
            CREATE INDEX events_member_creation_date_index
            ON events(member_creation_date ASC);
        ");

        DB::statement("
            ALTER TABLE events
            ADD COLUMN order_variants json
            GENERATED ALWAYS AS ( JSON_VALUE(attributes, '$.variants') ) VIRTUAL AFTER member_creation_date;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Not applicable
    }
}
