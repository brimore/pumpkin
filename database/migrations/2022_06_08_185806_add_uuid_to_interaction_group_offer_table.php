<?php

use App\Domain\InteractionGroup\Model\InteractionGroupOffer;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUuidToInteractionGroupOfferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('interaction_group_offer', function (Blueprint $table) {
            $table->uuid('uuid')->nullable()->after('id');
        });

        $interactionGroupOffers = InteractionGroupOffer::query()->get();
        foreach ($interactionGroupOffers as $interactionGroupOffer) {
            $interactionGroupOffer->fill(['uuid' => $interactionGroupOffer->resolveUuid()])->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('interaction_group_offer', function (Blueprint $table) {
            $table->dropColumn(['uuid']);
        });
    }
}
