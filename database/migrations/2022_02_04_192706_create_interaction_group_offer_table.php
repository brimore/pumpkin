<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInteractionGroupOfferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interaction_group_offer', function (Blueprint $table) {
            $table->id();

            $table->foreignId('interaction_group_id')->references('id')->on('interaction_groups');
            $table->foreignId('offer_id')->references('id')->on('offers');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interaction_group_offer');
    }
}
