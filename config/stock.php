<?php
return [
    'default_repo' => env('STOCK_DEFAULT_REPO', 'live'),

    'repos' => [
        'live' => [
            'endpoint' => env('STOCK_LIVE_REPO_ENDPOINT')
        ],
        'fixed' => [
            'amount' => 1
        ]
    ]
];
