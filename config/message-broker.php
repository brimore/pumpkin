<?php

$sslOptions = [];
if (env('RABBITMQ_SSL')) {
    $sslOptions['verify_peer_name'] = env('RABBITMQ_VERIFY_PEER_NAME', true);
}

return [
    'mapping' => [
        'hercules' => [
            'member' => [
                'sync' => \App\Domain\Member\Events\SignedUp::class,
            ],
            'order' => [
                'sync' => \App\Domain\Order\Events\PlacedOrderEvent::class,
            ]
        ]
    ],

    'default_connection' => 'rabbitmq',

    'retries' => env('MESSAGE_BROKER_RETRIES', 3),

    'prefix' => env('MESSAGE_BROKER_PREFIX', ''),

    'connections' => [
        'rabbitmq' => [
            'driver' => 'rabbitmq',
            'host' => env('RABBITMQ_HOST', 'localhost'),
            'port' => env('RABBITMQ_PORT', 5672),
            'username' => env('RABBITMQ_USER', ''),
            'password' => env('RABBITMQ_PASSWORD', ''),
            'vhost' => env('RABBITMQ_VHOST', '/'),
            'connect_options' => [
                'keepalive' => env('RABBITMQ_KEEPALIVE', true),
                'heartbeat' => env('RABBITMQ_HEARTBEAT', 60),
            ],
            'ssl_options' => $sslOptions,

            'exchange' => 'amq.topic',
            'exchange_type' => 'topic',
            'exchange_passive' => false,
            'exchange_durable' => true,
            'exchange_auto_delete' => false,
            'exchange_internal' => false,
            'exchange_nowait' => false,
            'exchange_properties' => [],

            'queue_name' => env('RABBITMQ_QUEUE_NAME', ''),
            'queue_force_declare' => false,
            'queue_passive' => false,
            'queue_durable' => true,
            'queue_exclusive' => false,
            'queue_auto_delete' => false,
            'queue_nowait' => false,
            'queue_properties' => ['x-ha-policy' => ['S', 'all']],

            'consumer_tag' => env('RABBITMQ_CONSUMER_TAG', ''),
            'consumer_no_local' => false,
            'consumer_no_ack' => false,
            'consumer_exclusive' => false,
            'consumer_nowait' => false,
            'consumer_properties' => [],

            'timeout' => 0,
            'persistent' => true,
            'publish_timeout' => 0,

            'qos' => false,
            'qos_prefetch_size' => 0,
            'qos_prefetch_count' => 1,
            'qos_a_global' => false,

            'blocking' => true,

            'routing' => env('RABBITMQ_ROUTING', ''),
        ]
    ]
];
