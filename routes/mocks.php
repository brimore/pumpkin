<?php


use Illuminate\Support\Facades\Route;


Route::group(['prefix' => 'available-offers'], function () {
    Route::post('/', function () {
        return response()->json([
            "meta" => [
                "page" => [
                    "currentPage" => 1,
                    "from" => 1,
                    "lastPage" => 1,
                    "perPage" => 15,
                    "to" => 6,
                    "total" => 6
                ]
            ],
            "jsonapi" => [
                "version" => "1.0"
            ],
            "links" => [
                "first" => "https:/hercules-v2-testing.brimore.com/api/v1/available-offers?page%5Bnumber%5D=1&page%5Bsize%5D=15",
                "last" => "https:/hercules-v2-testing.brimore.com/api/v1/available-offers?page%5Bnumber%5D=1&page%5Bsize%5D=15"
            ],
            "data" => [
                [
                    "id" => "00000000-0000-0000-0000-000000000001",
                    "type" => "offers",
                    "attributes" => [
                        "code" => "string",
                        "title" => [
                            "en" => "string1",
                            "ar" => "string1"
                        ],
                        "startsAt" => "string",
                        "endsAt" => "string",
                        "isActive" => true,
                        "reward" => [
                            "type" => "get-single-sku",
                            "payload" => null,
                            "sku_payload" => [
                                "id" => "12",
                                "name" => "Iphone",
                                "image" => "https://ia800402.us.archive.org/26/items/03-05-2016_Images_Images_1-30/01_PT_hero_42_153645159.jpg",
                                "quantity" => 5
                            ]
                        ],
                        "createdAt" => "2021-11-08T21:17:58.000000Z",
                        "updatedAt" => "2021-11-08T21:17:58.000000Z"
                    ]
                ],
                [
                    "id" => "00000000-0000-0000-0000-000000000002",
                    "type" => "offers",
                    "attributes" => [
                        "code" => "string",
                        "title" => [
                            "en" => "string2",
                            "ar" => "string2"
                        ],
                        "startsAt" => "string",
                        "endsAt" => "string",
                        "isActive" => true,
                        "reward" => [
                            "type" => "select-x-sku",
                            "payload" => null,
                            "sku_payload" => null
                        ],
                        "createdAt" => "2021-11-08T21:17:58.000000Z",
                        "updatedAt" => "2021-11-08T21:17:58.000000Z"
                    ]
                ],
                [
                    "id" => "00000000-0000-0000-0000-000000000003",
                    "type" => "offers",
                    "attributes" => [
                        "code" => "string",
                        "title" => [
                            "en" => "string3",
                            "ar" => "string3"
                        ],
                        "startsAt" => "string",
                        "endsAt" => "string",
                        "isActive" => true,
                        "reward" => [
                            "type" => "discount",
                            "sku_payload" => null,
                            "payload" => 100,
                            'discount_type' => 'fixed',
                            'service_code' => '551311'
                        ],
                        "createdAt" => "2021-11-08T21:17:58.000000Z",
                        "updatedAt" => "2021-11-08T21:17:58.000000Z"
                    ]
                ],
                [
                    "id" => "00000000-0000-0000-0000-000000000004",
                    "type" => "offers",
                    "attributes" => [
                        "code" => "string",
                        "title" => [
                            "en" => "string4",
                            "ar" => "string4"
                        ],
                        "startsAt" => "string",
                        "endsAt" => "string",
                        "isActive" => true,
                        "reward" => [
                            "type" => "voucher",
                            "sku_payload" => null,
                            "payload" => 200,
                            'consumed' => 80
                        ],
                        "createdAt" => "2021-11-08T21:17:58.000000Z",
                        "updatedAt" => "2021-11-08T21:17:58.000000Z"
                    ]
                ]
            ]
        ]);
    });
    Route::get('{id}/gifts', function () {
        return response()->json([
            "meta" => [
                "page" => [
                    "currentPage" => 1,
                    "from" => 1,
                    "lastPage" => 1,
                    "perPage" => 15,
                    "to" => 6,
                    "total" => 6
                ]
            ],
            "jsonapi" => [
                "version" => "1.0"
            ],
            "links" => [
                "first" => "https:/hercules-v2-testing.brimore.com/api/v1/gifts?page%5Bnumber%5D=1&page%5Bsize%5D=15",
                "last" => "https:/hercules-v2-testing.brimore.com/api/v1/gifts?page%5Bnumber%5D=1&page%5Bsize%5D=15"
            ],
            "data" => [
                [
                    "id" => "12",
                    "type" => "gifts",
                    "attributes" => [
                        "code" => "string",
                        "title" => "Iphone 13",
                        "image" => "https://ia800402.us.archive.org/26/items/03-05-2016_Images_Images_1-30/01_PT_hero_42_153645159.jpg"
                    ]
                ]
            ]
        ]);
    });
});
