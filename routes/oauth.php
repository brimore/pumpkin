<?php

use Illuminate\Support\Facades\Route;

Route::post('revoke-token', ['as' => 'revoke_token', 'uses' => 'RevokeTokenController@revokeAccessToken']);
