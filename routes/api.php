<?php

use App\Http\Controllers\Api\V1\AudienceListsController;
use App\Http\Controllers\Api\V1\ImagesController;
use App\Http\Controllers\Api\V1\OffersController;
use App\Http\Controllers\Api\V1\SheetsController;
use App\Http\Controllers\Api\V1\SKUListsController;
use Illuminate\Support\Facades\Route;
use LaravelJsonApi\Laravel\Facades\JsonApiRoute;
use LaravelJsonApi\Laravel\Http\Controllers\JsonApiController;

// Member's Routes.
JsonApiRoute::server('v1')
    ->prefix('v1')
    ->middleware('auth:users,members')
    ->resources(function ($server) {
        $server->resource('offers', OffersController::class)->actions(function ($actions) {
            $actions->post('deserved')->middleware('impersonate');
            $actions->withId()->post('calculate-gifts')->middleware('impersonate');
            $actions->post('validate-gifts')->middleware('impersonate');
            $actions->post('disable-all');
        })->only('deserved');
    });

// User's Routes.
JsonApiRoute::server('v1')
    ->prefix('v1')
    ->middleware('auth:users')
    ->resources(function ($server) {
        $server->resource('offers', JsonApiController::class)
            ->relationships(function ($relations) {
                $relations->hasOne('audience-list');
                $relations->hasOne('condition');
                $relations->hasOne('reward');
                $relations->hasOne('accumulation-group');
            })->only('index', 'show', 'store', 'update');
        $server->resource('audience-lists', AudienceListsController::class)
            ->relationships(function ($relations) {
                $relations->hasMany('offers');
            })->actions(function ($actions) {
                $actions->post('affected-members');
                $actions->post('export-affected-members');
            });
        $server->resource('conditions', JsonApiController::class)
            ->relationships(function ($relations) {
                $relations->hasOne('offer');
            });
        $server->resource('rewards', JsonApiController::class)
            ->relationships(function ($relations) {
                $relations->hasOne('offer');
            });
        $server->resource('sku-lists', JsonApiController::class);
        $server->resource('sku-lists', SKUListsController::class)->actions(function ($actions) {
            $actions->withId()->patch('update-criteria');
        })->only('update-criteria');
        $server->resource('interaction-group-offers', JsonApiController::class);
        $server->resource('interaction-groups', JsonApiController::class)
            ->relationships(function ($relations) {
                $relations->hasMany('interaction_group_offers');
            })->only('index', 'show', 'update', 'store', 'delete');
        $server->resource('accumulation-groups', JsonApiController::class);
        // Only list & show endpoints.
        $server->resource('event-types', JsonApiController::class)->only('index', 'show');
        $server->resource('condition-types', JsonApiController::class)->only('index', 'show');
        $server->resource('reward-types', JsonApiController::class)->only('index', 'show');
        $server->resource('members', JsonApiController::class)->only('index', 'show');
        $server->resource('categories', JsonApiController::class)
            ->relationships(function ($relations) {
                $relations->hasMany('products');
            })->only('index', 'show');
        $server->resource('brands', JsonApiController::class)
            ->relationships(function ($relations) {
                $relations->hasMany('products');
            })->only('index', 'show');
        $server->resource('products', JsonApiController::class)
            ->relationships(function ($relations) {
                $relations->hasOne('category');
                $relations->hasOne('brand');
                $relations->hasMany('product-variants');
            })->only('index', 'show');
        $server->resource('product-variants', JsonApiController::class)
            ->relationships(function ($relations) {
                $relations->hasOne('product');
            })->only('index', 'show');
        // External routes.
        Route::resource('sheets', SheetsController::class)->only(['store', 'update']);
        Route::resource('images', ImagesController::class)->only(['store', 'update']);
    });


// Client Routes.
JsonApiRoute::server('v1')
    ->prefix('v1')
    ->middleware('client')
    ->resources(function ($server) {
        $server->resource('offers', OffersController::class)->actions(function ($actions) {
            $actions->post('client-deserved', 'deserved')->middleware('impersonate');
            $actions->withId()->post('client-calculate-gifts', 'calculateGifts')->middleware('impersonate');
            $actions->post('client-validate-gifts', 'validate-gifts')->middleware('impersonate');
        })->only('client-deserved');
    });

