<?php

namespace App\Providers;

use App\OfferConsumption\InteractionGroups\Contract\Repository;
use App\OfferConsumption\InteractionGroups\Repository\EloquentRepository;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factories\Factory;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Repository::class, EloquentRepository::class);

        Factory::guessFactoryNamesUsing(
            fn(string $modelName) => 'Database\\Factories\\' . class_basename($modelName) . 'Factory'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
