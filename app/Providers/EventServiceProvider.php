<?php

namespace App\Providers;

use App\Domain\Condition\Model\Condition;
use App\Domain\Member\Events\SignedUp;
use App\Domain\Member\Listeners\MemberSignedUp;
use App\Domain\Offer\Model\Offer;
use App\Domain\Order\Events\PlacedOrderEvent;
use App\Domain\Order\Listeners\PlacedOrderListener;
use App\Domain\Reward\Model\Reward;
use App\Domain\SKUList\Model\SkuList;
use App\Observers\ConditionObserver;
use App\Observers\OfferObserver;
use App\Observers\RewardObserver;
use App\Observers\SKUListObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        SignedUp::class => [
            MemberSignedUp::class,
        ],
        PlacedOrderEvent::class => [
            PlacedOrderListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Offer::observe(OfferObserver::class);
        Condition::observe(ConditionObserver::class);
        SkuList::observe(SKUListObserver::class);
        Reward::observe(RewardObserver::class);
    }
}
