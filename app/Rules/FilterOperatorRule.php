<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use function PHPUnit\Framework\isEmpty;

class FilterOperatorRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $filterBy1 = 'filter';
        $filterBy2 = 'filter-group';
        $filterBy3 = 'condition';
        $filterBy6 = 'filters';
        $filtered  = array_filter($value, function ($var) use ( $filterBy1, $filterBy2,$filterBy3, $filterBy6 )
        {
            return ($var['type'] == $filterBy1 || $var['type'] == $filterBy2 || $var['type'] == $filterBy3 || $var['type'] == $filterBy6);
        });
        $operators  = array_filter($value, function ($var) use ( $filterBy1, $filterBy2,$filterBy3, $filterBy6 )
        {
            return ($var['type'] != $filterBy1 && $var['type'] != $filterBy2 && $var['type'] != $filterBy3 && $var['type'] != $filterBy6);
        });
        if (!empty($filtered) || !empty($operators)){
            $filters_count = count($filtered);
            $operators_count = count($operators);
            return ($filters_count > $operators_count) && ($filters_count - $operators_count === 1);
        }else{
            return true;
        }


    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'number of operators and filters are not compatible';
    }
}
