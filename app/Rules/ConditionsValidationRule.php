<?php

namespace App\Rules;

use Couchbase\DesignDocument;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

/**
 *
 */
class ConditionsValidationRule implements Rule
{
    /**
     * @param $attribute
     * @param $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $step = request()->data['type'];
        $attribute1_name = explode('.', $attribute)[0];
        $index1 = (int)explode('.', $attribute)[1];
        $attribute2_name = explode('.', $attribute)[2];
        $index2 = (int)explode('.', $attribute)[3];
        $attribute3_name = explode('.', $attribute)[4];
        $filter_type = request()->data['attributes'][$attribute1_name][$index1]['type'];
        if ($filter_type == 'filter') {
            $step_condition_name = $this->checkStepAttribute($step);
            $step_condition = request()->data['attributes'][$attribute1_name][$index1][$step_condition_name];
            $conditions = request()->data['attributes'][$attribute1_name][$index1][$attribute2_name][$index2];
            return $this->checkAll($step, $step_condition, $conditions, $attribute3_name, $value);
        } elseif ($filter_type == 'filter-group') {
            $index3 = (int)explode('.', $attribute)[5];
            $attribute4_name = explode('.', $attribute)[6];
            $step_condition_name = $this->checkStepAttribute($step);
            $step_condition = request()->data['attributes'][$attribute1_name][$index1][$attribute2_name][$index2][$step_condition_name];
            $conditions = request()->data['attributes'][$attribute1_name][$index1][$attribute2_name][$index2][$attribute3_name][$index3];
            return $this->checkAll($step, $step_condition, $conditions, $attribute4_name, $value);

        }
        return false;
    }

    /**
     * @param string $step
     * @param string $step_condition
     * @param array $conditions
     * @param string $attribute3_name
     * @param string $value
     * @return bool
     */
    protected function checkAll(string $step, string $step_condition, array $conditions, string $attribute3_name, mixed $value): bool
    {
        if ($attribute3_name == 'type') {
            if ($conditions['type'] == 'condition' || $conditions['type'] == 'logical-operator')
                return true;
        } elseif ($attribute3_name == 'value') {
            return $this->checkValue($conditions['type'], $value);

        } elseif ($attribute3_name == 'attribute') {
            $conditions_attribute = $conditions['attribute'];
            return $this->checkDbAttribute($step, $conditions_attribute, $step_condition);

        } elseif ($attribute3_name == 'operator') {
            $conditions_operator = $conditions['operator'];
            $conditions_attribute = $conditions['attribute'];
            return $this->checkDbOperator($step, $conditions_operator, $step_condition, $conditions_attribute);
        }
        return false;
    }

    /**
     * @param string $str
     * @param mixed $val
     * @return bool
     */
    protected function checkValue(string $str, mixed $val): bool
    {
        if ($str == 'condition') {
            //need to adjust according to each type
            return true;
        } elseif ($str == 'logical-operator') {
            return in_array($val, ['AND', 'OR', 'NOT']);
        }
        return false;
    }

    /**
     * @param string $step
     * @param string $condition_attribute
     * @param string $step_condition
     * @return bool
     */
    protected function checkDbAttribute(string $step, string $condition_attribute, string $step_condition): bool
    {
        $tables = ['audience-lists' => 'event_types', 'conditions' => 'condition_types', 'rewards' => 'reward_types'];
        $db_condition = DB::table($tables[$step])->where('name', $step_condition)->first();
        if (!isset($db_condition)){
            return false;
        }else{
            $db_attributes = json_decode($db_condition->attributes, true);
            return in_array($condition_attribute, array_keys($db_attributes));
        }
    }

    /**
     * @param string $step
     * @param string $condition_operator
     * @param string $step_condition
     * @param string $condition_attribute
     * @return bool
     */
    protected function checkDbOperator(string $step, string $condition_operator, string $step_condition, string $condition_attribute): bool
    {
        $tables = ['audience-lists' => 'event_types', 'conditions' => 'condition_types', 'rewards' => 'reward_types'];
        $db_condition = DB::table($tables[$step])->where('name', $step_condition)->first();
        if (!isset($db_condition)){
            return false;
        }else{
            $db_attributes = json_decode($db_condition->attributes, true);
            if (!isset($db_attributes[$condition_attribute])){
                return false;
            }else{
                foreach ($db_attributes[$condition_attribute] as $item)
                {
                    if (!isset($item['operators'])){
                        return false;
                    }else
                    {
                        return in_array($condition_operator, $item['operators']);
                    }
                }
            }

            if (!isset($db_attributes[$condition_attribute])){
                return false;
            }else{
                foreach ($db_attributes[$condition_attribute] as $item)
                {
                    if (!isset($item['operators'])){
                        return false;
                    }else
                    {
                        return in_array($condition_operator, $item['operators']);
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param string $step
     * @return string
     */
    protected function checkStepAttribute(string $step) : string{
        $attribute_name = ['audience-lists' => 'event', 'conditions' => 'condition', 'rewards' => 'reward' ];
        return $attribute_name[$step];
    }

    /**
     * @return string
     */
    public function message()
    {
        return ':conditions type or value is invalid';
    }


}
