<?php

namespace App\Rules;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule as Rules;
use Illuminate\Contracts\Validation\Rule;

class LocalizationRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @param string $table
     * @param Model|null $model
     */
    public function __construct(protected string $table, protected ?Model $model = null)
    {
    }
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool|array
     */
    public function passes($attribute, $value): bool|array
    {
        $localized_attributes = [];
        foreach (get_locales() as $locale) {
            if (!array_key_exists($locale,$value)) {
                return false;
            }
            $localized_attributes[(string)($locale)] = Rules::unique($this->table, $attribute . "->" . $locale)
                ->ignore($this->model?->id);
        }
        $validator = Validator::make($value, $localized_attributes);
        return (!$validator->fails());
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The name missing language attribute is not valid.';
    }
}
