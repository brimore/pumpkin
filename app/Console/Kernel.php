<?php

namespace App\Console;

use App\Domain\Offer\Model\Offer;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('passport:purge')->hourly();
        $schedule->command('data-migration:members')->hourly();
        $schedule->command('hercules:data-migrate')->hourly();
        $schedule->command('MigrateUsers')->hourly();
        $schedule->command('audience-exports:delete')->hourly();
        $schedule->command('offers-deactivate')->everyMinute();
        $schedule->command('events:migrate SIGNED_UP --monthly=true')->everyTenMinutes();
        $schedule->command('events:migrate PLACED_ORDER --monthly=true')->everyTenMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
