<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MigrateUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'MigrateUsers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate Users from old system to configuration tool';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info("Migrating Users data");
        $this->info("Migrating Users data");
        try {
            DB::connection('Hercules')->table('users')
                ->select(['id', 'name', 'email', 'password', 'created_at', 'updated_at'])
                ->orderBy('created_at')
                ->chunk(500, function ($AllData) {
                    $insertion_rows = [];
                    $update_rows_id = [];

                    foreach ($AllData as $Data) {
                        $old_data = DB::connection('mysql')
                            ->table('users')
                            ->where('email', '=', $Data->email)
                            ->first();
                        $name = $this->prepareName($Data->name);
                        if ($old_data != null) {
                            if ([$old_data->first_name, $old_data->last_name, $old_data->password] != [$name['firstname'], $name['lastname'], $Data->password]) {
                                $update_rows_id[] = [
                                    'id' => $old_data->id
                                ];
                                $insertion_rows[] = [
                                    'id' => $Data->id,
                                    'first_name' => $name['firstname'],
                                    'last_name' => $name['lastname'],
                                    'email' => $Data->email,
                                    'password' => $Data->password,
                                    'created_at' => $Data->created_at,
                                    'updated_at' => $Data->updated_at,
                                ];
                            }
                        } else {
                            $insertion_rows[] = [
                                'id' => $Data->id,
                                'first_name' => $name['firstname'],
                                'last_name' => $name['lastname'],
                                'email' => $Data->email,
                                'password' => $Data->password,
                                'created_at' => $Data->created_at,
                                'updated_at' => $Data->updated_at,
                            ];
                        }

                    }
                    if ($update_rows_id != null) {
                        $this->info("updating...");
                        DB::connection('mysql')->table('users')->whereIn('id', $update_rows_id)->delete();
                    }

                    DB::connection('mysql')->table('users')->insert($insertion_rows);
                });
        } catch (QueryException $ex) {
            Log::error('Error in importing Users data: ' . $ex->getMessage());
            $this->info('Error in importing Users data: ' . $ex->getMessage());
        }
        Log::info("Finished Migrating Users data");
        $this->info("Finished Migrating Users data");
        return true;
    }

    public function prepareName($name): array
    {
        $parts = explode(" ", $name);
        if (count($parts) > 1) {
            $lastname = array_pop($parts);
            $firstname = implode(" ", $parts);
        } else {
            $firstname = $name;
            $lastname = " ";
        }
        return compact('firstname', 'lastname');
    }
}

