<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MigrateMember extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data-migration:members';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate Member data from old database to the new one';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        Log::info("Migrating Members data");
        $this->info("Migrating Members data");
        try {
            DB::connection('Hercules')->table('members')
                ->select(['id', 'member_code', 'name', 'password', 'created_at', 'updated_at'])
                ->chunkById(500, function ($AllData) {
                    $insertion_rows = [];
                    $update_rows_id = [];
                    foreach ($AllData as $Data) {
                        $old_data = DB::connection('mysql')->table('members')->find($Data->id);
                        if( $old_data != null ){
                            if([$old_data->name, $old_data->password] != [$Data->name, $Data->password]){
                                DB::connection('mysql')
                                    ->table('members')
                                    ->where('id' , $Data->id)
                                    ->update([
                                        'code' => $Data->member_code,
                                        'name' => $Data->name,
                                        'password' => $Data->password,
                                    ]);
                            }
                        }else{
                            $insertion_rows[] = [
                                'id' => $Data->id,
                                'code' => $Data->member_code,
                                'name' => $Data->name,
                                'password' => $Data->password,
                                'created_at' => $Data->created_at,
                                'updated_at' => $Data->updated_at,
                            ];
                        }
                    }

                    if(count($insertion_rows)>0){
                        DB::connection('mysql')->table('members')->insert($insertion_rows);
                    }
                });
            Log::info("Finished Migrating Members data");
            $this->info("Finished Migrating Members data");
        } catch (QueryException $ex) {
            Log::error('Error in importing Members data: ' . $ex->getMessage());
            $this->info('Error in importing Members data: ' . $ex->getMessage());
        }
        return true;
    }
}
