<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AprilProgramGift extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'april:program-gift-consumption:sync {reward_amount} {hercules_offer_id} {pumpkin_offer_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        DB::connection('Hercules')
            ->table('orders_lines')
            ->select(['orders_lines.*', 'orders.member_id', 'orders.status', 'orders.total_points', 'orders.total_price', 'orders.created_at as order_creation_date'])
            ->join('orders', 'orders.id', '=', 'orders_lines.order_id')
            ->where('orders_lines.promotion_id', $this->argument('hercules_offer_id'))
            ->whereNotIn('orders.status', [1, 5, 6, 7, 8, 13, 16])
            ->get()
            ->map(function ($order_line) {
                DB::table('member_offer_consumption')->insert([
                    'member_id' => $order_line->member_id,
                    'offer_id' => $this->argument('pumpkin_offer_id'),
                    'order_id' => $order_line->order_id,
                    'status' => $order_line->status,
                    'quantity' => ($order_line->quantity / ((int)$this->argument('reward_amount'))),
                    'total_points' => $order_line->total_points,
                    'total_price' => $order_line->total_price,
                    'created_at' => $order_line->order_creation_date,
                    'updated_at' => $order_line->order_creation_date
                ]);
            });
    }
}
