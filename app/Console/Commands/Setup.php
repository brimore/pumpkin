<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Laravel\Passport\ClientRepository;

class Setup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:setup {--root-password=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setup the project';

    /**
     * @var ClientRepository
     */
    private ClientRepository $passportClientRepo;

    public function __construct(ClientRepository $passportClientRepo)
    {
        parent::__construct();

        $this->passportClientRepo = $passportClientRepo;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->withErrorHandling(function () {
            $this->validateNotSetupBefore();

            $this->createRootUser();

            $this->setupPassport();

            $this->completeSetup();
        });
    }

    /**
     * Handle expected errors
     *
     * @param $callback
     */
    protected function withErrorHandling($callback)
    {
        try {
            DB::transaction(function () use ($callback) {
                $callback();
            });
        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }
    }

    /**
     * Create the root user
     *
     * @throws \Exception
     */
    protected function createRootUser()
    {
        $rootPassword = $this->option('root-password');
        if (!$rootPassword) {
            $rootPassword = Str::random(8);
        }

        User::query()->create([
            'first_name' => 'Root',
            'last_name' => 'User',
            'email' => 'root-user@brimore.com',
            'email_verified_at' => now(),
            'password' => bcrypt($rootPassword),
        ]);

        $this->line('<comment>Root Password:</comment> ' . $rootPassword);
    }

    /**
     * Setup Passport
     */
    protected function setupPassport()
    {
        $this->call('passport:keys');

        $user = $this->passportClientRepo->createPasswordGrantClient(
            null, 'Password Grant Client', 'http://localhost', 'users'
        );
        $user->fill(['secret' => null])->save();

        $member = $this->passportClientRepo->createPasswordGrantClient(
            null, 'Member\'s Password Grant Client', 'http://localhost', 'members'
        );
        $member->fill(['secret' => null])->save();

        $this->line('<comment>Password Grant Client ID:</comment> ' . $user->id);
        $this->line('<comment>Member\'s Password Grant Client ID:</comment> ' . $member->id);
    }

    /**
     * Validate whether the setup has been done before
     *
     * @throws \Exception
     */
    protected function validateNotSetupBefore()
    {
        if (file_exists($this->getSetupFilePath())) {
            throw new \Exception('Setup is done already');
        }
    }

    /**
     * Mark the setup as completed
     */
    protected function completeSetup()
    {
        file_put_contents($this->getSetupFilePath(), '');

        $this->info('Setup is completed');
    }

    /**
     * Get the lock file name
     *
     * @return string
     */
    protected function getSetupFilePath()
    {
        return storage_path('setup');
    }
}
