<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\QueryException;
use function PHPUnit\Framework\isEmpty;

class DataMigrate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hercules:data-migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate data from old hercules to new hercules';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //Categories
        Log::info("Migrating Categories data");
        $this->info("Migrating Categories data");
        try {
            DB::connection('Hercules')->table('eve_categories')
                ->select(['id','parent_id','name'])
                ->chunkById(500, function ($AllData) {
                    $insertion_rows = [];
                    $update_rows_id = [];
                    foreach ($AllData as $Data) {
                        $old_data = DB::connection('mysql')->table('categories')->find($Data->id);
                        if( $old_data != null ){
                            if([$old_data->name, $old_data->parent_id] != [$Data->name, $Data->parent_id]){
                                $update_rows_id[] =[
                                    'id' => $Data->id
                                ];
                                $insertion_rows[] = [
                                    'id' => $Data->id,
                                    'parent_id' => $Data->parent_id,
                                    'name' => $Data->name
                                ];
                            }
                        }else{
                            $insertion_rows[] = [
                                'id' => $Data->id,
                                'parent_id' => $Data->parent_id,
                                'name' => $Data->name
                            ];
                        }
                    }

                    if($update_rows_id != null){
                        $this->info("updating...");
                        DB::connection('mysql')->table('categories')->whereIn('id',$update_rows_id)->delete();
                    }

                    DB::connection('mysql')->table('categories')->insert($insertion_rows);
                });
            Log::info("Finished Migrating Categories data");
            $this->info("Finished Migrating Categories data");
        } catch(QueryException $ex){
            Log::error('Error in importing categories data: '.$ex->getMessage());
            $this->info('Error in importing categories data: '.$ex->getMessage());
        }


        //Brands

        Log::info("Migrating Brands data");
        $this->info("Migrating Brands data");
        try {
            DB::connection('Hercules')->table('brands')
                ->select(['id','name'])
                ->chunkById(500, function ($AllData) {
                    $insertion_rows = [];
                    $update_rows_id = [];
                    foreach ($AllData as $Data) {
                        $old_data = DB::connection('mysql')->table('brands')->find($Data->id);
                        if( $old_data != null ){
                            if($old_data->name != $Data->name){
                                $update_rows_id[] =[
                                    'id' => $Data->id
                                ];
                                $insertion_rows[] = [
                                    'id' => $Data->id,
                                    'name' => $Data->name
                                ];
                            }
                        }else{
                            $insertion_rows[] = [
                                'id' => $Data->id,
                                'name' => $Data->name
                            ];
                        }
                    }

                    if($update_rows_id != null){
                        $this->info("updating...");
                        DB::connection('mysql')->table('brands')->whereIn('id',$update_rows_id)->delete();
                    }

                    DB::connection('mysql')->table('brands')->insert($insertion_rows);
//                    DB::connection('mysql')->table('brands')->insert(json_decode(json_encode($AllData), true));
                });
            Log::info("Finished Migrating Brands data");
            $this->info("Finished Migrating Brands data");
        } catch(QueryException $ex){
            Log::error('Error in importing Brands data: '.$ex->getMessage());
            $this->info('Error in importing Brands data: '.$ex->getMessage());
        }


        //Products

        Log::info("Migrating Products data");
        $this->info("Migrating Products data");
        try {
            DB::connection('Hercules')->table('products')
                ->select(['id','name','eve_category_id','brand_id','image'])
                ->chunkById(500, function ($AllData) {
                    $insertion_rows = [];
                    $update_rows_id = [];
                    foreach ($AllData as $Data) {
                        $old_data = DB::connection('mysql')->table('products')->find($Data->id);
                        if( $old_data != null ){
                            if([$old_data->name, $old_data->category_id, $old_data->brand_id, $old_data->image] != [$Data->name, $Data->eve_category_id, $Data->brand_id, $Data->image]){
                                $update_rows_id[] =[
                                    'id' => $Data->id
                                ];
                                $insertion_rows[] = [
                                    'id' => $Data->id,
                                    'name' => $Data->name,
                                    'category_id' => $Data->eve_category_id,
                                    'brand_id' => $Data->brand_id,
                                    'image' => $Data->image,
                                ];
                            }
                        }else{
                            $insertion_rows[] = [
                                'id' => $Data->id,
                                'name' => $Data->name,
                                'category_id' => $Data->eve_category_id,
                                'brand_id' => $Data->brand_id,
                                'image' => $Data->image,
                            ];
                        }
                    }

                    if($update_rows_id != null){
                        $this->info("updating...");
                        DB::connection('mysql')->table('products')->whereIn('id',$update_rows_id)->delete();
                    }

                    DB::connection('mysql')->table('products')->insert($insertion_rows);
                });
            Log::info("Finished Migrating Products data");
            $this->info("Finished Migrating Products data");
        } catch(QueryException $ex){
            Log::error('Error in importing Products data: '.$ex->getMessage());
            $this->info('Error in importing Products data: '.$ex->getMessage());
        }


        //Product-Variants

        Log::info("Migrating Products-Variants data");
        $this->info("Migrating Products-Variants data");
        try {
            $portfolio = DB::connection('Hercules')->table('portfolios')
                ->select('portfolios.id')
                ->whereMonth('start_date', Carbon::now()->month)
                ->whereYear('start_date', Carbon::now()->year)
                ->orderBy('start_date', 'DESC')
                ->first();
            //truncate product_variants table
            DB::connection('mysql')->table('product_variants')->delete();
            //import data using chunk
            DB::connection('Hercules')->table('portfolios_products_variant')
                ->where('portfolio_id', '=', $portfolio->id )
                ->whereNotNull('pricelist_id')
                ->rightJoin('products_variant', 'products_variant_id', '=', 'products_variant.id')
                ->select(['products_variant.id','products_variant.name as name', 'products_variant.code as code', 'products_variant.product_id', 'portfolios_products_variant.member_price as price'])
                ->orderBy('products_variant.id','DESC')
                ->chunkById(500, function ($AllData) {
                    $AllData_unique = $AllData->unique('id');
                    //update rows
//                    $insertion_rows = [];
//                    $update_rows_id = [];
//                    foreach ($AllData_unique as $Data) {
//                        $old_data = DB::connection('mysql')->table('product_variants')->find($Data->id);
//                        if( $old_data != null ){
//                            if([$old_data->name, $old_data->code, $old_data->product_id, $old_data->price] != [$Data->name, $Data->code, $Data->product_id, $Data->price]){
//                                $update_rows_id[] =[
//                                    'id' => $Data->id
//                                ];
//                                $insertion_rows[] = [
//                                    'id' => $Data->id,
//                                    'name' => $Data->name,
//                                    'code' => $Data->code,
//                                    'product_id' => $Data->product_id,
//                                    'price' => $Data->price,
//                                ];
//                            }
//                        }else{
//                            $insertion_rows[] = [
//                                'id' => $Data->id,
//                                'name' => $Data->name,
//                                'code' => $Data->code,
//                                'product_id' => $Data->product_id,
//                                'price' => $Data->price,
//                            ];
//                        }
//                    }
//
//                    if($update_rows_id != null){
//                        $this->info("updating...");
//                        DB::connection('mysql')->table('product_variants')->whereIn('id',$update_rows_id)->delete();
//                    }

//                    DB::connection('mysql')->table('product_variants')->insert($insertion_rows);
                    DB::connection('mysql')->table('product_variants')->insert(json_decode(json_encode($AllData_unique), true));
                },'products_variant.id','id');
            Log::info("Finished Migrating Products-Variants data");
            $this->info("Finished Migrating Products-Variants data");
        } catch (QueryException $ex) {
            Log::error('Error in importing Products-Variants data: ' . $ex->getMessage());
            $this->info('Error in importing Products-Variants data: ' . $ex->getMessage());
        }

        return true;
    }
}
