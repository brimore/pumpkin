<?php

namespace App\Console\Commands;

use App\Domain\Offer\Model\Offer;
use Illuminate\Console\Command;

class DisableOffers extends Command
{
    protected $signature = 'offers-deactivate';

    protected $description = 'Disable offer if its ends_at has passed';

    public function handle()
    {
        Offer::where('ends_at', '<', now())
            ->where('is_active','active')
            ->update(['is_active' => 'ended']);
    }
}
