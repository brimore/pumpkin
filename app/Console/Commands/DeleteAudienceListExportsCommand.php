<?php

namespace App\Console\Commands;

use Storage;
use Illuminate\Console\Command;

class DeleteAudienceListExportsCommand extends Command
{
    protected $signature = 'audience-exports:delete';

    protected $description = 'Delete stored sheets for export affected members';

    public function handle()
    {
        Storage::disk('public')->deleteDir('export_affected_members');
    }
}
