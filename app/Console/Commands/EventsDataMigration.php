<?php

namespace App\Console\Commands;

use App\Domain\Event\Model\Event;
use App\Domain\EventType\Model\EventType;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Symfony\Component\Console\Helper\ProgressBar;

class EventsDataMigration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'events:migrate {event?} {--start_date=} {--end_date=} {--monthly=} {--for_testing=} {--member_points_only=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate and Map old data to the current event module.';

    /**
     * @var string[]
     */
    protected $events = [
        'PLACED_ORDER' => 'orders',
        'SIGNED_UP' => 'members'
    ];

    /**
     * @var int
     */
    protected int $insertedEventsCount = 0;

    /**
     * @var int
     */
    protected int $insertedMemberPointsCount = 0;
    protected int $deletedMemberPointsCount = 0;

    /**
     * @var int
     */
    protected int $insertedOfferConsumptionCount = 0;
    protected int $deletedOfferConsumptionCount = 0;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        Log::info('Migrating Events (Members/Orders)');
    }

    /**
     * Execute the console command.
     * @return bool|null
     */
    public function handle()
    {
        // Check if event type exists or not.
        $event_type = EventType::where('name', $this->argument('event'))->first();
        if (!$event_type && $this->argument('event')) {
            $this->error('Provided event not exist.');
            return false;
        }

        // Then, we will build our migration queries and run them.
        if ($event_type) {
            $this->BuildAndRunMigrationQuery($event_type);
        } else {
            foreach ($this->events as $event) {
                $this->BuildAndRunMigrationQuery($event);
            }
        }
    }

    /**
     * @param Collection $members
     * @param bool $for_testing
     */
    public function membersMigration(Collection $members, $for_testing = false)
    {
        $event_type = EventType::whereName('SIGNED_UP')->first();
        $events = [];
        $members->each(function ($member) use (&$events, $event_type) {
            $national_id = $member->national_id;
            if (!Event::where('member_id', $member->id)->where('event_type_id', $event_type->id)->exists())
                $events[] = [
                    'event_type_id' => $event_type->id,
                    'member_id' => $member->id,
                    'attributes' => [
                        'age' => $national_id ? $this->getAgeFromIdentity($member->national_id) : NULL,
                        'gender' => $national_id ? $this->getGenderFromIdentity($member->national_id) : NULL,
                        'created_at' => $member->created_at
                    ]
                ];
        });

        $this->insertedEventsCount += count($events);
        if (!$for_testing)
            $event_type->events()->createMany($events);
    }

    /**
     * @param Collection $orders
     * @param bool $for_testing
     */
    public function ordersMigration(Collection $orders, $for_testing = false)
    {
        $event_type = EventType::whereName('PLACED_ORDER')->first();
        $events = [];
        $this->calculateMemberPoints($orders, $for_testing);
        $this->calculateMemberOfferConsumption($orders, $for_testing);
        if (!$this->option('member_points_only')) {
            $order_product_variants = $this->mapProductVariants($orders);
            $orders->each(function ($order) use (&$events, $event_type, $order_product_variants) {
                if (!Event::where('order_id', $order->id)->exists())
                    $events[] = [
                        'event_type_id' => $event_type->id,
                        'member_id' => $order->member_id,
                        'attributes' => [
                            'order_id' => $order->id,
                            'source' => $order->source,
                            'variants' => $order_product_variants->where('order_id', $order->id)
                                ->map(fn($variant) => Arr::except($variant, ['order_id']))->toArray(),
                            'status' => $order->status,
                            "total_price" => $order->total_price,
                            "total_points" => $order->total_points,
                            'created_at' => $order->created_at
                        ]
                    ];
            });

            $this->insertedEventsCount += count($events);
            if (!$for_testing)
                $event_type->events()->createMany($events);
        }
    }

    /**
     * @param string $identity
     * @return string
     */
    public function getGenderFromIdentity(string $identity): string
    {
        // substring gender data and convert it to int
        $gender = (int)substr($identity, 12, 1);

        return $gender % 2 ? 'male' : 'female';
    }

    /**
     * @param string $identity
     * @return int|null
     */
    public function getAgeFromIdentity(string $identity): ?int
    {
        // substring identity to get birthday and convert it to be a carbon object.
        try {
            $identity = $this->convertArabicToEnglishNumber($identity);
            $birthdate = Carbon::createFromFormat('ymd', substr($identity, 1, 6));
        } catch (\Exception $exception) {
        }

        return isset($birthdate) ? Carbon::today()->diffInYears($birthdate) : NULL;
    }

    /**
     * @param int $transferred_rows
     * @return ProgressBar
     */
    private function InitProgressBar(int $transferred_rows): ProgressBar
    {
        $bar = $this->output->createProgressBar($transferred_rows);
        $bar->setFormat(
            'Transferring Rows: %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%'
        );

        return $bar;
    }

    /**
     * @param Builder $query
     */
    private function ValidateAndApplyDateFilter(Builder $query): void
    {
        $start_date = $this->option('start_date');
        $end_date = $this->option('end_date');

        $validator = Validator::make(
            compact(['start_date', 'end_date']),
            ['start_date' => 'required|date', 'end_date' => 'required|date']
        );

        if (!$validator->fails()) {
            $query->where('created_at', '>=', Carbon::parse($start_date))
                ->where('created_at', '<=', Carbon::parse($end_date));
        }
    }

    /**
     * @param $event_type
     * @param array $joins
     * @return void
     */
    private function BuildAndRunMigrationQuery($event_type, $joins = []): void
    {
        // At first, we have to build our query and define our event.
        $event = is_string($event_type) ? $event_type : $this->events[$event_type->name];
        $query = DB::connection('Hercules')->table($event);

        if ($this->option('monthly'))
            $query->where('created_at', '>=', now()->firstOfMonth())
                ->where('created_at', '<=', now()->endOfMonth());

        // Then, validate if any date filters exist and apply it.
        $this->ValidateAndApplyDateFilter($query);

        $forTesting = (boolean)$this->option('for_testing');

        $transferred_rows = $query->count();

        $this->info("Total events => " . $transferred_rows);

        // After that, we'll define our progress bar.
        $bar = $this->InitProgressBar($transferred_rows);
        $bar->start();

        // Finally, we have to perform data migration.
        $query->orderBy('id')->chunk(500, function ($members) use ($bar, $event, $forTesting) {
            $this->{$event . "Migration"}($members, $forTesting);
            $bar->advance(500);
        });
        $bar->finish();

        $this->newLine();
        $this->info($this->insertedEventsCount . " events has been transferred successfully!");
        if ($event === 'orders') {
            $this->info($this->insertedMemberPointsCount . " member points that will be inserted !");
            $this->info($this->deletedMemberPointsCount . " member points that will be deleted !");
            $this->info($this->insertedOfferConsumptionCount . " offer-consumptions that will be inserted !");
            $this->info($this->deletedOfferConsumptionCount . " offer-consumptions that will be deleted !");
        }
    }

    /**
     * @param Collection $orders
     * @return Collection
     */
    private function mapProductVariants(Collection $orders): Collection
    {
        return DB::connection('Hercules')->table('orders_lines AS ol')
            ->select(['ol.*', 'p.category_id', 'p.brand_id', 'pv.code'])
            ->join('products_variant AS pv', 'pv.id', '=', 'ol.products_variant_id')
            ->join('products AS p', 'p.id', '=', 'pv.product_id')
            ->whereIn('order_id', $orders->pluck('id')->toArray())
            ->get()
            ->map(function ($variant) {
                return [
                    'order_id' => $variant->order_id,
                    'id' => $variant->products_variant_id,
                    'SKU' => $variant->code,
                    "price" => $variant->unit_price,
                    "brand_id" => $variant->brand_id,
                    "quantity" => $variant->quantity,
                    "category_id" => $variant->category_id,
                    "total_price" => $variant->total_price,
                    "offer_id" => $variant->promotion_id,
                    "total_points" => $variant->total_points
                ];
            });
    }

    /**
     * @param Collection $orders
     * @return void
     */


    /**
     * @param Collection $orders
     * @return void
     */
    private function calculateMemberPoints(Collection $orders, bool $for_testing = false)
    {
        $inserted_orders = [];
        $orders->each(function ($order) use (&$inserted_orders, $for_testing) {
            $points = DB::table('member_points')->where('order_id', $order->id);
            if (!in_array($order->status, [1, 5, 6, 7, 8, 13, 16])) {
                if (!$points->exists())
                    $inserted_orders[] = [
                        'member_id' => $order->member_id,
                        'order_id' => $order->id,
                        'status' => $order->status,
                        'order_creation_date' => $order->created_at,
                        'total_points' => $order->total_points,
                        'created_at' => now(),
                        'updated_at' => now()
                    ];
            } else {
                if ($points->exists())
                    $this->deletedMemberPointsCount += 1;
                if (!$for_testing)
                    $points->delete();
            }
        });

        $this->insertedMemberPointsCount += count($inserted_orders);
        if (!$for_testing)
            DB::table('member_points')->insert($inserted_orders);
    }

    /**
     * @param Collection $orders
     * @param bool $for_testing
     * @return void
     */
    public function calculateMemberOfferConsumption(Collection $orders, bool $for_testing = false)
    {
        $inserted_offers = [];
        $offers = DB::connection('Hercules')->table('order_offers')
            ->whereIn('order_id', $orders->pluck('id')->toArray())
            ->get();
        $orders->each(function ($order) use (&$inserted_offers, $offers, $for_testing) {
            foreach ($offers->where('order_id', $order->id) as $offer) {
                $member_offer = DB::table('member_offer_consumption')
                    ->where('offer_id', $offer->uuid)
                    ->where('member_id', $order->member_id)
                    ->where('order_id', $order->id);

                if (!in_array($order->status, [1, 5, 6, 7, 8, 13, 16])) {
                    if (!$member_offer->exists())
                        $inserted_offers[] = [
                            'member_id' => $order->member_id,
                            'offer_id' => $offer->uuid,
                            'order_id' => $order->id,
                            'status' => $order->status,
                            'quantity' => $offer->multiple,
                            'total_points' => 0,
                            'total_price' => 0,
                            'created_at' => Carbon::parse($order->created_at)->format('Y-m-d H:i:s'),
                            'updated_at' => Carbon::parse($order->updated_at)->format('Y-m-d H:i:s')
                        ];
                } else {
                    if ($member_offer->exists())
                        $this->deletedOfferConsumptionCount += $member_offer->count();
                    if (!$for_testing)
                        $member_offer->delete();
                }
            }
        });

        $this->insertedOfferConsumptionCount += count($inserted_offers);
        if (!$for_testing)
            DB::table('member_offer_consumption')->insert($inserted_offers);
    }

    /**
     * @param $number
     * @return string
     */
    function convertArabicToEnglishNumber($number): string
    {
        $arabic = ['٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'];
        $english = range(0, 9);

        return Str::replace($arabic, $english, $number);
    }
}
