<?php

namespace App\Http\Requests;

use App\Rules\FilterOperatorRule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;

class UpdateSKUListCriteriaRequest extends ResourceRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'criteria' => ['required', 'array', new FilterOperatorRule()],
            'criteria.*.type' => 'required|in:condition,logical-operator',
            'criteria.*.value' => 'required',
            'criteria.*.attribute' => 'required_if:criteria.conditions.*.type,condition',
            'criteria.*.operator' => 'required_if:criteria.conditions.*.type,condition|in:greater_than_or_equal,less_than_or_equal,less_than,greater_than,equal,not_equal,in,not_in,between',
        ];
    }
}
