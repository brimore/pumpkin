<?php

namespace App\Http\Middleware;

use App\Domain\Member\Model\Member;
use Closure;
use Illuminate\Http\Request;
use Laravel\Passport\Passport;
use Laravel\Passport\Token;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Token\Parser;

class Impersonate
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $member = Member::find($request->header('MEMBER-ID'));

        // Get current token scopes.
        $bearerToken = request()->bearerToken();
        $tokenId = Configuration::forUnsecuredSigner()->parser()->parse($bearerToken)->claims()->get('jti');
        $token_scopes = Token::find($tokenId)->scopes;

        if (!$member && in_array('admins', $token_scopes))
            return abort(404, 'Member Not Synced !');

        if (in_array('admins', $token_scopes) && $member)
            Passport::actingAs($member, ['members'], 'members');

        return $next($request);
    }
}
