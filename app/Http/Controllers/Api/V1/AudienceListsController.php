<?php

namespace App\Http\Controllers\Api\V1;

use Str;
use App\Exports\AudienceListExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use App\JsonApi\V1\Members\MemberSchema;
use App\OfferConsumption\Parsers\Parser;
use LaravelJsonApi\Core\Responses\DataResponse;
use App\JsonApi\V1\AudienceLists\AffectedMembersRequest;
use LaravelJsonApi\Laravel\Http\Controllers\JsonApiController;
use App\OfferConsumption\Parsers\Factories\AudienceListFactory;

class AudienceListsController extends JsonApiController
{
    /**
     * @param MemberSchema $schema
     * @param AffectedMembersRequest $request
     *
     * @return DataResponse
     */
    public function affectedMembers(MemberSchema $schema, AffectedMembersRequest $request): DataResponse
    {
        $parser = new Parser($request->data['attributes']['config']);

        return new DataResponse(
            $parser->parse(new AudienceListFactory())->get()->paginate()
        );
    }

    /**
     * @param MemberSchema $schema
     * @param AffectedMembersRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function exportAffectedMembers(MemberSchema $schema, AffectedMembersRequest $request)
    {
        $headers = [
            'Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        ];
        $parser = new Parser($request->data['attributes']['config']);
        $builder = $parser->parse(new AudienceListFactory())->get();

        $affected_members = array_map(function ($row) {
            return [
                'code' => $row['code'],
                'name' => $row['name']
            ];
        }, $builder->get()->toArray());

        $timestamp = now()->timestamp;
        $random = Str::random(5);

        $filename = "export_affected_members/audience_list_{$timestamp}_{$random}.xlsx";
        Excel::store(new AudienceListExport($affected_members), $filename, 'public');

        return response()->json([
            'export_link' => asset(Storage::url($filename))
        ]);
    }
}
