<?php

namespace App\Http\Controllers\Api\V1;

use App\Domain\SKUList\Model\SkuList;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateSKUListCriteriaRequest;
use Illuminate\Http\JsonResponse;

class SKUListsController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param SkuList $skuList
     *
     * @return JsonResponse
     */
    public function updateCriteria(SkuList $skuList, UpdateSKUListCriteriaRequest $request): JsonResponse
    {
        $skuList->update(['criteria' => $request->data['attributes']['criteria']]);

        return response()->json(['message' => 'Criteria has been updated successfully.']);
    }
}
