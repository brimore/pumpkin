<?php

namespace App\Http\Controllers\Api\V1;

use App\Domain\ListData\Model\ListData;
use App\Domain\Offer\Model\Offer;
use App\Domain\Sheet\Model\Sheet;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSheetRequest;
use App\Imports\MembersImport;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Excel as ReaderType;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\StreamedResponse;

class SheetsController extends Controller
{
    /**
     * Download sheet from Storage
     *
     * @param StoreSheetRequest $request
     * @return StreamedResponse
     */
    public function get(StoreSheetRequest $request): StreamedResponse
    {
        return Storage::disk('public')->download('sheets', $request->file->getClientOriginalName());
    }

    /**
     * Store a newly created resource in storage.
     * @param StoreSheetRequest $request
     *
     * @return JsonResponse
     */
    public function store(StoreSheetRequest $request): JsonResponse
    {
        $sheet = Sheet::create($request->only(['file', 'type']) + ['name' => $request->file->getClientOriginalName()]);

        if ($request->type === 'Cap')
            $sheet->storeInDB($request);

        return response()->json(['sheet_id' => $sheet->id, 'message' => 'File has been uploaded successfully.']);
    }

    /**
     * Update the specified resource in storage.
     * @param StoreSheetRequest $request
     * @param Sheet $sheet
     *
     * @return JsonResponse
     */
    public function update(StoreSheetRequest $request, Sheet $sheet): JsonResponse
    {
        $sheet->update($request->only(['file', 'type']) + ['name' => $request->file->getClientOriginalName()]);

        return response()->json(['sheet_id' => $sheet->id, 'message' => 'File has been updated successfully.']);
    }
}
