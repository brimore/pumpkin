<?php

namespace App\Http\Controllers\Api\V1;

use App\Domain\Offer\Model\Offer;
use App\JsonApi\V1\Offers\DeservedOffersRequest;
use App\JsonApi\V1\Offers\ValidateGiftsRequest;
use App\JsonApi\V1\Offers\OfferSchema;
use App\OfferConsumption\Cart\Factory as CartFactory;
use App\OfferConsumption\Cart\Lib\Pipe\CsVoucherDeduction;
use App\OfferConsumption\Cart\Lib\Repo\CsVoucherRepo;
use App\OfferConsumption\DeservedOffers\FormatterFactory;
use App\OfferConsumption\InteractionGroups\InteractionGroups;
use App\OfferConsumption\Offers;
use App\OfferConsumption\Rewards\Rewards;
use App\OfferConsumption\Rewards\Setup\Stock\StockFactory;
use App\OfferConsumption\Rewards\VoucherConsumption;
use App\OfferConsumption\Steps\AudienceListStep;
use App\OfferConsumption\Steps\ConditionStep;
use Illuminate\Http\JsonResponse;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use LaravelJsonApi\Core\Responses\DataResponse;
use LaravelJsonApi\Laravel\Http\Controllers\JsonApiController;

class OffersController extends JsonApiController
{
    /**
     * @var CartFactory
     */
    private CartFactory $cartFactory;

    /**
     * @var Rewards
     */
    private Rewards $rewards;
    private InteractionGroups $interactionGroups;

    public function __construct(CartFactory $cartFactory, Rewards $rewards, InteractionGroups $interactionGroups)
    {
        $this->cartFactory = $cartFactory;
        $this->rewards = $rewards;
        $this->interactionGroups = $interactionGroups;
    }

    /**
     * @param OfferSchema $schema
     * @param DeservedOffersRequest $request
     *
     * @return DataResponse
     */
    public function deserved(OfferSchema $schema, DeservedOffersRequest $request, FormatterFactory $formatterFactory): DataResponse
    {
        $offer = new Offers();

        $audience_list = new AudienceListStep();
        $audience_list->nextStep(new ConditionStep());

        $offer->checks($audience_list);

        $cart = $this->cartFactory->make(CartFactory::SOURCE_REQUEST, $request->get('data'));

        $cart = app(Pipeline::class)
            ->send($cart)
            ->through([
                CsVoucherDeduction::class
            ])->thenReturn();

        $offers = $schema->repository()->findMany($offer->get_deserved($cart));

        $deservedOffers = new Collection();
        foreach ($offers as $offer) {
            $stock = new StockFactory();
            foreach ($offer->reward->filters as $setup) {
                $stock->createStockElement($setup)->addToStock($offer, $setup);
            }
        }

        foreach ($offers as $offer) {
            $deservedOffers->push(
                $formatterFactory->make($offer)->format($offer, $cart)
            );
        }

        $processedList = $this->interactionGroups->processOfferList($deservedOffers, $cart);

        $processedList = (new VoucherConsumption($processedList, $cart))->recalculate();

        $meta = $cart->getTotal($processedList->getEntries());
        $meta['cs_voucher_list'] = CsVoucherRepo::getProducts();
        $meta['deducted_points'] = round(CsVoucherRepo::getDeductedPoints(), 2);

        return (new DataResponse($processedList->getEntries()))->withMeta($meta);
    }

    /**
     * @param $offer
     * @param DeservedOffersRequest $request
     *
     * @return DataResponse
     */
    public function calculateGifts($offer, DeservedOffersRequest $request): DataResponse
    {
        $cart = $this->cartFactory->make(CartFactory::SOURCE_REQUEST, $request->get('data'));

        $stock = new StockFactory();
        foreach ($offer->reward->filters as $setup) {
            $stock->createStockElement($setup)->addToStock($offer, $setup);
        }

        $list = $this->rewards->paginateGifts($offer, $cart, $request->get('page', 1));

        return new DataResponse($list['payload']);
    }

    /**
     * @param ValidateGiftsRequest $request
     *
     * @return JsonResponse
     */
    public function validateGifts(ValidateGiftsRequest $request): JsonResponse
    {
        $cart = $this->cartFactory->make(CartFactory::SOURCE_REQUEST, $request->get('data')['cart']);

        foreach ($request->get('data')['gifts'] as $gift) {
            $selected_gift_ids = array_map(fn($gift) => $gift['id'], $gift['variants']);

            $offer = Offer::whereUuid($gift['offerId'])->get()->first();
            $offer_gift_ids = $this->rewards->listGiftIds($offer, $cart);

            $actual_gifts = array_intersect($selected_gift_ids, $offer_gift_ids['payload']);
            if (count($actual_gifts) !== count($selected_gift_ids)) {
                return response()->json(['valid_gifts' => false]);
            }
        }

        return response()->json(['valid_gifts' => true]);
    }

    public function disableAll(): JsonResponse
    {
        DB::table('offers')->update(['deleted_at' => now()]);

        return response()->json(["message" => "All offers has been disabled"]);

    }
}
