<?php

namespace App\Http\Controllers\Api\V1;

use App\Domain\Image\Model\Image;
use App\Domain\Offer\Model\Offer;
use App\Domain\Sheet\Model\Sheet;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreImageRequest;
use App\Http\Requests\StoreSheetRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ImagesController extends Controller
{
    /**
     * Download sheet from Storage
     *
     * @param StoreImageRequest $request
     * @return StreamedResponse
     */
    public function get(StoreImageRequest $request): StreamedResponse
    {
        return Storage::disk('public')->download('images', $request->image->getClientOriginalName());
    }
    /**
     * Store a newly created resource in storage.
     * @param StoreImageRequest $request
     *
     * @return JsonResponse
     */
    public function store(StoreImageRequest $request): JsonResponse
    {
        $offer = Offer::whereUuid($request->get('offer_id'))->first();
        $image = Image::create($request->only(['image', 'type']) +
            ['name' => $request->image->getClientOriginalName(), 'size' => $request->image->getSize()] +
        ['offer_id' => $offer->id ?? null]);

        return response()->json(['image_id' => $image->id, 'message' => 'Image has been uploaded successfully.']);
    }

    /**
     * Update the specified resource in storage.
     * @param StoreImageRequest $request
     * @param Image $image
     *
     * @return JsonResponse
     */
    public function update(StoreImageRequest $request, Image $image): JsonResponse
    {
        $offer = Offer::whereUuid($request->get('offer_id'))->first();
        $image->update($request->only(['image', 'type']) +
            ['name' => $request->image->getClientOriginalName(), 'size' => $request->image->getSize()] +
        [['offer_id' => $offer->id ?? null]]);

        return response()->json(['image_id' => $image->id, 'message' => 'Image has been updated successfully.']);
    }
}
