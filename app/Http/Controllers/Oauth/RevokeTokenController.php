<?php

namespace App\Http\Controllers\Oauth;

use App\Domain\Oauth\TokenManager;
use App\Http\Controllers\Controller;
use App\Http\Requests\Oauth\RevokeTokenRequest;

class RevokeTokenController extends Controller
{
    /**
     * @var TokenManager
     */
    private $tokenManager;

    public function __construct(TokenManager $tokenManager)
    {
        $this->tokenManager = $tokenManager;
    }

    public function revokeAccessToken(RevokeTokenRequest $request)
    {
        $status = $this->tokenManager->revokeAccessToken($request->get('token'));

        return $status ? response()->json(true) : response()->json(false, 422);
    }
}
