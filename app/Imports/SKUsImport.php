<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class SKUsImport implements ToCollection
{
    /**
     * @param Collection $rows
     */
    public function collection(Collection $rows): Collection
    {
        return $rows->except(0)->pluck(0);
    }
}
