<?php

namespace App\Imports;

use App\Models\AudienceListSheet;
use Maatwebsite\Excel\Concerns\ToModel;

class SheetImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new AudienceListSheet([
            //
        ]);
    }
}
