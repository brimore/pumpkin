<?php

namespace App\Observers;


use App\Domain\Condition\Model\Condition;

class ConditionObserver
{
    /**
     * Handle the Condition "created" event.
     *
     * @param Condition $condition
     * @return void
     */
    public function created(Condition $condition)
    {
        //
    }

    /**
     * Handle the Condition "updated" event.
     *
     * @param Condition $condition
     * @return void
     */
    public function updated(Condition $condition)
    {
        //
    }

    /**
     * Handle the Condition "deleted" event.
     *
     * @param Condition $condition
     * @return void
     */
    public function deleted(Condition $condition)
    {
        //
    }

    /**
     * Handle the Condition "restored" event.
     *
     * @param Condition $condition
     * @return void
     */
    public function restored(Condition $condition)
    {
        //
    }

    /**
     * Handle the Condition "force deleted" event.
     *
     * @param Condition $condition
     * @return void
     */
    public function forceDeleted(Condition $condition)
    {
        //
    }
}
