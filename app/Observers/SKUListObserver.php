<?php

namespace App\Observers;

use App\Domain\SKUList\Events\VirtualSkuCreated;
use App\Domain\SKUList\Model\SkuList;
use Illuminate\Support\Arr;

class SKUListObserver
{
    /**
     * Handle the SKUList "saved" event.
     *
     * @param  SKUList  $sKUList
     * @return void
     */
    public function saved(SKUList $sKUList): void
    {
//        $is_virtual_sku = Arr::first($sKUList->toArray()['criteria'], function ($condition)  {
//            return $condition['type'] === 'condition' && $condition['attribute'] === 'virtual_sku';
//        });
//       if(!empty($is_virtual_sku)){
//           event(new VirtualSkuCreated($sKUList));
//       }
    }

}
