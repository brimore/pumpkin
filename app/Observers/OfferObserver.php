<?php

namespace App\Observers;


use App\Domain\Offer\Events\BundleDisabled;
use App\Domain\Offer\Model\Offer;
use Carbon\Carbon;

class OfferObserver
{
    /**
     * Handle the Offer "creating" event.
     *
     * @param Offer $offer
     * @return void
     */
    public function creating(Offer $offer): void
    {
//        $offer->code = $offer->toArray()['title']['ar'].'-'.(Carbon::now())->toDateString();
        $offer->code = $offer->toArray()['title']['ar'].'-'.(Carbon::now());

    }
    /**
     * Handle the Offer "created" event.
     *
     * @param Offer $offer
     * @return void
     */
    public function created(Offer $offer): void
    {
        //
    }

    /**
     * Handle the Offer "updated" event.
     *
     * @param Offer $offer
     * @return void
     */
    public function updating(Offer $offer): void
    {
        if($offer->isDirty('deleted_at'))
        {
            event(new BundleDisabled($offer));
        }

    }

    /**
     * Handle the Offer "deleted" event.
     *
     * @param Offer $offer
     * @return void
     */
    public function deleted(Offer $offer): void
    {
        //
    }

    /**
     * Handle the Offer "restored" event.
     *
     * @param Offer $offer
     * @return void
     */
    public function restored(Offer $offer): void
    {
        //
    }

    /**
     * Handle the Offer "force deleted" event.
     *
     * @param Offer $offer
     * @return void
     */
    public function forceDeleted(Offer $offer): void
    {
        //
    }
}
