<?php

namespace App\Observers;


use App\Domain\Reward\Model\Reward;
use App\Domain\SKUList\Events\VirtualSkuCreated;
use App\Domain\SKUList\Model\SkuList;
use Illuminate\Support\Arr;

class RewardObserver
{
    /**
     * Handle the Reward "saved" event.
     *
     * @param Reward $reward
     * @return void
     */
    public function saved(Reward $reward)
    {
        $offer_uuid = $reward->offer()->first()->uuid;
        $filter = Arr::first($reward->filters, function ($condition) {
            return ($condition['type'] === 'filter' && $condition['reward'] === 'DISCOUNT');
        });
        if (empty($filter)) {
            $filter_group = Arr::first($reward->filters, function ($condition) {
                return ($condition['type'] === 'filter-group');
            });

            if (empty($filter_group) && !isset($filter_group['filters'])) {
                return;
            }
            $filter = Arr::first($filter_group['filters'], function ($condition) {
                return ($condition['type'] === 'filter' && $condition['reward'] === 'DISCOUNT');
            });
            $bundle = Arr::first($filter['conditions'], function ($condition) {
                return ($condition['type'] === 'condition' && $condition['attribute'] === 'sku_list');
            });
            $virtual_sku = SKUList::find((int)$bundle['value']);
            $is_virtual_sku = Arr::first($virtual_sku->toArray()['criteria'], function ($condition) {
                return $condition['type'] === 'condition' && $condition['attribute'] === 'virtual_sku';
            });
            if (!empty($is_virtual_sku)) {
                event(new VirtualSkuCreated($virtual_sku, $filter['conditions'], $offer_uuid));
            }
            return;

        }
        $bundle = Arr::first($filter['conditions'], function ($condition) {
            return ($condition['type'] === 'condition' && $condition['attribute'] === 'sku_list');
        });
        if (isset($bundle['value'])) {
            $virtual_sku = SKUList::find((int)$bundle['value']);
            $is_virtual_sku = Arr::first($virtual_sku->toArray()['criteria'], function ($condition) {
                return $condition['type'] === 'condition' && $condition['attribute'] === 'virtual_sku';
            });
            if (!empty($is_virtual_sku)) {
                event(new VirtualSkuCreated($virtual_sku, $filter['conditions'], $offer_uuid));
            }
        }
    }

    /**
     * Handle the Reward "updated" event.
     *
     * @param Reward $reward
     * @return void
     */
    public function updated(Reward $reward)
    {
        //
    }

    /**
     * Handle the Reward "deleted" event.
     *
     * @param Reward $reward
     * @return void
     */
    public function deleted(Reward $reward)
    {
        //
    }

    /**
     * Handle the Reward "restored" event.
     *
     * @param Reward $reward
     * @return void
     */
    public function restored(Reward $reward)
    {
        //
    }

    /**
     * Handle the Reward "force deleted" event.
     *
     * @param Reward $reward
     * @return void
     */
    public function forceDeleted(Reward $reward)
    {
        //
    }
}
