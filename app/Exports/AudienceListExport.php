<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class AudienceListExport implements WithHeadings, WithMapping, ShouldAutoSize, FromArray
{
    protected array $members;

    public function __construct(array $members)
    {
        $this->members = $members;
    }

    public function headings(): array
    {
        return [
            "code",
            "name"
        ];
    }

    public function map($row): array
    {
        return [
            $row['code'],
            $row['name']
        ];
    }

    public function array(): array
    {
        return $this->members;
    }
}
