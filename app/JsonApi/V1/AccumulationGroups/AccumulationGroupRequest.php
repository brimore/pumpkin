<?php

namespace App\JsonApi\V1\AccumulationGroups;

use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;

class AccumulationGroupRequest extends ResourceRequest
{
    /**
     * Get the validation rules for the resource.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
        ];
    }
}
