<?php

namespace App\JsonApi\V1;

use LaravelJsonApi\Core\Server\Server as BaseServer;

class Server extends BaseServer
{

    /**
     * The base URI namespace for this server.
     *
     * @var string
     */
    protected string $baseUri = '/api/v1';

    /**
     * Bootstrap the server when it is handling an HTTP request.
     *
     * @return void
     */
    public function serving(): void
    {
        //
    }

    /**
     * Get the server's list of schemas.
     *
     * @return array
     */
    protected function allSchemas(): array
    {
        return [
            Offers\OfferSchema::class,
            EventTypes\EventTypeSchema::class,
            Events\EventSchema::class,
            AudienceLists\AudienceListSchema::class,
            Conditions\ConditionSchema::class,
            ConditionTypes\ConditionTypeSchema::class,
            Rewards\RewardSchema::class,
            RewardTypes\RewardTypeSchema::class,
            EventTypes\EventTypeSchema::class,
            Events\EventSchema::class,
            Members\MemberSchema::class,
            Categories\CategorySchema::class,
            Brands\BrandSchema::class,
            Products\ProductSchema::class,
            ProductVariants\ProductVariantSchema::class,
            SkuLists\SkuListSchema::class,
            InteractionGroups\InteractionGroupSchema::class,
            InteractionGroupOffers\InteractionGroupOfferSchema::class,
            DeservedOffers\DeservedOfferSchema::class,
            AccumulationGroups\AccumulationGroupSchema::class,
        ];
    }
}
