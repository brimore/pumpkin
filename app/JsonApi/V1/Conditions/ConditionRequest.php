<?php

namespace App\JsonApi\V1\Conditions;

use App\Rules\FilterOperatorRule;
use App\Rules\ConditionsValidationRule;
use LaravelJsonApi\Validation\Rule as JsonApiRule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;

class ConditionRequest extends ResourceRequest
{

    /**
     * Get the validation rules for the resource.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'criteria' => ['nullable', 'array', 'min:1', new FilterOperatorRule()],
            'criteria.*.type' => 'required|in:filter,logical-operator,filter-group',
            'criteria.*.value' => 'required_if:criteria.*.type,logical-operator|in:AND,OR,NOT',
            'criteria.*.condition' => 'required_if:criteria.*.type,filter|in:ACHIEVE_POINTS,BUY_SKU',
            'criteria.*.conditions' => ['required_if:criteria.*.type,filter', new FilterOperatorRule()],
            'criteria.*.conditions.*.type' => ['required', new ConditionsValidationRule()],
            'criteria.*.conditions.*.value' => ['required_if:criteria.*.condition,ACHIEVE_POINTS,BUY_SKU'],
            'criteria.*.conditions.*.attribute' => ['required_if:criteria.*.conditions.*.type,condition', new ConditionsValidationRule()],
            'criteria.*.conditions.*.operator' => ['required_if:criteria.*.conditions.*.type,condition', new ConditionsValidationRule()],
            // Filter Group Configuration Validation.
            'criteria.*.criteria' => ['required_if:criteria.*.type,filter-group', 'array', new FilterOperatorRule()],
            'criteria.*.criteria.*.type' => 'required|in:filter,logical-operator',
            'criteria.*.criteria.*.condition' => 'required_if:criteria.*.criteria.*.type,filter|in:ACHIEVE_POINTS,BUY_SKU',
            'criteria.*.criteria.*.conditions' => ['required_if:criteria.*.criteria.*.type,filter', new FilterOperatorRule()],
            'criteria.*.criteria.*.conditions.*.type' => ['required', new ConditionsValidationRule()],
            'criteria.*.criteria.*.conditions.*.value' => ['required_if:criteria.*.criteria.*.condition,ACHIEVE_POINTS,BUY_SKU'],
            'criteria.*.criteria.*.conditions.*.attribute' => ['required_if:criteria.*.criteria.*.conditions.*.type,condition', new ConditionsValidationRule()],
            'criteria.*.criteria.*.conditions.*.operator' => ['required__if:criteria.*.criteria.*.conditions.*.type,condition', new ConditionsValidationRule()],
            'offer' => JsonApiRule::toOne(),
        ];
    }
}
