<?php

namespace App\JsonApi\V1\ProductVariants;

use App\Domain\Offer\Model\Offer;
use App\Domain\ProductVariant\Model\ProductVariant;
use Illuminate\Validation\Rule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

class ProductVariantRequest extends ResourceRequest
{

    /**
     * Get the validation rules for the resource.
     *
     * @return array
     */
    public function rules(): array
    {
        $uniqueCode = Rule::unique('offers');

        /** @var ProductVariant|null $product_variant */
        if ($product_variant= $this->model()) $uniqueCode->ignore($product_variant);
        return [
            'name' => 'required',
            'price' => 'required',
            'code' => ['required', $uniqueCode],
            'product_id' => 'required',
            'product' => JsonApiRule::toOne(),
        ];
    }

}
