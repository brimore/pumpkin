<?php

namespace App\JsonApi\V1\Members;

use App\Domain\Member\Model\Member;
use Illuminate\Validation\Rule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;

class MemberRequest extends ResourceRequest
{

    /**
     * Get the validation rules for the resource.
     *
     * @return array
     */
    public function rules(): array
    {
        $uniqueCode = Rule::unique('offers');

        /** @var Member|null $member */
        if ($member = $this->model()) $uniqueCode->ignore($member);

        return [
            'member_code' => ['required', $uniqueCode],
            'name' => 'required',
        ];
    }

}
