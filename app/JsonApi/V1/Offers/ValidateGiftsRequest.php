<?php

namespace App\JsonApi\V1\Offers;

use Illuminate\Foundation\Http\FormRequest;

class ValidateGiftsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            // Cart validation.
            'data' => 'required',
            'data.cart' => 'required',
            'data.cart.variants' => 'required|array',
            'data.cart.variants.*.variantId' => 'required|exists:product_variants,id',
            'data.cart.variants.*.quantity' => 'required|numeric|integer',
            'data.cart.variants.*.price' => 'required|numeric|integer',
            'data.cart.subtract' => 'required|numeric|integer',
            'data.cart.add' => 'required|numeric|integer',
            // Gifts validation.
            'data.gifts' => 'required|array',
            'data.gifts.*.offerId' => 'required|exists:offers,uuid',
            'data.gifts.*.variants' => 'required|array',
            'data.gifts.*.variants.*.id' => 'required|exists:product_variants,id',
            'data.gifts.*.variants.*.quantity' => 'required|numeric|integer',
        ];
    }

    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            // Cart messages.
            'data.cart.variants.*.id.required' => 'The selected variant is invalid.',
            'data.cart.variants.*.variantId.exists' => 'The selected variant is invalid.',
            'data.cart.variants.*.quantity.required' => 'The selected quantity is invalid.',
            'data.cart.variants.*.quantity.numeric' => 'The selected quantity is invalid.',
            'data.cart.variants.*.quantity.integer' => 'The selected quantity is invalid.',
            'data.cart.variants.*.price.required' => 'The selected price is invalid.',
            'data.cart.variants.*.price.numeric' => 'The selected price is invalid.',
            'data.cart.variants.*.price.integer' => 'The selected price is invalid.',
            // Gifts messages.
            'data.gifts.*.variants.*.id.required' => 'The selected variant is invalid.',
            'data.gifts.*.variants.*.id.exists' => 'The selected variant is invalid.',
            'data.gifts.*.variants.*.quantity.required' => 'The selected quantity is invalid.',
            'data.gifts.*.variants.*.quantity.numeric' => 'The selected quantity is invalid.',
            'data.gifts.*.variants.*.quantity.integer' => 'The selected quantity is invalid.'
        ];
    }
}
