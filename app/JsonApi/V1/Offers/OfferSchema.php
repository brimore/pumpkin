<?php

namespace App\JsonApi\V1\Offers;

use App\Domain\Common\Translations;
use App\Domain\Offer\Model\Offer;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use LaravelJsonApi\Core\Json\Json;
use LaravelJsonApi\Eloquent\Contracts\Paginator;
use LaravelJsonApi\Eloquent\Fields\ArrayHash;
use LaravelJsonApi\Eloquent\Fields\ArrayList;
use LaravelJsonApi\Eloquent\Fields\Boolean;
use LaravelJsonApi\Eloquent\Fields\DateTime;
use LaravelJsonApi\Eloquent\Fields\ID;
use LaravelJsonApi\Eloquent\Fields\Map;
use LaravelJsonApi\Eloquent\Fields\Number;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsTo;
use LaravelJsonApi\Eloquent\Fields\Relations\HasOne;
use LaravelJsonApi\Eloquent\Fields\SoftDelete;
use LaravelJsonApi\Eloquent\Fields\Str;
use LaravelJsonApi\Eloquent\Filters\Scope;
use LaravelJsonApi\Eloquent\Filters\WhereDoesntHave;
use LaravelJsonApi\Eloquent\Filters\WhereIdIn;
use LaravelJsonApi\Eloquent\Filters\WithTrashed;
use LaravelJsonApi\Eloquent\Pagination\PagePagination;
use LaravelJsonApi\Eloquent\Schema;
use LaravelJsonApi\Eloquent\SoftDeletes;
use MyCLabs\Enum\Enum;

class OfferSchema extends Schema
{
    use SoftDeletes;

    /**
     * The model the schema corresponds to.
     *
     * @var string
     */
    public static string $model = Offer::class;

    /**
     * Default pagination config
     *
     * @var array|int[]|null
     */
    protected ?array $defaultPagination = ['limit' => 15];

    /**
     * The maximum include path depth.
     *
     * @var int
     */
    protected int $maxDepth = 3;

    /**
     * Get the resource fields.
     *
     * @return array
     */
    public function fields(): array
    {

        return [
            ID::make('uuid')->uuid(),
            Str::make('legacy_id', 'id')->extractUsing(
                static fn($model) => 5021
            ),
            Str::make('code'),
            Translations::make('title'),
            Str::make('brief'),
            Str::make('image')->on('image'),
            DateTime::make('startsAt')->sortable(),
            DateTime::make('endsAt')->sortable(),
            Str::make('isActive'),
            ArrayList::make('multiples'),
            ArrayList::make('tags'),
            DateTime::make('createdAt')->sortable()->readOnly(),
            DateTime::make('updatedAt')->sortable()->readOnly(),
            ArrayHash::make('deserved_reward')->extractUsing(static fn($model) => $model->deserved_reward),
            Boolean::make('editable')->extractUsing(static function ($model) {
                return false;
            }),
            SoftDelete::make('deletedAt'),
            HasOne::make('condition'),
            BelongsTo::make('audience-list', 'audience_list'),
            HasOne::make('reward'),
            BelongsTo::make('accumulation-group'),
        ];
    }

    /**
     * Get the resource filters.
     *
     * @return array
     */
    public function filters(): array
    {
        return [
            WhereIdIn::make($this),
            Scope::make('status', 'status'),
            Scope::make('tags', 'filter'),
            Scope::make('name', 'title'),
            Scope::make('audience-list', 'audienceList'),
            WithTrashed::make('with-trashed')
        ];
    }

    /**
     * Get the resource paginator.
     *
     * @return Paginator|null
     */
    public function pagination(): ?Paginator
    {
        return PagePagination::make()->withoutNestedMeta();
    }

    /**
     * Determine if the resource is authorizable.
     *
     *
     * @return bool
     */
    public function authorizable(): bool
    {
        return false;
    }
}
