<?php

namespace App\JsonApi\V1\Offers;

use Illuminate\Foundation\Http\FormRequest;

class ValidateOfferSelectionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            // Cart validation.
            'data' => 'required',
            'data.attributes.cart' => 'required|array',
            'data.attributes.cart.*.variantId' => 'required|exists:product_variants,id',
            'data.attributes.cart.*.quantity' => 'required|numeric|integer',
            'data.attributes.cart.*.price' => 'required|numeric|integer',
            'data.attributes.subtract' => 'required|numeric|integer',
            'data.attributes.add' => 'required|numeric|integer',
            // Gifts validation.
            'data.attributes.offers_selection' => 'required|array',
            'data.attributes.offers_selection.*.groupId' => 'required|exists:interaction_groups,uuid',
            'data.attributes.offers_selection.*.offers' => 'required|array',
            'data.attributes.offers_selection.*.offers.*.id' => 'required|exists:offers,uuid'
        ];
    }

    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            // Cart messages.
            'data.attributes.cart.*.variantId.required' => 'The selected variant is invalid.',
            'data.attributes.cart.*.variantId.exists' => 'The selected variant is invalid.',
            'data.attributes.cart.*.quantity.required' => 'The selected quantity is invalid.',
            'data.attributes.cart.*.quantity.numeric' => 'The selected quantity is invalid.',
            'data.attributes.cart.*.quantity.integer' => 'The selected quantity is invalid.',
            'data.attributes.cart.*.price.required' => 'The selected price is invalid.',
            'data.attributes.cart.*.price.numeric' => 'The selected price is invalid.',
            'data.attributes.cart.*.price.integer' => 'The selected price is invalid.',
            // Gifts messages.
            'data.attributes.offers_selection.*.offers.*.id.required' => 'The selected offer is invalid.',
            'data.attributes.offers_selection.*.offers.*.id.exists' => 'The selected offer is invalid.'
        ];
    }
}
