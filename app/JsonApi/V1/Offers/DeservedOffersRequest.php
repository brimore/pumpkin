<?php

namespace App\JsonApi\V1\Offers;

use Illuminate\Foundation\Http\FormRequest;

class DeservedOffersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'data' => 'required|array',
            'data.attributes' => 'required',
            'data.attributes.cart' => 'required|array',
            'data.attributes.cart.*.variantId' => 'required|exists:product_variants,id',
            'data.attributes.cart.*.quantity' => 'required|numeric|integer|min:1',
            'data.attributes.cart.*.price' => 'required|numeric',
            'data.attributes.subtract' => 'required|numeric',
            'data.attributes.add' => 'required|numeric'
        ];
    }

    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            'data.attributes.cart.*.variantId.required' => 'The selected variant is invalid.',
            'data.attributes.cart.*.variantId.exists' => 'The selected variant is invalid.',
            'data.attributes.cart.*.quantity.required' => 'The selected quantity is invalid.',
            'data.attributes.cart.*.quantity.numeric' => 'The selected quantity is invalid.',
            'data.attributes.cart.*.quantity.integer' => 'The selected quantity is invalid.',
            'data.attributes.cart.*.price.required' => 'The selected price is invalid.',
            'data.attributes.cart.*.price.numeric' => 'The selected price is invalid.',
            'data.attributes.cart.*.price.integer' => 'The selected price is invalid.',
        ];
    }
}
