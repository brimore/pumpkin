<?php

namespace App\JsonApi\V1\Offers;

use App\Domain\Offer\Model\Offer;
use Illuminate\Validation\Rule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

class OfferRequest extends ResourceRequest
{

    /**
     * Get the validation rules for the resource.
     *
     * @return array
     */
    public function rules(): array
    {
        $uniqueCode = Rule::unique('offers');

        /** @var Offer|null $offer */
        if ($offer = $this->model()) $uniqueCode->ignore($offer);

        return [
            'title.ar' => 'required',
            'title.en' => 'required',
            'startsAt' => 'required|date',
            'endsAt' => 'required|date|after:startsAt',
            'isActive' => 'required|in:active,inactive,ended',
            'multiples' => 'nullable|array',
            'multiples.*.type' => 'required|in:cap,order_amount,consumed_offer_count,sheet,offer_consumption,offer_consumption_per_member',
            'multiples.*.conditions.value' => 'required_if:multiples.*.type,sheet|required_if:multiples.*.type,cap|numeric|gt:0',
            'multiples.*.conditions.offer_id' => 'required_if:multiples.*.type,consumed_offer_count',
            'multiples.*.conditions.formula' => 'required_if:multiples.*.type,order_amount|string',
            'multiples.*.conditions.operator' => 'required_if:multiples.*.type,order_amount|in:equal',
            'multiples.*.conditions.from' => 'required_if:multiples.*.type,order_amount,consumed_offer_count|date',
            'multiples.*.conditions.to' => 'required_if:multiples.*.type,order_amount,consumed_offer_count|date|after:multiples.*.conditions.from',
            'tags' => 'nullable|array',
            'brief' => 'nullable|string',
            'deletedAt' => ['nullable', JsonApiRule::dateTime()],
            'audience-list' => JsonApiRule::toOne(),
            'condition' => JsonApiRule::toOne(),
            'reward' => JsonApiRule::toOne(),
            'accumulation-group' => JsonApiRule::toOne(),
        ];
    }

}
