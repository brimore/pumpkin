<?php

namespace App\JsonApi\V1\Products;

use Illuminate\Validation\Rule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

class ProductRequest extends ResourceRequest
{

    /**
     * Get the validation rules for the resource.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'image' => 'required',
            'category_id' => ['required','nullable'],
            'brand_id' => ['required','nullable'],
            'category' => JsonApiRule::toOne(),
            'brand' => JsonApiRule::toOne(),
            'product-variants' => JsonApiRule::toMany(),
        ];
    }

}
