<?php

namespace App\JsonApi\V1\InteractionGroupOffers;

use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

class InteractionGroupOfferRequest extends ResourceRequest
{
    /**
     * Get the validation rules for the resource.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'order' => ['required', 'numeric', 'min:0'],
            'offer' => ['required', JsonApiRule::toOne()],
            'interaction_group' => ['required', JsonApiRule::toOne()],
        ];
    }
}
