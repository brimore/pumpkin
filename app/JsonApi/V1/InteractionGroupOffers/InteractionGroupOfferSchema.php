<?php

namespace App\JsonApi\V1\InteractionGroupOffers;

use App\Domain\InteractionGroup\Model\InteractionGroupOffer;
use LaravelJsonApi\Eloquent\Fields\ID;
use LaravelJsonApi\Eloquent\Fields\Number;
use LaravelJsonApi\Eloquent\Fields\Relations\BelongsTo;
use LaravelJsonApi\Eloquent\Schema;

class InteractionGroupOfferSchema extends Schema
{

    /**
     * The model the schema corresponds to.
     *
     * @var string
     */
    public static string $model = InteractionGroupOffer::class;

    /**
     * Get the resource fields.
     *
     * @return array
     */
    public function fields(): array
    {
        return [
            ID::make('uuid')->uuid(),
            BelongsTo::make('offer'),
            BelongsTo::make('interaction_group'),
            Number::make('order')
        ];
    }

    /**
     * @return bool
     */
    public function authorizable(): bool
    {
        return false;
    }
}
