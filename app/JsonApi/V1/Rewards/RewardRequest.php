<?php

namespace App\JsonApi\V1\Rewards;

use App\Domain\Reward\Model\Reward;
use App\Rules\FilterOperatorRule;
use App\Rules\ConditionsValidationRule;
use Illuminate\Validation\Rule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

class RewardRequest extends ResourceRequest
{

    /**
     * Get the validation rules for the resource.
     *
     * @return array
     */
    public function rules(): array
    {
        $unique = Rule::unique('rewards', 'offer_id');

        /** @var Reward|null $reward */
        if ($reward = $this->model()) {
            $unique->ignore($reward);
        }
        return [
            'filters' => ['nullable', 'array', 'min:1', new FilterOperatorRule()],
            'filters.*.type' => 'required|in:filter,logical-operator,filter-group',
            'filters.*.value' => 'required_if:filters.*.type,logical-operator|in:AND,OR,NOT',
            'filters.*.reward' => 'required_if:filters.*.type,filter|in:GET_SKU,SELECT_SKU,VOUCHER,DISCOUNT,MEMBERSHIP_FEES,SHIPPING_FEES,VOUCHER_SKU,CASHBACK_VOUCHER,CASHBACK_DISCOUNT,GET_SAME_PURCHASED_SKU',
            'filters.*.conditions' => ['required_if:filters.*.type,filter', new FilterOperatorRule()],
            'filters.*.conditions.*.type' => ['required', new ConditionsValidationRule()],
            'filters.*.conditions.*.value' => ['required', new ConditionsValidationRule()],
            'filters.*.conditions.*.attribute' => ['required_if:filters.*.conditions.*.type,condition', new ConditionsValidationRule()],
            'filters.*.conditions.*.operator' => ['required_if:filters.*.conditions.*.type,condition', new ConditionsValidationRule()],
            // Filter Group Configuration Validation.
            'filters.*.filters' => ['required_if:filters.*.type,filter-group', 'array', 'min:1', new FilterOperatorRule()],
            'filters.*.filters.*.type' => 'required|in:filter,logical-operator,filter-group',
            'filters.*.filters.*.reward' => 'required_if:filters.*.filters.*.type,filter|in:GET_SKU,SELECT_SKU,VOUCHER,DISCOUNT,MEMBERSHIP_FEES,SHIPPING_FEES,VOUCHER_SKU,CASHBACK_VOUCHER,CASHBACK_DISCOUNT,GET_SAME_PURCHASED_SKU',
            'filters.*.filters.*.conditions' => ['required_if:filters.*.filters.*.type,filter', new FilterOperatorRule()],
            'filters.*.filters.*.conditions.*.type' => ['required', new ConditionsValidationRule()],
            'filters.*.filters.*.conditions.*.value' => ['required', new ConditionsValidationRule()],
            'filters.*.filters.*.conditions.*.attribute' => ['required_if:filters.*.filters.*.conditions.*.type,condition', new ConditionsValidationRule()],
            'filters.*.filters.*.conditions.*.operator' => ['required_if:filters.*.filters.*.conditions.*.type,condition', new ConditionsValidationRule()],
            'offer' => JsonApiRule::toOne(),
        ];
    }

}
