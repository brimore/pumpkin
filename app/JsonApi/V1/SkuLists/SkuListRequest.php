<?php

namespace App\JsonApi\V1\SkuLists;

use App\Rules\FilterOperatorRule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;
use LaravelJsonApi\Validation\Rule as JsonApiRule;
use App\Rules\LocalizationRule;

class SkuListRequest extends ResourceRequest
{
    /**
     * Get the validation rules for the resource.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['required', new LocalizationRule('sku_lists', $this->audience_list)],
            'criteria' => ['required', 'array', new FilterOperatorRule()],
            'criteria.*.type' => 'required|in:condition,logical-operator',
            'criteria.*.value' => 'required',
            'criteria.*.attribute' => 'required_if:criteria.conditions.*.type,condition',
            'criteria.*.operator' => 'required_if:criteria.conditions.*.type,condition|in:greater_than_or_equal,less_than_or_equal,less_than,greater_than,equal,not_equal,in,not_in,between',
            'condition' => 'nullable',
            'condition.type' => 'required_with:condition|in:quantity,points,percentage',
            'condition.value' => 'required_with:condition.type',
            'same_as_purchase' => 'required|boolean',
            'same_skus' => 'required|boolean',
        ];
    }
}
