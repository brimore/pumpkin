<?php

namespace App\JsonApi\V1\InteractionGroups;

use App\Domain\InteractionGroup\Model\InteractionGroup;
use Illuminate\Validation\Rule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;

class InteractionGroupRequest extends ResourceRequest
{
    /**
     * Get the validation rules for the resource.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title.en' => 'nullable',
            'title.ar' => 'nullable',
            'description.en' => 'nullable',
            'description.ar' => 'nullable',
            'selection_strategy' => ['required', Rule::in(InteractionGroup::getSelectionStrategies())],
            'limit' => ['nullable', 'numeric'],
        ];
    }
}
