<?php

namespace App\JsonApi\V1\InteractionGroups;

use App\Domain\Common\Translations;
use App\Domain\InteractionGroup\Model\InteractionGroup;
use LaravelJsonApi\Eloquent\Contracts\Paginator;
use LaravelJsonApi\Eloquent\Fields\ID;
use LaravelJsonApi\Eloquent\Fields\Number;
use LaravelJsonApi\Eloquent\Fields\DateTime;
use LaravelJsonApi\Eloquent\Fields\Relations\HasMany;
use LaravelJsonApi\Eloquent\Fields\Str;
use LaravelJsonApi\Eloquent\Pagination\PagePagination;
use LaravelJsonApi\Eloquent\Schema;

class InteractionGroupSchema extends Schema
{
    /**
     * The model the schema corresponds to.
     *
     * @var string
     */
    public static string $model = InteractionGroup::class;

    /**
     * Default pagination config
     *
     * @var array|int[]|null
     */
    protected ?array $defaultPagination = ['limit' => 15];

    /**
     * Get the resource fields.
     *
     * @return array
     */
    public function fields(): array
    {
        return [
            ID::make('uuid')->uuid(),
            Translations::make('title'),
            Translations::make('description'),
            Str::make('selection_strategy'),
            DateTime::make('created_at')->sortable(),
            Number::make('limit'),
            HasMany::make('interaction_group_offers'),
        ];
    }

    /**
     * Get the resource paginator.
     *
     * @return Paginator|null
     */
    public function pagination(): ?Paginator
    {
        return PagePagination::make()->withoutNestedMeta();
    }

    /**
     * @return bool
     */
    public function authorizable(): bool
    {
        return false;
    }

    public function includePaths(): iterable
    {
        return ['interaction_group_offers', 'interaction_group_offers.offer'];
    }
}
