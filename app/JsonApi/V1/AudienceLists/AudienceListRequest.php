<?php

namespace App\JsonApi\V1\AudienceLists;

use App\Rules\FilterOperatorRule;
use App\Rules\ConditionsValidationRule;
use Illuminate\Validation\Rule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;
use LaravelJsonApi\Validation\Rule as JsonApiRule;
use App\Rules\LocalizationRule;

class AudienceListRequest extends ResourceRequest
{
    /**
     * Get the validation rules for the resource.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
//            'name' => ['required', new LocalizationRule('audience_lists', $this->audience_list)],
            'name.en' => 'nullable|string|min:1|max:255',
            'name.ar' => 'nullable|string|min:1|max:255',
            'config' => ['nullable', 'array', 'min:0', new FilterOperatorRule()],
            'config.*.type' => 'required|in:filter,logical-operator,filter-group',
            'config.*.event' => 'required_if:config.*.type,filter|in:SIGNED_UP,PLACED_ORDER' /* |exists:event_types,name */,
            'config.*.from' => 'nullable|date',
            'config.*.to' => 'nullable|date|after:config.*.from',
            'config.*.conditions' => ['nullable', 'array', 'min:0' ,new FilterOperatorRule()],
            'config.*.conditions.*.type' => ['nullable', new ConditionsValidationRule()],
            'config.*.conditions.*.attribute' => ['required_if:config.*.conditions.*.type,condition', new ConditionsValidationRule()],
            'config.*.conditions.*.value' => ['required_if:config.*.conditions.*.attribute,total_points', new ConditionsValidationRule()],
            'config.*.conditions.*.operator' => ['required_if:config.*.conditions.*.type,condition', new ConditionsValidationRule()],
            // Filter Group Configuration Validation.
            'config.*.config' => ['required_if:config.*.type,filter-group', 'array', new FilterOperatorRule()],
            'config.*.config.*.type' => 'required|in:filter,logical-operator,filter-group',
            'config.*.config.*.event' => 'required_if:config.*.type,filter|in:SIGNED_UP,PLACED_ORDER' /* |exists:event_types,name */,
            'config.*.config.*.from' => 'nullable|date',
            'config.*.config.*.to' => 'nullable|date|after:config.*.config.*.from',
            'config.*.config.*.conditions' => ['nullable', 'array', new FilterOperatorRule()],
            'config.*.config.*.conditions.*.type' => ['nullable', new ConditionsValidationRule()],
            'config.*.config.*.conditions.*.attribute' => ['required_if:config.*.config.*.conditions.*.type,condition', new ConditionsValidationRule()],
            'config.*.config.*.conditions.*.value' => ['required_if:config.*.config.*.conditions.*.attribute,total_points', new ConditionsValidationRule()],
            'config.*.config.*.conditions.*.operator' => ['required_if:config.*.config.*.conditions.*.type,condition', new ConditionsValidationRule()],
            //relationship
            'offers' => JsonApiRule::toMany(),
        ];
    }
}
