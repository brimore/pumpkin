<?php

namespace App\JsonApi\V1\AudienceLists;

use App\Rules\FilterOperatorRule;
use LaravelJsonApi\Laravel\Http\Requests\ResourceRequest;
use LaravelJsonApi\Validation\Rule as JsonApiRule;

class AffectedMembersRequest extends ResourceRequest
{
    /**
     * Get the validation rules for the resource.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'config' => ['required_without:offers', 'array', 'min:1', new FilterOperatorRule()],
            'config.*.type' => 'required|in:filter,logical-operator,filter-group',
            'config.*.event' => 'required_if:config.*.type,filter' /* |exists:event_types,name */,
            'config.*.from' => 'nullable|date',
            'config.*.to' => 'nullable|date|after:config.*.from',
            'config.*.conditions' => ['nullable', 'array', 'min:0', new FilterOperatorRule()],
            'config.*.conditions.*.type' => 'nullable|in:condition,logical-operator',
            'config.*.conditions.*.value' => 'nullable|required_if:config.*.conditions.*.type,condition',
            'config.*.conditions.*.attribute' => 'required_if:config.*.conditions.*.type,condition',
            'config.*.conditions.*.operator' => 'required_if:config.*.conditions.*.type,condition|in:greater_than_or_equal,less_than_or_equal,less_than,greater_than,equal,not_equal,in,not_in,between',
            // Filter Group Configuration Validation.
            'config.*.config' => ['required_if:config.*.type,filter-group', 'array', new FilterOperatorRule()],
            'config.*.config.*.type' => 'required|in:filter,logical-operator,filter-group',
            'config.*.config.*.event' => 'required_if:config.*.type,filter' /* |exists:event_types,name */,
            'config.*.config.*.from' => 'nullable|date',
            'config.*.config.*.to' => 'nullable|date|after:config.*.config.*.from',
            'config.*.config.*.conditions' => ['nullable', 'array', new FilterOperatorRule()],
            'config.*.config.*.conditions.*.type' => 'nullable|in:condition,logical-operator',
            'config.*.config.*.conditions.*.value' => 'nullable|required_if:config.*.config.*.conditions.*.type,condition',
            'config.*.config.*.conditions.*.attribute' => 'required_if:config.*.conditions.*.type,condition',
            'config.*.config.*.conditions.*.operator' => 'required_if:config.*.conditions.*.type,condition|in:greater_than_or_equal,less_than_or_equal,less_than,greater_than,equal,not_equal,in,not_in,between',
            //relationship
            'offers' => ['required_without:config', JsonApiRule::toMany()]
        ];
    }
}
