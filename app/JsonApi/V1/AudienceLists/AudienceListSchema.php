<?php

namespace App\JsonApi\V1\AudienceLists;

use App\Domain\AudienceList\Model\AudienceList;
use App\Domain\Common\Translations;
use LaravelJsonApi\Eloquent\Contracts\Paginator;
use LaravelJsonApi\Eloquent\Fields\ArrayList;
use LaravelJsonApi\Eloquent\Fields\DateTime;
use LaravelJsonApi\Eloquent\Fields\ID;
use LaravelJsonApi\Eloquent\Fields\Relations\HasMany;
use LaravelJsonApi\Eloquent\Filters\Scope;
use LaravelJsonApi\Eloquent\Filters\WhereIdIn;
use LaravelJsonApi\Eloquent\Pagination\PagePagination;
use LaravelJsonApi\Eloquent\Schema;
class AudienceListSchema extends Schema
{

    /**
     * The model the schema corresponds to.
     *
     * @var string
     */
    public static string $model = AudienceList::class;

    /**
     * Default pagination config
     *
     * @var array|int[]|null
     */
    protected ?array $defaultPagination = ['limit' => 15];

    /**
     * The maximum include path depth.
     *
     * @var int
     */
     protected int $maxDepth = 3;
    /**
     * Get the resource fields.
     *
     * @return array
     */
    public function fields(): array
    {
        return [
            ID::make('uuid')->uuid(),
            Translations::make('name'),
            ArrayList::make('config'),
            DateTime::make('createdAt')->sortable()->readOnly(),
            DateTime::make('updatedAt')->sortable()->readOnly(),
            HasMany::make('offers')
        ];
    }

    /**
     * Get the resource filters.
     *
     * @return array
     */
    public function filters(): array
    {
        return [
            WhereIdIn::make($this),
            Scope::make('name','filter'),
            Scope::make('name-not-null','name'),
        ];
    }

    /**
     * Get the resource paginator.
     *
     * @return Paginator|null
     */
    public function pagination(): ?Paginator
    {
        return PagePagination::make()->withoutNestedMeta();
    }

    /**
     * @return bool
     */
    public function authorizable(): bool
    {
        return false;
    }
}
