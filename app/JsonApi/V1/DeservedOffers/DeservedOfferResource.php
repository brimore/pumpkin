<?php

namespace App\JsonApi\V1\DeservedOffers;

use LaravelJsonApi\Core\Resources\JsonApiResource;

class DeservedOfferResource extends JsonApiResource
{
    public function id(): string
    {
        return $this->resource->getId();
    }

    public function attributes($request): iterable
    {
        return [
            'entry_type' => $this->resource->getType(),
            'payload' => $this->resource->getPayload(),
        ];
    }
}
