<?php

namespace App\Domain\EventType\Model;

use App\Domain\Event\Model\Event;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class EventType extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'event_types';

    /**
     * @var string[]
     */
    protected $fillable = ['name', 'attributes'];

    /**
     * @var string[]
     */
    protected $casts = [
        'attributes' => 'array'
    ];

    /**
     * @return HasMany
     */
    public function events(): HasMany
    {
        return $this->hasMany(Event::class);
    }
}
