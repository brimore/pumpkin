<?php

namespace App\Domain\Oauth;

use Laravel\Passport\RefreshTokenRepository;
use Laravel\Passport\TokenRepository;
use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Token\Parser;

class TokenManager
{
    /**
     * @var TokenRepository
     */
    private $tokenRepo;

    /**
     * @var RefreshTokenRepository
     */
    private $refreshTokenRepo;

    public function __construct(TokenRepository $tokenRepo, RefreshTokenRepository $refreshTokenRepo)
    {
        $this->tokenRepo = $tokenRepo;
        $this->refreshTokenRepo = $refreshTokenRepo;
    }

    public function revokeAccessToken($token)
    {
        $tokenId = $this->getTokenId($token);

        $revoked = $this->tokenRepo->revokeAccessToken($tokenId);

        if ($revoked) {
            $this->refreshTokenRepo->revokeRefreshTokensByAccessTokenId($tokenId);
        }

        return (boolean)$revoked;
    }

    public function getTokenId($token)
    {
        return (new Parser(new JoseEncoder()))->parse($token)->claims()->all()['jti'];
    }
}
