<?php

namespace App\Domain\Sheet\Model;

use App\Domain\ListData\Model\ListData;
use App\Domain\Offer\Model\Offer;
use App\Imports\MembersImport;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Excel as ReaderType;
use Maatwebsite\Excel\Facades\Excel;

class Sheet extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'sheets';

    /**
     * @var string[]
     */
    protected $fillable = ['name', 'file', 'type'];

    /**
     * @param $value
     */
    public function setFileAttribute($value): void
    {
        Storage::disk('public')->delete($this->file);
        $this->attributes['file'] = Storage::disk('public')->putFile('sheets', $value);
    }

    /**
     * @return HasMany
     */
    public function listData(): HasMany
    {
        return $this->hasMany(ListData::class);
    }

    /**
     * @return void
     */
    public function storeInDB(Request $request)
    {
        $file_path = storage_path('app/public/' . $this->file);
        Excel::toCollection(
            new MembersImport, $file_path, null, ReaderType::CSV
        )->first()->except(0)
            ->map(function ($row) use ($request) {
                $mapped_row['module_id'] = $row[0];
                $mapped_row['extra_attributes'] = ['quantity' => $row[1]];
                $mapped_row['module_type'] = 'member';
                $mapped_row['offer_id'] = Offer::whereUuid($request->offer_id)->first()->id;
                return $mapped_row;
            })
            ->unique(fn($row) => $row['module_id'])
            ->chunk(5)
            ->each(fn($rows) => $this->listData()->createMany($rows->toArray()));
    }
}
