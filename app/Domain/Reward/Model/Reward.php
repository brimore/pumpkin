<?php

namespace App\Domain\Reward\Model;

use App\Domain\Offer\Model\Offer;
use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @method static select(string[] $array)
 */
class Reward extends Model
{
    use HasFactory, GeneratesUuid, SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'rewards';

    /**
     * @inheritdoc
     */
    protected $fillable = ['filters', 'offer_id'];

    /**
     * @var string[]
     */
    protected $casts = [
        'filters' => 'array'
    ];

    /**
     * Get the user that owns the phone.
     * @return BelongsTo
     */
    public function offer(): BelongsTo
    {
        return $this->belongsTo(Offer::class)->withTrashed();
    }
}
