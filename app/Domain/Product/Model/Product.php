<?php

namespace App\Domain\Product\Model;

use App\Domain\Brand\Model\Brand;
use App\Domain\Category\Model\Category;
use App\Domain\ProductVariant\Model\ProductVariant;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Product extends Model
{
    use HasFactory;
    /**
     * @inheritdoc
     */
    protected $fillable = ['name','category_id','brand_id','image'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * @return BelongsTo
     */
    public function brand(): BelongsTo
    {
        return $this->belongsTo(Brand::class);
    }
    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }
    /**
     * @return HasMany
     */
    public function productVariant(): HasMany
    {
        return $this->hasMany(ProductVariant::class);
    }


}
