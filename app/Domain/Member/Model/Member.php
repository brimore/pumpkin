<?php

namespace App\Domain\Member\Model;

use App\Domain\Event\Model\Event;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Member extends Authenticatable
{
    use HasFactory, HasApiTokens, Notifiable;

    /**
     * @var string
     */
    protected $table = 'members';

    /**
     * @inheritdoc
     */
    protected $fillable = ['id','code', 'name', 'password'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = ['password'];

    /**
     * @return HasMany
     */
    public function events(): HasMany
    {
        return $this->hasMany(Event::class);
    }

    /**
     * @param $identifier
     * @return mixed
     */
    public function findForPassport($identifier)
    {
        return $this->where('code', $identifier)->first();
    }
}
