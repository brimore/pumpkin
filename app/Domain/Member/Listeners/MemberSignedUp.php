<?php

namespace App\Domain\Member\Listeners;

use App\Domain\Event\Model\Event;
use App\Domain\EventType\Model\EventType;
use App\Domain\Member\Events\SignedUp;
use App\Domain\Member\Model\Member;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class MemberSignedUp
{
    public function handle(SignedUp $event): void
    {
        $member = Member::where('id',$event->payload['id'] )->first();
        if($member){
            Log::info("Updating Member ....");
            try {
                $member->update([
                        'name' => $event->payload['name'],
                        'password' => $event->payload['password'],
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                ]);
                Log::info("A member is updated");
            } catch(QueryException $ex){
                Log::error('Error in updating member: '.$ex->getMessage());
            }

        }else
        {
            Log::info("Adding a New Member ....");
            try {
                $member = Member::create([
                    'id' => $event->payload['id'],
                    'name' => $event->payload['name'],
                    'code' => $event->payload['member_code'],
                    'password' => $event->payload['password'],
                    'created_at' => Carbon::parse($event->payload['created_at'])->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::parse($event->payload['created_at'])->format('Y-m-d H:i:s'),
                ]);
                Log::info("A new member is added");
            } catch(QueryException $ex){
                Log::error('Error in adding a new member: '.$ex->getMessage());
            }

            Log::info("Adding a New Sign Up Event ....");
            $event_type = EventType::whereName('SIGNED_UP')->first();
            $national_id = $event->payload['national_id'];
            try {
                $event_type->events()->create([
                    'member_id' => $member->id,
                    'attributes' => [
                        'age' => $national_id ? $this->getAgeFromIdentity($national_id) : NULL,
                        'gender' => $national_id ? $this->getGenderFromIdentity($national_id) : NULL,
                        'created_at' => Carbon::parse($event->payload['created_at'])->format('Y-m-d H:i:s')
                    ]
                ]);
                Log::info("A new Sign Up Event is added");
            } catch(QueryException $ex){
                Log::error('Error in adding a new Sign Up Event: '.$ex->getMessage());
            }
        }

    }

    /**
     * @param string $identity
     * @return int|null
     */
    public function getAgeFromIdentity(string $identity): ?int
    {
        // substring identity to get birthday and convert it to be a carbon object.
        try {
            $identity = $this->convertArabicToEnglishNumber($identity);
            $birthdate = Carbon::createFromFormat('ymd', substr($identity, 1, 6));
        } catch (\Exception $exception) {
        }

        return isset($birthdate) ? Carbon::today()->diffInYears($birthdate) : NULL;
    }
    /**
     * @param $number
     * @return string
     */
    public function convertArabicToEnglishNumber($number): string
    {
        $arabic = ['٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'];
        $english = range(0, 9);

        return Str::replace($arabic, $english, $number);
    }
    /**
     * @param string $identity
     * @return string
     */
    public function getGenderFromIdentity(string $identity): string
    {
        // substring gender data and convert it to int
        $gender = (int)substr($identity, 12, 1);

        return $gender % 2 ? 'male' : 'female';
    }
}
