<?php

namespace App\Domain\InteractionGroup\Model;

use App\Domain\Offer\Model\Offer;
use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class InteractionGroupOffer extends Model
{
    use GeneratesUuid;

    protected $fillable = ['uuid', 'interaction_group_id', 'offer_id', 'order'];

    protected $with = ['offer'];

    protected $table = 'interaction_group_offer';

    public function offer()
    {
        return $this->belongsTo(Offer::class);
    }

    public function interactionGroup()
    {
        return $this->belongsTo(InteractionGroup::class);
    }

    public static function boot()
    {
        parent::boot();

        self::addGlobalScope('ordered', function (Builder $builder) {
            return $builder->orderBy('order');
        });
    }
}
