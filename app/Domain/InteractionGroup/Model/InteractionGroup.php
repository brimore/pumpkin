<?php

namespace App\Domain\InteractionGroup\Model;

use App\Domain\Offer\Model\Offer;
use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class InteractionGroup extends Model
{
    use HasFactory, GeneratesUuid, HasTranslations;

    const SELECTION_STRATEGY_USER_CAN_SELECT = 'USER_CAN_SELECT';
    const SELECTION_STRATEGY_HIGHEST_PRIORITY_FIRST = 'HIGHEST_PRIORITY_FIRST';
    const SELECTION_STRATEGY_RANDOM = 'RANDOM';
    const SELECTION_BLOCKING_SUBTOTAL = 'BLOCKING_SUBTOTAL';

    protected $fillable = ['selection_strategy', 'limit', 'title', 'description'];

    protected $with = ['offers'];

    protected $translatable = ['title', 'description'];

    protected array $appliedOffers = [];

    public function offers()
    {
        return $this->belongsToMany(Offer::class)
            ->withPivot(['order'])
            ->orderBy('interaction_group_offer.order')
            ->withTimestamps();
    }

    public function interactionGroupOffers()
    {
        return $this->hasMany(InteractionGroupOffer::class);
    }

    public function setAppliedOffers(array $appliedOffers): self
    {
        $this->appliedOffers = $appliedOffers;

        return $this;
    }

    public function getAppliedOffers(): array
    {
        return $this->appliedOffers;
    }

    public static function getSelectionStrategies()
    {
        return [
            self::SELECTION_STRATEGY_USER_CAN_SELECT,
            self::SELECTION_STRATEGY_HIGHEST_PRIORITY_FIRST,
            self::SELECTION_STRATEGY_RANDOM,
            self::SELECTION_BLOCKING_SUBTOTAL
        ];
    }

    public static function boot()
    {
        parent::boot();

        self::deleting(function (InteractionGroup $model) {
            $model->offers()->detach();
        });
    }
}
