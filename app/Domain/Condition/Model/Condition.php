<?php

namespace App\Domain\Condition\Model;

use App\Domain\Offer\Model\Offer;
use App\Domain\SKUList\Model\SkuList;
use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @method static select(string[] $array)
 */
class Condition extends Model
{
    use HasFactory, GeneratesUuid, SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'conditions';

    /**
     * @inheritdoc
     */
    protected $fillable = ['criteria', 'offer_id'];

    /**
     * @var string[]
     */
    protected $casts = [
        'criteria' => 'array'
    ];

    /**
     * Get the user that owns the phone.
     * @return BelongsTo
     */
    public function offer(): BelongsTo
    {
        return $this->belongsTo(Offer::class)->withTrashed();
    }

    /**
     * Get the user that owns the phone.
     * @return HasMany
     */
    public function sku_lists(): HasMany
    {
        return $this->hasMany(SkuList::class);
    }
}
