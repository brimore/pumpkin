<?php

namespace App\Domain\Common;

use Illuminate\Database\Eloquent\Model;

trait HasLocalizedAttribute
{
    public function formatLocalizedAttribute(Model $model, $attributeName): array
    {
        $attribute = [];

        foreach (get_locales() as $locale) {
            $attribute[$locale] = $model->getTranslation($attributeName, $locale, false);
        }

        return $attribute;
    }
}
