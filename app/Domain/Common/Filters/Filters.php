<?php

namespace App\Domain\Common\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Traits\ForwardsCalls;

abstract class Filters
{
    use ForwardsCalls;

    /**
     * @var array
     */
    protected $rawFilters = [];

    /**
     * @var Collection
     */
    protected $parsedFilters;


    public function __construct($filters = [])
    {
        $this->setFilters($filters);
    }

    public function setFilters($filters)
    {
        $this->rawFilters = $filters instanceof Request ? $filters->query() : $filters;

        $this->setParsedFilters($this->rawFilters);
    }

    public function setParsedFilters($rawFilters)
    {
        $this->parsedFilters = new Collection();

        foreach ($rawFilters as $key => $value) {
            $this->add($key, $value);
        }

        return $this;
    }

    public function apply(Builder $builder)
    {
        foreach ($this->parsedFilters as $name => $values) {
            if ($method = $this->getFilterMethod($name)) {
                foreach ($values as $value) {
                    call_user_func_array([$this, $method], [$builder, $value]);
                }
            }
        }

        return $builder;
    }

    public function add($key, $value = true)
    {
        $values = $this->parsedFilters->get($key, []);
        $values[] = $value;

        $this->parsedFilters->put($key, $values);

        return $this;
    }

    protected function getFilterMethod($name)
    {
        $method = Str::camel($name);

        return method_exists($this, $method) ? $method : null;
    }

    public function __call($name, $arguments)
    {
        return $this->forwardCallTo($this->parsedFilters, $name, $arguments);
    }
}
