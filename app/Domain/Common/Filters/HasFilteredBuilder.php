<?php

namespace App\Domain\Common\Filters;

use Illuminate\Database\Eloquent\Builder;

trait HasFilteredBuilder
{
    /**
     * @param Builder $builder
     * @param Filters|null $filters
     * @return Builder
     */
    protected function getFilteredBuilder(Builder $builder, Filters $filters = null)
    {
        if ($filters) $filters->apply($builder);

        return $builder;
    }
}
