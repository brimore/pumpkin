<?php

namespace App\Domain\Common;

use LaravelJsonApi\Eloquent\Fields\Map;
use LaravelJsonApi\Eloquent\Fields\Str;

class Translations
{
    public static function make(string $fieldName): Map
    {
        $map = [];

        foreach (get_locales() as $locale) {
            $map[] = Str::make($locale)
                ->extractUsing(
                    static fn($model) => $model->getTranslation($fieldName, $locale, false) ?: null
                )
                ->fillUsing(
                    static fn($model, $locale, $value) => $model->setTranslation($fieldName, $locale, $value)
                );
        }

        return new Map($fieldName, $map);
    }
}
