<?php

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

function get_locales()
{
    return array_keys(LaravelLocalization::getSupportedLocales());
}
