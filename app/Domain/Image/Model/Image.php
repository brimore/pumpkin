<?php

namespace App\Domain\Image\Model;

use App\Domain\Offer\Model\Offer;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'images';

    /**
     * @var string[]
     */
    protected $fillable = ['name', 'image', 'size', 'type', 'offer_id'];

    /**
     * @param $value
     */
    public function setImageAttribute($value) : void
    {
        Storage::disk('public')->delete($this->image);
        $this->attributes['image'] = Storage::disk('public')->putFile('images', $value);
    }

    /**
     * Get the offer that owns the image.
     * @return BelongsTo
     */
    public function offer(): BelongsTo
    {
        return $this->belongsTo(Offer::class)->withTrashed();
    }
}
