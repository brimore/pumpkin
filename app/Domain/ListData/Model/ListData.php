<?php

namespace App\Domain\ListData\Model;

use App\Domain\Offer\Model\Offer;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ListData extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'list_data';

    /**
     * @inheritdoc
     */
    protected $fillable = ['offer_id', 'sheet_id', 'module_id', 'extra_attributes', 'module_type'];

    /**
     * @var string[]
     */
    protected $casts = ['extra_attributes' => 'array'];

    /**
     * @return BelongsTo
     */
    public function offer(): BelongsTo
    {
        return $this->belongsTo(Offer::class);
    }
}
