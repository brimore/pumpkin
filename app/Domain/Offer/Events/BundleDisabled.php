<?php

namespace App\Domain\Offer\Events;

use App\Domain\Offer\Model\Offer;
use Brimore\Toolkit\MessageBroker\Contracts\ExternalEvent;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BundleDisabled implements ExternalEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private Offer $offer;

    public function __construct(Offer $offer)
    {
        $this->offer = $offer;
    }

    public function getName()
    {
        return 'bundle.disabled';
    }

    public function getPayload()
    {
        return [
            'id' => $this->offer->uuid,
            'code' => $this->offer->code,
            'title' => [
                'en' => $this->offer->getTranslation('title', 'en', false),
                'ar' => $this->offer->getTranslation('title', 'ar', false),
            ],
            'disabled_at' => $this->offer->deleted_at
        ];
    }
}
