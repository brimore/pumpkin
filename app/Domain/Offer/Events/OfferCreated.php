<?php

namespace App\Domain\Offer\Events;

use App\Domain\Offer\Model\Offer;
use Brimore\Toolkit\MessageBroker\Contracts\ExternalEvent;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OfferCreated implements ExternalEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private Offer $offer;

    public function __construct(Offer $offer)
    {
        $this->offer = $offer;
    }

    public function getName()
    {
        return 'offer.created';
    }

    public function getPayload()
    {
        $promotion = DB::connection('Hercules')->table('promotions')
            ->where('uuid', $this->offer->uuid)
            ->exists();

        if (!$promotion) {
            DB::connection('Hercules')->table('promotions')
                ->insert([
                    'uuid'       => $this->offer->uuid,
                    'name'       => $this->offer->getTranslation('title', 'ar', false),
                    'type'       => 16,
                    'created_at' => now(),
                    'updated_at' => now()
                ]);
        }

        return [
            /*'id' => $this->offer->uuid,
            'code' => $this->offer->code,
            'title' => [
                'en' => $this->offer->getTranslation('title', 'en', false),
                'ar' => $this->offer->getTranslation('title', 'ar', false),
            ]*/
        ];
    }
}
