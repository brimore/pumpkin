<?php

namespace App\Domain\Offer\Model;

use App\Domain\AccumulationGroup\Model\AccumulationGroup;
use App\Domain\AudienceList\Model\AudienceList;
use App\Domain\Condition\Model\Condition;
use App\Domain\Image\Model\Image;
use App\Domain\ListData\Model\ListData;
use App\Domain\Offer\Events\OfferCreated;
use App\Domain\Reward\Model\Reward;
use Carbon\Carbon;
use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Arr;
use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\SoftDeletes;

class Offer extends Model
{
    use HasFactory, HasTranslations, GeneratesUuid, SoftDeletes;

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'code',
        'title',
        'starts_at',
        'ends_at',
        'is_active',
        'audience_list_id',
        'multiples',
        'brief',
        'tags',
        'accumulation_group_id'
    ];

    /**
     * The translatable attributes.
     *
     * @var string[]
     */
    protected $translatable = ['title'];

    /**
     * @inheritdoc
     */
    protected $dates = ['starts_at', 'ends_at'];

    /**
     * @inheritdoc
     */
    protected $casts = [
        'multiples' => 'array',
        'title' => 'array',
        'tags' => 'array',
    ];

    /**
     * @inheritdoc
     */
    protected $dispatchesEvents = [
        'created' => OfferCreated::class
    ];

    /**
     * Get the audience list that owns the offer.
     * @return BelongsTo
     */
    public function audience_list(): BelongsTo
    {
        return $this->belongsTo(AudienceList::class)->withTrashed();
    }

    /**
     * Get the condition associated with the offer.
     * @return HasOne
     */
    public function condition(): HasOne
    {
        return $this->hasOne(Condition::class)->withTrashed();
    }

    /**
     * Get the Image associated with the offer.
     * @return HasOne
     */
    public function image(): HasOne
    {
        return $this->hasOne(Image::class);
    }

    /**
     * @return HasOne
     */
    public function reward(): HasOne
    {
        return $this->hasOne(Reward::class)->withTrashed();
    }

    /**
     * @return BelongsTo
     */
    public function accumulationGroup(): BelongsTo
    {
        return $this->belongsTo(AccumulationGroup::class);
    }

    /**
     * @return BelongsTo
     */
    public function listData(): BelongsTo
    {
        return $this->belongsTo(ListData::class);
    }

    /**
     * @return void
     */
    public function setDeservedRewardAttribute($rewards)
    {
        $this->attributes['deserved_reward'] = $rewards;
    }

    /**
     * @param $query
     * @param $search_value
     * @return mixed
     */
    public function scopeFilter($query, $search_value)
    {
        return $query->where('tags', 'like', "%$search_value%");
    }

    /**
     * @param $query
     * @param $search_value
     * @return mixed
     */
    public function scopeAudienceList($query, $search_value)
    {
        if ($search_value === 'all-members') {
            $query->whereNull('audience_list_id');
        }
        $audience_list = AudienceList::whereUuid($search_value)->first();
        if (!empty($audience_list)) {
            $query->where('audience_list_id', 'like', "%$audience_list->id%");
        }
        if (request('from'))
            $query->whereDate('starts_at', '>=', Carbon::parse(request('from')));
        if (request('to'))
            $query->whereDate('starts_at', '<=', Carbon::parse(request('to')));

        return $query->orderBy('is_active', 'ASC');
    }

    /**
     * @param $query
     * @param $search_value
     * @return mixed
     */
    public function scopeTitle($query, $search_value): mixed
    {
        return $query->where('title', 'like', "%$search_value%");
    }

    /**
     * @param $query
     * @param $search_value
     * @return mixed
     */
    public function scopeStatus($query, $search_value): mixed
    {
        return $query->where('is_active', $search_value);
    }

    /**
     * @return false
     */
    public function getIsAccumulativeAttribute()
    {
        foreach (($this->condition?->criteria ?? []) as $criterion) {
            if (isset($criterion['conditions'])) {
                foreach ($criterion['conditions'] as $condition) {
                    if (isset($condition['value']) && is_array($condition['value'])) {
                        foreach ($condition['value'] as $value) {
                            if (isset($value['type']) && $value['type'] === 'accumulation')
                                return $value['value'] === 'accumulative';
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * @return int
     */
    public function getConditionPointsAttribute()
    {
        foreach (($this->condition?->criteria ?? []) as $criterion) {
            if (isset($criterion['conditions'])) {
                foreach ($criterion['conditions'] as $condition) {
                    if (isset($condition['value']) && is_array($condition['value'])) {
                        foreach ($condition['value'] as $value) {
                            if (isset($value['type']) && $value['type'] === 'points')
                                return $value['value'];
                        }
                    }
                }
            }
        }
        return 0;
    }
}
