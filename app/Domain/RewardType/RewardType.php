<?php

namespace App\Domain\RewardType;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RewardType extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'reward_types';

    /**
     * @var string[]
     */
    protected $fillable = ['name','attributes'];

    /**
     * @var string[]
     */
    protected $casts = [
        'name' => 'string',
        'attributes' => 'array'
    ];

}
