<?php

namespace App\Domain\RewardType\Model;

use App\Domain\Reward\Model\Reward;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class RewardType extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'reward_types';

    /**
     * @var string[]
     */
    protected $fillable = ['name','attributes'];

    /**
     * @var string[]
     */
    protected $casts = [
        'name' => 'string',
        'attributes' => 'array'
    ];

    /**
     * @return HasMany
     */
    public function events(): HasMany
    {
        return $this->hasMany(Reward::class);
    }
}
