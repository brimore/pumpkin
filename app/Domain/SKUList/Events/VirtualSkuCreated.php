<?php

namespace App\Domain\SKUList\Events;


use App\Domain\SKUList\Model\SkuList;
use App\OfferConsumption\Rewards\Setup\Element\Reward\Concern\HasConditions;
use Brimore\Toolkit\MessageBroker\Contracts\ExternalEvent;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;

class VirtualSkuCreated implements ExternalEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels, HasConditions;

    private SKUList $sku_list;
    private array $discount;
    private string $offer_uuid;

    public function __construct(SKUList $sku_list, array $discount, string $offer_uuid)
    {
        $this->sku_list = $sku_list;
        $this->discount = $discount;
        $this->offer_uuid = $offer_uuid;
    }

    public function getName(): string
    {
        return 'bundle.created';
    }

    public function getPayload()
    {

        $bundle = Arr::first( $this->sku_list->criteria, function ($condition)  {
            return $condition['type'] === 'condition' && $condition['attribute'] === 'virtual_sku';
        });
        $type = $this->getConditionValue($this->discount, 'type', 'percentage');
        $amount = $this->getConditionValue($this->discount, 'amount', 0);
        return [
            'offer_uuid' => $this->offer_uuid,
            'bundle_uuid' => $this->sku_list->uuid,
            'discount_type' => $type,
            'discount_amount' => $amount,
            'bundles' => $bundle['value'],
            'title' => [
                'en' => $this->sku_list->getTranslation('name', 'en', false),
                'ar' => $this->sku_list->getTranslation('name', 'ar', false),
            ]
        ];
    }
}
