<?php

namespace App\Domain\SKUList\Model;

use App\Domain\AudienceList\Model\AudienceList;
use App\Domain\Condition\Model\Condition;
use App\Domain\Reward\Model\Reward;
use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

/**
 * @method static select(string[] $array)
 */
class SkuList extends Model
{
    use HasFactory, HasTranslations, GeneratesUuid, SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'sku_lists';

    /**
     * @inheritdoc
     */
    protected $fillable = ['name', 'condition', 'criteria', 'same_skus', 'same_as_purchase'];

    /**
     * The translatable attributes.
     *
     * @var string[]
     */
    protected $translatable = ['name'];
    /**
     * @var string[]
     */
    protected $casts = [
        'condition' => 'array',
        'criteria' => 'array',
        'name' => 'array'
    ];

    /**
     * @return BelongsTo
     */
    public function audience_list(): BelongsTo
    {
        return $this->belongsTo(AudienceList::class);
    }

    /**
     * @return BelongsTo
     */
    public function condition(): BelongsTo
    {
        return $this->belongsTo(Condition::class);
    }

    /**
     * @return BelongsTo
     */
    public function reward(): BelongsTo
    {
        return $this->belongsTo(Reward::class);
    }
    /**
     * @param $query
     * @param $search_value
     * @return mixed
     */
    public function scopeFilter($query, $search_value): mixed
    {
        return $query->where('name', 'like', "%$search_value%");
    }

}
