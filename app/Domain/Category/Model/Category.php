<?php

namespace App\Domain\Category\Model;

use App\Domain\Product\Model\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
    use HasFactory;

    /**
     * @inheritdoc
     */
    protected $fillable = ['name', 'parent_id'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return HasMany
     */
    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }

    /**
     * @param $query
     * @param $name
     *
     * @return mixed
     */
    public function scopeFilter($query, $search_value)
    {
        return $query->where('name', 'like', "%$search_value%");
    }
}
