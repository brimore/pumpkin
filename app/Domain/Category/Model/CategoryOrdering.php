<?php


namespace App\Domain\Category\Model;


use Illuminate\Database\Eloquent\Model;

class CategoryOrdering extends Model
{
    protected $table ='category_ordering';
    protected $fillable = ['category_id', 'order'];
}
