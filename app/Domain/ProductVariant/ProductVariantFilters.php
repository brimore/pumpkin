<?php

namespace App\Domain\ProductVariant;

use App\Domain\Common\Filters\Filters;
use Illuminate\Database\Eloquent\Builder;

class ProductVariantFilters extends Filters
{
    public function ids(Builder $builder, $ids)
    {
        return $builder->whereIn('id', $ids);
    }

    public function id(Builder $builder, $id)
    {
        return $builder->where('id', $id);
    }

}
