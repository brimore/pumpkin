<?php

namespace App\Domain\ProductVariant\Model;

use App\Domain\Product\Model\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ProductVariant extends Model
{
    use HasFactory;
    /**
     * @inheritdoc
     */
    protected $fillable = ['code', 'name', 'product_id', 'price'];
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
    /**
     * @param $query
     * @param $search_value
     * @return mixed
     */
    public function scopeName($query, $search_value)
    {
        return $query->where('name', 'like', "%$search_value%");
    }
    /**
     * @param $query
     * @param $search_value
     * @return mixed
     */
    public function scopeCode($query, $search_value)
    {
        return $query->where('code', 'like', "%$search_value%");
    }
}
