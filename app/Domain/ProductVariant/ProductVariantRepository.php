<?php

namespace App\Domain\ProductVariant;

use App\Domain\Common\Filters\HasFilteredBuilder;
use App\Domain\ProductVariant\Model\ProductVariant;

class ProductVariantRepository
{
    use HasFilteredBuilder;

    public function find($id)
    {
        return $this->getFilteredBuilder(ProductVariant::query())->find($id);
    }

    public function paginate(ProductVariantFilters $filters, $paginationConfig)
    {
        return $this->getFilteredBuilder(ProductVariant::query(), $filters)->paginate(
            null, ['*'], 'page', $paginationConfig['page']
        );
    }
}
