<?php

namespace App\Domain\AudienceList\Model;

use App\Domain\Offer\Model\Offer;
use App\Domain\SKUList\Model\SkuList;
use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

/**
 * @method static select(string[] $array)
 */
class AudienceList extends Model
{
    use HasFactory, HasTranslations, GeneratesUuid, SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'audience_lists';

    /**
     * @inheritdoc
     */
    protected $fillable = ['name', 'config'];

    /**
     * The translatable attributes.
     *
     * @var string[]
     */
    protected $translatable = ['name'];
    /**
     * @var string[]
     */
    protected $casts = [
        'config' => 'array',
        'name' => 'array'
    ];

    /**
     * Get the offers for the audience list.
     * @return HasMany
     */
    public function offers(): HasMany
    {
        return $this->hasMany(Offer::class)->withTrashed();
    }

    /**
     * Get the offers for the audience list.
     * @return HasMany
     */
    public function sku_lists(): HasMany
    {
        return $this->hasMany(SkuList::class);
    }

    /**
     * @param $query
     * @param $search_value
     * @return mixed
     */
    public function scopeFilter($query, $search_value): mixed
    {
        return $query->where('name', 'like', "%$search_value%");
    }

    /**
     * @param $query
     * @param $search_value
     * @return mixed
     */
    public function scopeName($query, $search_value): mixed
    {
        return $query->whereNotNull('name');
    }
}
