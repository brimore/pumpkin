<?php

namespace App\Domain\Order\Listeners;


use App\Domain\Event\Model\Event;
use App\Domain\EventType\Model\EventType;
use App\Domain\Order\Events\PlacedOrderEvent;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PlacedOrderListener
{
    /**
     * @param PlacedOrderEvent $event
     * @return void
     */
    public function handle(PlacedOrderEvent $event): void
    {

        Log::info("Placed Order ....");
        $event_type = EventType::whereName('PLACED_ORDER')->first();
        try {
            if (!Event::where('order_id', $event->payload['order_id'])->where('order_status', $event->payload['status'])->exists()) {
                $event_type->events()->create([
                    'member_id' => $event->payload['member_id'],
                    'attributes' => [
                        'order_id' => $event->payload['order_id'],
                        'source' => $event->payload['source'],
                        'variants' => $this->mapVariants($event->payload),
                        'total_price' => $event->payload['total_price'],
                        'total_points' => $event->payload['total_points'],
                        'status' => $event->payload['status'],
                        'created_at' => Carbon::parse($event->payload['created_at'])->format('Y-m-d H:i:s')
                    ]
                ]);
                $this->calculateMemberOfferConsumption($event->payload);
                Log::info("A New PLACED_ORDER Event is Added");
            }
        } catch (QueryException $ex) {
            Log::error('Error in adding PLACED_ORDER Event: ' . $ex->getMessage());
        }
    }

    /**
     * @param $variants
     * @return Collection
     */
    public function mapVariants($payload): Collection
    {
        $variants = $payload['variants'];
        return Collection::make($variants)->map(function ($variant) use ($payload) {
            $this->calculateMemberPoints($payload);
            return [
                'id' => $variant['variant_id'],
                'SKU' => $variant['sku'],
                "price" => $variant['price'],
                "brand_id" => $variant['brand_id'],
                "quantity" => $variant['quantity'],
                "category_id" => $variant['category_id'],
                "total_price" => $variant['total_price'],
                "offer_id" => $variant['promotion_id'],
                "total_points" => $variant['total_points']
            ];
        });
    }

    /**
     * @param $variant
     * @param $payload
     * @return void
     */
    public function calculateMemberOfferConsumption($payload)
    {
        $offers = $payload['offers'];
        $status = $payload['status'];
        $member_id = $payload['member_id'];

        foreach ($offers as $offer) {
            $member_offer = DB::table('member_offer_consumption')
                ->where('offer_id', $offer['uuid'])
                ->where('member_id', $member_id)
                ->where('order_id', $payload['order_id']);

            if (!in_array($status, [1, 5, 6, 7, 8, 13, 16])) {
                if (!$member_offer->exists())
                    DB::table('member_offer_consumption')->insert([
                        'member_id' => $member_id,
                        'offer_id' => $offer['uuid'],
                        'order_id' => $payload['order_id'],
                        'status' => $status,
                        'quantity' => $offer['multiple'],
                        'total_points' => 0,
                        'total_price' => 0,
                        'created_at' => now(),
                        'updated_at' => now()
                    ]);
            } else {
                $member_offer->delete();
            }
        }
    }

    /**
     * @param $variant
     * @param $payload
     * @return void
     */
    public function calculateMemberPoints($payload)
    {
        $status = $payload['status'];
        $member_id = $payload['member_id'];

        $points = DB::table('member_points')
            ->where('member_id', $member_id)
            ->where('order_id', $payload['order_id']);

        if (!in_array($status, [1, 5, 6, 7, 8, 13, 16])) {
            if (!$points->exists())
                DB::table('member_points')->insert([
                    'member_id' => $member_id,
                    'order_id' => $payload['order_id'],
                    'status' => $status,
                    'order_creation_date' => Carbon::parse($payload['created_at'])->format('Y-m-d H:i:s'),
                    'total_points' => $payload['total_points'],
                    'created_at' => now(),
                    'updated_at' => now()
                ]);
        } else {
            $points->delete();
        }
    }
}
