<?php

namespace App\Domain\AccumulationGroup\Model;

use App\Domain\Offer\Model\Offer;
use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccumulationGroup extends Model
{
    use HasFactory, GeneratesUuid;

    protected $fillable = ['name'];

    public function offers()
    {
        return $this->hasMany(Offer::class);
    }
}
