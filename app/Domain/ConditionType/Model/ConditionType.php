<?php

namespace App\Domain\ConditionType\Model;

use App\Domain\Condition\Model\Condition;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;


class ConditionType extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'condition_types';

    /**
     * @var string[]
     */
    protected $fillable = ['name','attributes'];

    /**
     * @var string[]
     */
    protected $casts = [
        'name' => 'string',
        'attributes' => 'array'
    ];
}
