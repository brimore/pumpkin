<?php

namespace App\OfferConsumption\Steps;

use App\Domain\Offer\Model\Offer;
use App\OfferConsumption\Cart\Contracts\Cart;
use App\OfferConsumption\Parsers\Factories\AudienceListFactory;
use App\OfferConsumption\Parsers\Parser;

/**
 * Concrete step checks if audience-list contains a specific member.
 */
class AudienceListStep extends Steps
{
    /**
     * @param Offer $offer
     * @param Cart $cart
     *
     * @return bool
     */
    public function check(Offer $offer, Cart $cart): bool
    {
        if (!$offer?->reward) return false;

        if (!$offer?->audience_list?->config) return parent::check($offer, $cart);

        $parser = new Parser($offer->audience_list->config);

        $audience_list = $parser->parse(new AudienceListFactory());

        return $audience_list->get()->exists() && parent::check($offer, $cart);
    }
}
