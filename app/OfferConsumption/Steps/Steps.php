<?php

namespace App\OfferConsumption\Steps;

use App\Domain\Offer\Model\Offer;
use App\OfferConsumption\Cart\Contracts\Cart;

abstract class Steps
{
    /**
     * @var Steps
     */
    private Steps $next_step;

    /**
     * build a chain of steps.
     *
     * @param Steps $next_step
     *
     * @return Steps
     */
    public function nextStep(Steps $next_step): Steps
    {
        $this->next_step = $next_step;

        return $next_step;
    }

    /**
     * Concrete classes must override this method to provide their own checks.
     * @param Offer $offer
     * @param array $cart
     *
     * @return bool
     */
    public function check(Offer $offer, Cart $cart): bool
    {
        return !isset($this->next_step) || $this->next_step->check($offer, $cart);
    }
}
