<?php

namespace App\OfferConsumption\Steps;

use App\Domain\Offer\Model\Offer;
use App\OfferConsumption\Cart\Contracts\Cart;
use App\OfferConsumption\Parsers\Factories\ConditionFactory;
use App\OfferConsumption\Parsers\Parser;

/**
 * Concrete step checks if member deserves the offer based on the condition.
 */
class ConditionStep extends Steps
{
    /**
     * @param Offer $offer
     * @param Cart $cart
     *
     * @return bool
     */
    public function check(Offer $offer, Cart $cart): bool
    {
        if (!$offer?->condition?->criteria) return false;

        $parser = new Parser($offer->condition->criteria);

        $condition = $parser->parse(new ConditionFactory($cart, $offer));

        return $condition->get() && parent::check($offer, $cart);
    }
}
