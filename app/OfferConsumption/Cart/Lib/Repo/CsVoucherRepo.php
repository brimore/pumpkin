<?php


namespace App\OfferConsumption\Cart\Lib\Repo;

use App\Domain\ProductVariant\Model\ProductVariant;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class CsVoucherRepo
{
    /**
     * @var array
     */
    private static $products = [];

    /**
     * @param $item
     * @param $newPrice
     * @param $newPoint
     * @return void
     */
    public static function add($item, $newPrice = 0, $newPoint = 0)
    {
        $variant = ProductVariant::find($item['variantId']);
        $item['new_unit_price'] = $newPrice;
        $item['unit_discount'] = $item['price'] - $newPrice;
        $item['unit_point_discount'] = ($item['point'] - $newPoint) * ($item['quantity']);
        $item['name'] = $variant->name;
        $item['code'] = $variant->code;
        self::$products[] = $item;
    }

    /**
     * @return array
     */
    public static function getProducts()
    {
        return self::$products;
    }

    /**
     * @return mixed
     */
    public static function getDeductedPoints()
    {
        return Collection::make(self::$products)->sum('unit_point_discount');
    }
}
