<?php


namespace App\OfferConsumption\Cart\Lib\Pipe;


use App\Domain\ProductVariant\Model\ProductVariant;
use App\OfferConsumption\Cart\Lib\Repo\CsVoucherRepo;
use Illuminate\Support\Arr;
use Closure;
use Illuminate\Support\Facades\DB;


class CsVoucherDeduction
{
    public function handle($cart, Closure $next)
    {
        $subtract = $cart->getSubtract();
        if ($subtract > 0) {
            $ids = collect($cart->getCartItems())->pluck('variantId');
            $sortedVariants = $this->getSortedVariants($ids);
            foreach ($sortedVariants as $variant) {
                $items = $this->getVariant($cart->getCartItems(), $variant['id']);
                foreach ($items as $index => $value) {
                    if ($subtract == 0) {
                        break;
                    }
                    $newPrice = $this->newPrice($subtract, $value);
                    $newPoint = $cart->setVariantPointAndPrice($index, $newPrice);
                    CsVoucherRepo::add($value, $newPrice, $newPoint);
                }
            }
            $cart->setSubtract($subtract);
        }
        return $next($cart);
    }

    private function getSortedVariants($items)
    {
        return ProductVariant::
        join('products as p', 'product_id', 'p.id')->
        LeftJoin('categories as c', 'p.category_id', 'c.id')->
        leftJoin('categories as mc', 'c.parent_id', 'mc.id')->
        leftJoin('category_ordering as co', 'co.category_id', 'mc.id')->
        whereIn('product_variants.id', $items)->
        select('product_variants.id', 'co.order')->
        groupBy('product_variants.id', 'co.order')->
        orderBy(DB::raw('ISNULL(co.order), co.order'), 'ASC')->
        get()->toArray();
    }

    private function getVariant($variants, $id): mixed
    {
        return collect($variants)->where('variantId', $id);
    }

    public function newPrice(&$csVoucher, $item)
    {
        $totalPrice = $item['price'] * $item['quantity'];
        if ($csVoucher > $totalPrice) {
            $csVoucher -= $totalPrice;
            return 0;
        } else {
            $newPrice = $totalPrice - $csVoucher;
            $csVoucher = 0;
            return $newPrice / $item['quantity'];
        }
    }

}
