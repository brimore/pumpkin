<?php

namespace App\OfferConsumption\Cart\Traits;

use App\Domain\Offer\Model\Offer;
use App\Domain\Reward\Model\Reward;
use App\Domain\SKUList\Model\SkuList;
use Illuminate\Support\Arr;


trait BundleConsumption
{

    private static function BundleInformation(array $bundles): array
    {
        $bundle_info = [];
        foreach($bundles as $bundle) {
            $total_price = 0;
            foreach ($bundle['products'] as $variant) {
                $total_price += $variant['quantity'] * $variant['price'];
            }

                $bundle_info[] = [
                    'bundle_name' => Arr::get($bundle, 'name'),
                    'offer_uuid' => Arr::get($bundle, 'offer_uuid'),
                    'bundle_identifier' => Arr::get($bundle, 'identifier'),
                    'bundle_total_price' => $total_price,
                    'products' => Arr::get($bundle, 'products')
                ];
        }
        return $bundle_info;
    }

    private static function ValidateBundle(array $bundles)
    {
//        $bundles_info = self::BundleInformation($bundles);
        foreach($bundles as $bundle_info)
        {
            $offer = Offer::whereUuid($bundle_info['offer_uuid'])->get()->first();
            $reward = Reward::where('offer_id','=',$offer->id)->get()->first();
            if(!empty($reward))
            {
                $sku_list = self::ExtractVirtualSkuFromReward($reward);
                if($sku_list !== null)
                {
                    $virtual_sku = Arr::first($sku_list->toArray()['criteria'], function ($condition)  {
                        return $condition['type'] === 'condition' && $condition['attribute'] === 'virtual_sku';
                    });
                }
//                dd($virtual_sku['value'],$bundle_info);
            }

        }

    }
    private static function ExtractVirtualSkuFromReward(Reward $reward): ?SkuList
    {
        $filter =  Arr::first($reward->filters, function ($condition)  {
            return ($condition['type'] === 'filter' && $condition['reward'] === 'DISCOUNT');
        });
        if(empty($filter))
        {
            $filter_group =  Arr::first($reward->filters, function ($condition)  {
                return ($condition['type'] === 'filter-group');
            });

            if(empty($filter_group) && !isset($filter_group['filters']))
            {
                return null;
            }
            $filter =  Arr::first($filter_group['filters'], function ($condition)  {
                return ($condition['type'] === 'filter' && $condition['reward'] === 'DISCOUNT');
            });
            $bundle =  Arr::first($filter['conditions'], function ($condition)  {
                return ($condition['type'] === 'condition' && $condition['attribute'] === 'sku_list');
            });
            $virtual_sku = SKUList::find((int)$bundle['value']);
            $is_virtual_sku = Arr::first($virtual_sku->toArray()['criteria'], function ($condition)  {
                return $condition['type'] === 'condition' && $condition['attribute'] === 'virtual_sku';
            });
            if(!empty($is_virtual_sku)){
                return $virtual_sku;
            }
            return null;

        }
        $bundle =  Arr::first($filter['conditions'], function ($condition)  {
            return ($condition['type'] === 'condition' && $condition['attribute'] === 'sku_list');
        });
        $virtual_sku = SKUList::find((int)$bundle['value']);
        $is_virtual_sku = Arr::first($virtual_sku->toArray()['criteria'], function ($condition)  {
            return $condition['type'] === 'condition' && $condition['attribute'] === 'virtual_sku';
        });
        if(!empty($is_virtual_sku)){
            return $virtual_sku;
        }
        return null;
    }

}
