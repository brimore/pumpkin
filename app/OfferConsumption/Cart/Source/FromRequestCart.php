<?php

namespace App\OfferConsumption\Cart\Source;

use App\OfferConsumption\Cart\Contracts\Cart;
use App\OfferConsumption\Cart\Traits\BundleConsumption;
use App\OfferConsumption\Cart\Variant;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class FromRequestCart implements Cart
{
    use BundleConsumption;

    /**
     * @var array
     */
    private array $cartContent;

    /**
     * @param array $cartContent
     */
    public function __construct(array $cartContent)
    {
        $this->cartContent = $cartContent;
    }

    /**
     * @return array
     */
    public function getVariants(): array
    {
        $variants = [];

        foreach ($this->getAttribute('cart', []) as $variant) {
            $variants[] = new Variant($variant);
        }

        return $variants;
    }

    /**
     * @param string $in
     * @param bool $actual
     * @return float
     */
    public function getSubtotal($in = 'price', bool $actual = false): float
    {
        $added = $in === 'points'
            ? ($this->getAttribute('add', 0) / 3)
            : $this->getAttribute('add', 0);

        $subtotal = (float)array_reduce($this->getVariants(), function ($carry, Variant $variant) use ($in) {
                $amount = $in === 'points' ? $variant->getPoint() : $variant->getPrice();
                return $carry + ($amount * $variant->getQuantity());
            }, 0)
            + (!$actual ? $added : 0);

        return $subtotal < 0 ? 0 : $subtotal;
    }

    /**
     * @param array $only
     * @return float
     */
    public function getCartVariantPrice(array $only = []): float
    {
        $variants = count($only) == 0 ?
            $this->getVariants() : array_filter($this->getVariants(), function ($row) use ($only) {
                return in_array($row->getId(), $only);
            });

        return (float)array_reduce($variants, function ($carry, Variant $variant) {
            return $carry + ($variant->getPrice() * $variant->getQuantity());
        }, 0);
    }

    /**
     * @param $key
     * @param null $default
     *
     * @return mixed
     */
    protected function getAttribute($key, $default = null): mixed
    {
        return Arr::get($this->cartContent, sprintf('%s.%s', 'attributes', $key), $default);
    }

    /**
     * @param $key
     * @param $value
     * @return array
     */
    protected function setAttribute($key, $value)
    {
        return Arr::set($this->cartContent, sprintf('%s.%s', 'attributes', $key), $value);
    }

    /**
     * @param $rewards
     * @return float
     */
    public function getTotal(Collection $rewards = null): array
    {
        $total_points = $this->getSubtotal('points');
        $total_price = $this->getSubtotal();

        if ($rewards) {
            // Single Offers calculations.
            $rewards
                ->filter(fn($reward) => $reward->getType() === 'offer')
                ->each(function ($reward) use (&$total_points, &$total_price) {
                    $this->calculateDiscounts($reward->getPayload(), $total_price, $total_points);
                });

            // Selection Group calculations.
            $rewards->filter(fn($reward) => $reward->getType() === 'selection_group')
                ->each(function ($reward) use (&$total_points, &$total_price) {
                    // Fetch user selections.
                    $selected_groups = Collection::make(request()->data['attributes']['offers_selection'] ?? []);

                    // Check if user select this group.
                    if ($selected_groups->contains('groupId', $reward->getId())) {
                        // Fetch current group offers based on user's selection.
                        $group_offers = $selected_groups->where('groupId', $reward->getId())->first()['offers'];
                        $deserved_offers = Collection::make($reward->getPayload()['offers'])
                            ->whereIn('id', array_column($group_offers, 'id'));

                        // Calculate offer discounts.
                        foreach ($deserved_offers as $offer)
                            $this->calculateDiscounts($offer, $total_price, $total_points);
                    }
                });
        }

        return ['total_points' => round($total_points), 'total_price' => round($total_price)];
    }

    /**
     * @param array $offer
     * @param $total_price
     * @param $total_points
     * @return void
     */
    private function calculateDiscounts(array $offer, &$total_price, &$total_points)
    {
        $gift = $offer['deserved_rewards']['rewards'][0];
        switch ($gift['giftType']) {
            case 'cashback-discount':
            case 'discount':
                $total_price -= $gift['payload']['discountPercentageAmount'] ?: $gift['payload']['discountAmount'];
                break;
            case 'cashback-voucher':
            case 'voucher':
                $total_price -= $gift['payload']['voucherAmount'];
                if ($gift['payload']['applied_on'] === 'price-and-points')
                    $total_points -= $gift['payload']['consumed'] / (DB::table('voucher_configuration')->first()?->point_factor ?? 3);
                break;
        }
    }

    /**
     * @return array
     */
    public function getCartItems(): array
    {
        return $this->getAttribute('cart', []);
    }

    /**
     * @param $index
     * @param float $value
     * @return mixed
     */
    public function setVariantPointAndPrice($index, float $value)
    {
        $cartItems = Arr::get($this->cartContent, sprintf('%s.%s', 'attributes', 'cart'), []);
        $pointFactor = array_values(Arr::where($this->getVariants(), fn($value, $key) => $index == $key))[0]->getPointFactor();

        $cartItems[$index]['point'] = $value / $pointFactor;
        $cartItems[$index]['price'] = $value;

        Arr::set($this->cartContent, sprintf('%s.%s', 'attributes', 'cart'), $cartItems);

        return $cartItems[$index]['point'];
    }

    /**
     * @return float
     */
    public function getSubtract(): float
    {
        return $this->getAttribute('subtract', 0);
    }

    /**
     * @param float $value
     * @return array
     */
    public function setSubtract(float $value)
    {
        return $this->setAttribute('subtract', $value);
    }
}
