<?php

namespace App\OfferConsumption\Cart\Contracts;

use Illuminate\Support\Collection;

interface Cart
{
    public function getSubtotal($in = 'price'): float;

    public function getVariants(): array;

    public function getCartVariantPrice(array $only): float;

    public function getTotal(Collection $rewards = null): array;
}
