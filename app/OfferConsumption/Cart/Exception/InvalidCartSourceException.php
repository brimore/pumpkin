<?php

namespace App\OfferConsumption\Cart\Exception;

use Throwable;

class InvalidCartSourceException extends \RuntimeException
{
    public function __construct($message = "Invalid cart source", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
