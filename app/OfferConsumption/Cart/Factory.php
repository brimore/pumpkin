<?php

namespace App\OfferConsumption\Cart;

use App\OfferConsumption\Cart\Exception\InvalidCartSourceException;
use App\OfferConsumption\Cart\Source\FromRequestCart;

class Factory
{
    const SOURCE_REQUEST = 'request';

    public function make($source, $cartContent = null)
    {
        switch ($source) {
            case self::SOURCE_REQUEST:
                return new FromRequestCart($cartContent);
        }

        throw new InvalidCartSourceException();
    }
}
