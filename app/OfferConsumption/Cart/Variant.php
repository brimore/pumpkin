<?php

namespace App\OfferConsumption\Cart;

use Illuminate\Support\Arr;

class Variant
{
    /**
     * @var array
     */
    private array $specs;

    /**
     * @param array $specs
     */
    public function __construct(array $specs)
    {
        $this->specs = $specs;
    }

    /**
     * @return array|\ArrayAccess|mixed
     */
    public function getId()
    {
        return $this->get('variantId');
    }

    /**
     * @return array|\ArrayAccess|mixed
     */
    public function getQuantity()
    {
        return $this->get('quantity');
    }

    /**
     * @return array|\ArrayAccess|mixed
     */
    public function getPrice()
    {
        return $this->get('price');
    }

    /**
     * @return array|\ArrayAccess|mixed
     */
    public function getPoint()
    {
        return $this->get('point');
    }

    /**
     * @return array|\ArrayAccess|mixed
     */
    public function getPointFactor()
    {
        return $this->getPrice() / $this->get('point');
    }

    /**
     * @param $key
     * @param null $default
     * @return array|\ArrayAccess|mixed
     */
    protected function get($key, $default = null)
    {
        return Arr::get($this->specs, $key, $default);
    }
}
