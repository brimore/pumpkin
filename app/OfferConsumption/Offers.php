<?php

namespace App\OfferConsumption;

use App\Domain\Offer\Model\Offer;
use App\OfferConsumption\Cart\Contracts\Cart;
use App\OfferConsumption\Steps\Steps;
use Carbon\Carbon;

class Offers
{
    /**
     * @var Steps
     */
    private Steps $steps;

    /**
     * Offer will be handled with a chain of steps.
     *
     * @param Steps $steps
     */
    public function checks(Steps $steps): void
    {
        $this->steps = $steps;
    }


    /**
     * Return member's deserved offers.
     *
     * @param Cart $cart
     *
     * @return array
     */
    public function get_deserved(Cart $cart): array
    {
        $calculate_offer = request()->get('data')['attributes']['calculate_offers'] ?? false;

        // Get order's creation date if order is drafted except that we retrieve current date.
        $date = isset(request()->data['attributes']['creation-date']) && request()->data['attributes']['creation-date']
            ? Carbon::parse(request()->data['attributes']['creation-date'])->format('Y-m-d H:i:s')
            : now();

        return !$calculate_offer
            ? []
            : Offer::whereIn('is_active', ['active', 'ended'])
                ->where('starts_at', '<=', $date)
                ->where('ends_at', '>=', $date)
                ->get()
                ->filter(fn($offer) => $this->steps->check($offer, $cart))
                ->pluck('uuid')->toArray();
    }
}
