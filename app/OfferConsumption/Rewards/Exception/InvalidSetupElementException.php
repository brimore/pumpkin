<?php

namespace App\OfferConsumption\Rewards\Exception;

use Throwable;

class InvalidSetupElementException extends \RuntimeException
{
    public function __construct($message = "Invalid setup element", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
