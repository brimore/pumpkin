<?php

namespace App\OfferConsumption\Rewards\Exception;

use Throwable;

class InvalidMemberException extends \RuntimeException
{
    public function __construct($message = "Invalid member id", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
