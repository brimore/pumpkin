<?php

namespace App\OfferConsumption\Rewards\Exception;

use Throwable;

class OfferDoesntHaveSelectSkuRewardException extends \RuntimeException
{
    public function __construct($message = "Offer doesn't have SELECT_SKU reward", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
