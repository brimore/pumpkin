<?php

namespace App\OfferConsumption\Rewards\Exception;

use Throwable;

class OfferDoesntHaveVoucherSkuRewardException extends \RuntimeException
{
    public function __construct($message = "Offer doesn't have VOUCHER_SKU reward", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
