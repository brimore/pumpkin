<?php

namespace App\OfferConsumption\Rewards;

use App\Domain\Offer\Model\Offer;
use App\OfferConsumption\Cart\Contracts\Cart;
use App\OfferConsumption\Rewards\Exception\OfferDoesntHaveSelectSkuRewardException;
use App\OfferConsumption\Rewards\Setup\Element\Factory;
use App\OfferConsumption\Rewards\Setup\Element\Reward\SelectSku;
use App\OfferConsumption\Rewards\Setup\Multiplier;
use App\OfferConsumption\Rewards\Setup\Manager as SetupManager;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Arr;

class Rewards
{
    /**
     * @var Factory
     */
    private Factory $elementFactory;

    /**
     * @param Factory $elementFactory
     */
    public function __construct(Factory $elementFactory)
    {
        $this->elementFactory = $elementFactory;
    }

    /**
     * @param Offer $offer
     * @param Cart $cart
     * @return mixed
     */
    public function getOfferRewards(Offer $offer, Cart $cart): mixed
    {
        $multiplier = $this->createMultiplier($offer, $cart);

        $setup = $this->createSetupManager();

        return $setup->process($offer->reward->filters ?? [], $multiplier);
    }

    /**
     * @param Offer $offer
     * @param Cart $cart
     * @param $page
     * @return mixed
     */
    public function paginateGifts(Offer $offer, Cart $cart, $page): mixed
    {
        $multiplier = $this->createMultiplier($offer, $cart);

        $selectSkuReward = $this->extractSelectSkuReward($offer->reward->filters ?? []);

        $selectSkuReward['reward'] = request()->type === 'voucher' ? 'VOUCHER_SKU_EXTENDED' : 'SELECT_SKU_EXTENDED';

        if (!isset($selectSkuReward['type'])) {
            throw new OfferDoesntHaveSelectSkuRewardException();
        }

        return $this->elementFactory
            ->make($selectSkuReward, $multiplier, ['page' => $page])
            ->parse($selectSkuReward);
    }

    /**
     * @param Offer $offer
     * @param Cart $cart
     * @return mixed
     */
    public function listGiftIds(Offer $offer, Cart $cart): mixed
    {
        $multiplier = $this->createMultiplier($offer, $cart);

        $selectSkuReward = $this->extractSelectSkuReward($offer->reward->filters);
        $selectSkuReward['reward'] = request()->type === 'voucher'
            ? 'SELECT_VOUCHER_SKU_LIST_GIFT_IDS'
            : 'SELECT_SKU_LIST_GIFT_IDS';

        if (!isset($selectSkuReward['type'])) {
            throw new OfferDoesntHaveSelectSkuRewardException();
        }

        return $this->elementFactory
            ->make($selectSkuReward, $multiplier)
            ->parse($selectSkuReward);
    }

    /**
     * @param $setups
     * @return mixed|null
     */
    protected function extractSelectSkuReward($setups): mixed
    {
        $reward = request()->type === 'voucher' ? 'VOUCHER_SKU' : 'SELECT_SKU';
        foreach ($setups as $setup) {
            if (isset($setup['reward']) && $setup['reward'] === $reward) {
                return $setup;
            } else {
                foreach ($setup['filters'] as $filter) {
                    if ($filter['reward'] === $reward) {
                        return $filter;
                    }
                }
            }
        }

        return null;
    }

    /**
     * @return SetupManager
     * @throws BindingResolutionException
     */
    protected function createSetupManager(): SetupManager
    {
        return app()->make(SetupManager::class);
    }

    /**
     * @param Offer $offer
     * @param Cart $cart
     * @return Multiplier
     */
    protected function createMultiplier(Offer $offer, Cart $cart): Multiplier
    {
        return new Multiplier($offer, $cart);
    }
}
