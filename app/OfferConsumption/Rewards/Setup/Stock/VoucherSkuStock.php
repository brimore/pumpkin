<?php


namespace App\OfferConsumption\Rewards\Setup\Stock;


use App\OfferConsumption\SKUComponent\SKUList;
use Illuminate\Support\Facades\DB;

class VoucherSkuStock extends RewardStock
{
    public function addToStock($offer,$setup){
        $selected_gifts = $this->getSelectedGifts($offer);
        $this->add($selected_gifts);
        $sku_list_id = $this->getConditionValue($setup['conditions'], 'sku_list', null);
        if ($sku_list_id) {
            $SKUList = new SKUList($sku_list_id);
            $SKUList = $SKUList->build();
            $gifts_ids = $SKUList->list()->pluck('id')->toArray();
            $this->add($gifts_ids);
        }
    }
    public function getSelectedGifts($offer)
    {
        $selected_skus = collect(request()->data['attributes']['voucher_sku_selection'] ?? []);

        return collect($selected_skus->where('offerId', $offer->uuid)->first()['gifts'] ?? [])->pluck('id')->toArray();
    }
}
