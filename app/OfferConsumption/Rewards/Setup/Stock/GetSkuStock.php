<?php


namespace App\OfferConsumption\Rewards\Setup\Stock;


use App\Domain\ProductVariant\ProductVariantRepository;
use App\OfferConsumption\Rewards\Setup\Element\Reward\Concern\ParsesVariant;

class GetSkuStock extends RewardStock
{
    use ParsesVariant;
    private $productVariantRepo;
    public function __construct(ProductVariantRepository $productVariantRepo)
    {
        $this->productVariantRepo=$productVariantRepo;
    }

    public function addToStock($offer,$setup){
        $variant = $this->productVariantRepo->find(
            $this->getConditionValue($setup['conditions'], 'id')
        );
        $this->add($variant->id);
    }
}
