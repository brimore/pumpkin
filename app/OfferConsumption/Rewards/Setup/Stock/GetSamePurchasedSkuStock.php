<?php


namespace App\OfferConsumption\Rewards\Setup\Stock;

use App\OfferConsumption\Rewards\Setup\Element\Reward\Concern\ParsesVariant;
use App\OfferConsumption\SKUComponent\SKUList;

class GetSamePurchasedSkuStock extends RewardStock
{
    use ParsesVariant;

    /**
     * @param $offer
     * @param $setup
     * @return void
     */
    public function addToStock($offer, $setup){
        $skuListId = $this->getConditionValue($setup['conditions'], 'sku_list');
        $skuList = new SKUList($skuListId);
        $skuListVariantIds = $skuList->build()->list()->pluck('id')->toArray();

        $this->add($skuListVariantIds);
    }
}
