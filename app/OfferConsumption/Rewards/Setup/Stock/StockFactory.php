<?php


namespace App\OfferConsumption\Rewards\Setup\Stock;


use App\Domain\ProductVariant\ProductVariantRepository;
use Illuminate\Support\Arr;

class StockFactory
{
    const REWARD_TYPE_VOUCHER_SKU = 'VOUCHER_SKU';
    const REWARD_TYPE_GET_SKU = 'GET_SKU';
    const REWARD_TYPE_SELECT_SKU = 'SELECT_SKU';
    const REWARD_TYPE_SELECT_SKU_EXTENDED = 'SELECT_SKU_EXTENDED';
    const REWARD_TYPE_VOUCHER_SKU_EXTENDED = 'VOUCHER_SKU_EXTENDED';
    const REWARD_TYPE_GET_SAME_PURCHASED_SKU = 'GET_SAME_PURCHASED_SKU';

    public function createStockElement($setup)
    {
        $productVariantRepo=new ProductVariantRepository();
            $rewardType = Arr::get($setup, 'reward');

            switch ($rewardType) {
                case self::REWARD_TYPE_VOUCHER_SKU:
                    return new VoucherSkuStock();
                case self::REWARD_TYPE_GET_SKU:
                    return new GetSkuStock($productVariantRepo);
                case self::REWARD_TYPE_SELECT_SKU:
                    return new SelectSkuStock();
                case self::REWARD_TYPE_SELECT_SKU_EXTENDED:
                    return new SelectSkuExtendedStock();
                case self::REWARD_TYPE_VOUCHER_SKU_EXTENDED:
                    return new VoucherSkuExtendedStock();
                case self::REWARD_TYPE_GET_SAME_PURCHASED_SKU:
                    return new GetSamePurchasedSkuStock();
                default:
                    return new RewardStock();
            }

    }
}
