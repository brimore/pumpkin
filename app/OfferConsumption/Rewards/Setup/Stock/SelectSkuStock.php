<?php


namespace App\OfferConsumption\Rewards\Setup\Stock;


use App\OfferConsumption\SKUComponent\SKUList;
use Illuminate\Support\Facades\DB;

class SelectSkuStock extends RewardStock
{
    public function addToStock($offer,$setup){
        if (empty($this->getConditionValue($setup['conditions'], 'SKUs'))) {
            $sku_list_id = $this->getConditionValue($setup['conditions'], 'sku_list', null);
            if ($sku_list_id) {
                $SKUList = new SKUList($sku_list_id);
                $SKUList = $SKUList->build();
                $gifts_ids = $SKUList->list()->pluck('id')->toArray();
                $this->add($gifts_ids);
            }
        }
    }
}
