<?php


namespace App\OfferConsumption\Rewards\Setup\Stock;


use App\OfferConsumption\Rewards\Setup\Element\Reward\Concern\HasConditions;

class RewardStock
{
    use HasConditions;

    public static $GIFTS = [];

    public function addToStock($offer, $ids)
    {

    }

    private function parseToInt($ids)
    {
        $array = [];
        if (!is_array($ids)) {
            $ids = [$ids];
        }
        foreach ($ids as $id) {
            $array[] = (int)$id;
        }
        return $array;
    }

    public function add($ids)
    {
        $ids = $this->parseToInt($ids);
        self::$GIFTS = array_unique(array_merge(self::$GIFTS, $ids));
    }

    public static function getGifts(){
        return self::$GIFTS;
    }
}
