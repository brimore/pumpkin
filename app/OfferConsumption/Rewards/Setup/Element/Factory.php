<?php

namespace App\OfferConsumption\Rewards\Setup\Element;

use App\Domain\ProductVariant\ProductVariantRepository;
use App\OfferConsumption\Rewards\Contract\SetupElement;
use App\OfferConsumption\Rewards\Exception\InvalidSetupElementException;
use App\OfferConsumption\Rewards\Setup\Element\Reward\CashBackDiscount;
use App\OfferConsumption\Rewards\Setup\Element\Reward\CashBackVoucher;
use App\OfferConsumption\Rewards\Setup\Element\Reward\Discount;
use App\OfferConsumption\Rewards\Setup\Element\Reward\GetSamePurchasedSku;
use App\OfferConsumption\Rewards\Setup\Element\Reward\GetSku;
use App\OfferConsumption\Rewards\Setup\Element\Reward\MembershipFees;
use App\OfferConsumption\Rewards\Setup\Element\Reward\SelectSku;
use App\OfferConsumption\Rewards\Setup\Element\Reward\SelectSkuExtended;
use App\OfferConsumption\Rewards\Setup\Element\Reward\SelectSkuListGiftIDs;
use App\OfferConsumption\Rewards\Setup\Element\Reward\VoucherSku;
use App\OfferConsumption\Rewards\Setup\Element\Reward\VoucherSkuListGiftIDs;
use App\OfferConsumption\Rewards\Setup\Element\Reward\ShippingFees;
use App\OfferConsumption\Rewards\Setup\Element\Reward\Voucher;
use App\OfferConsumption\Rewards\Setup\Element\Reward\VoucherSkuExtended;
use App\OfferConsumption\Rewards\Setup\Multiplier;
use App\OfferConsumption\Stock\Stock;
use Illuminate\Support\Arr;

class Factory
{
    const TYPE_FILTER_GROUP = 'filter-group';
    const TYPE_FILTER = 'filter';
    const TYPE_OPERATOR = 'logical-operator';

    const REWARD_TYPE_GET_SKU = 'GET_SKU';
    const REWARD_TYPE_SELECT_SKU = 'SELECT_SKU';
    const REWARD_TYPE_SELECT_SKU_EXTENDED = 'SELECT_SKU_EXTENDED';
    const REWARD_TYPE_VOUCHER_SKU_EXTENDED = 'VOUCHER_SKU_EXTENDED';
    const REWARD_TYPE_SELECT_SKU_LIST_GIFT_IDS = 'SELECT_SKU_LIST_GIFT_IDS';
    const REWARD_TYPE_SELECT_VOUCHER_SKU_LIST_GIFT_IDS = 'SELECT_VOUCHER_SKU_LIST_GIFT_IDS';
    const REWARD_TYPE_DISCOUNT = 'DISCOUNT';
    const REWARD_TYPE_VOUCHER = 'VOUCHER';
    const REWARD_TYPE_VOUCHER_SKU = 'VOUCHER_SKU';
    const REWARD_TYPE_MEMBERSHIP_FEES = 'MEMBERSHIP_FEES';
    const REWARD_TYPE_SHIPPING_FEES = 'SHIPPING_FEES';
    const REWARD_TYPE_CASHBACK_VOUCHER = 'CASHBACK_VOUCHER';
    const REWARD_TYPE_CASHBACK_DISCOUNT = 'CASHBACK_DISCOUNT';
    const REWARD_TYPE_GET_SAME_PURCHASED_SKU = 'GET_SAME_PURCHASED_SKU';

    /**
     * @var ProductVariantRepository
     */
    private ProductVariantRepository $productVariantRepo;
    private Stock $stock;

    /**
     * @param ProductVariantRepository $productVariantRepo
     * @param Stock $stock
     */
    public function __construct(ProductVariantRepository $productVariantRepo, Stock $stock)
    {
        $this->productVariantRepo = $productVariantRepo;
        $this->stock = $stock;
    }

    /**
     * @param $setup
     * @param Multiplier $multiplier
     * @param $paginationConfig
     * @return SetupElement
     */
    public function make($setup, Multiplier $multiplier, $paginationConfig = null): SetupElement
    {
        $type = Arr::get($setup, 'type');

        switch ($type) {
            case self::TYPE_FILTER_GROUP:
                return new FilterGroup($this, $multiplier);
            case self::TYPE_FILTER:
                return $this->createFilterElement($setup, $multiplier, $paginationConfig);
            case self::TYPE_OPERATOR:
                return new Operator();
        }

        throw new InvalidSetupElementException();
    }

    /**
     * @param $setup
     * @param Multiplier $multiplier
     * @param $paginationConfig
     * @return CashBackDiscount|CashBackVoucher|Discount|GetSamePurchasedSku|GetSku|MembershipFees|SelectSku|SelectSkuExtended|SelectSkuListGiftIDs|ShippingFees|Voucher|VoucherSku|VoucherSkuExtended|VoucherSkuListGiftIDs
     */
    protected function createFilterElement($setup, Multiplier $multiplier, $paginationConfig)
    {
        $rewardType = Arr::get($setup, 'reward');

        switch ($rewardType) {
            case self::REWARD_TYPE_DISCOUNT:
                return new Discount($multiplier);
            case self::REWARD_TYPE_MEMBERSHIP_FEES:
                return new MembershipFees($multiplier);
            case self::REWARD_TYPE_SHIPPING_FEES:
                return new ShippingFees($multiplier);
            case self::REWARD_TYPE_VOUCHER:
                return new Voucher($multiplier, $this->stock);
            case self::REWARD_TYPE_VOUCHER_SKU:
                return new VoucherSku($multiplier, $this->stock);
            case self::REWARD_TYPE_GET_SKU:
                return new GetSku($multiplier, $this->productVariantRepo, $this->stock);
            case self::REWARD_TYPE_SELECT_SKU:
                return new SelectSku($multiplier, $this->stock);
            case self::REWARD_TYPE_SELECT_SKU_EXTENDED:
                return new SelectSkuExtended($multiplier, $this->productVariantRepo, $this->stock, $paginationConfig);
            case self::REWARD_TYPE_VOUCHER_SKU_EXTENDED:
                return new VoucherSkuExtended($multiplier, $this->productVariantRepo, $this->stock, $paginationConfig);
            case self::REWARD_TYPE_SELECT_SKU_LIST_GIFT_IDS:
                return new SelectSkuListGiftIDs($multiplier);
            case self::REWARD_TYPE_SELECT_VOUCHER_SKU_LIST_GIFT_IDS:
                return new VoucherSkuListGiftIDs($multiplier);
            case self::REWARD_TYPE_CASHBACK_VOUCHER:
                return new CashBackVoucher($multiplier, $this->stock);
            case self::REWARD_TYPE_CASHBACK_DISCOUNT:
                return new CashBackDiscount($multiplier);
            case self::REWARD_TYPE_GET_SAME_PURCHASED_SKU:
                return new GetSamePurchasedSku($multiplier, $this->productVariantRepo, $this->stock);
        }

        throw new InvalidSetupElementException();
    }
}
