<?php

namespace App\OfferConsumption\Rewards\Setup\Element\Reward;

class CashBackDiscount extends Discount
{
    /**
     * @return string
     */
    public function getType(): string
    {
        return 'cashback-discount';
    }

    /**
     * @param $setup
     * @return null
     */

    public function getPayload($setup)
    {
        $discount_payload = parent::getPayload($setup);
        $discount_payload['serviceCode'] = 551311;

        return  $discount_payload;
    }
}
