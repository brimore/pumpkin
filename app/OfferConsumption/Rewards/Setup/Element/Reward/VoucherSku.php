<?php

namespace App\OfferConsumption\Rewards\Setup\Element\Reward;

use App\Domain\ProductVariant\Model\ProductVariant;
use App\OfferConsumption\Parsers\Traits\ParserConfiguration;
use App\OfferConsumption\Rewards\Exception\OfferDoesntHaveVoucherSkuRewardException;
use App\OfferConsumption\Rewards\Setup\Element\Reward\Concern\HasConditions;
use App\OfferConsumption\Rewards\Setup\Multiplier;
use App\OfferConsumption\SKUComponent\SKUList;
use App\OfferConsumption\Stock\Stock;
use App\OfferConsumption\Stock\StockContainer;
use Illuminate\Support\Facades\DB;

class VoucherSku extends Reward
{
    use HasConditions, ParserConfiguration;

    private Stock $stock;

    public function __construct(Multiplier $multiplier, Stock $stock)
    {
        parent::__construct($multiplier);

        $this->stock = $stock;
    }

    public function getType(): string
    {
        return 'voucher-sku';
    }


    public function getPayload($setup)
    {
        $service_code = $this->getConditionValue($setup['conditions'], 'applied-on', 'price-and-points') == 'price-and-points' ? 221032 : 551311;

        $selected_gifts = $this->getSelectedGifts();
        if ($selected_gifts) {
            $variants = $this->getStock(array_column($selected_gifts, 'id'))->getAvailableVariants();
            $selected_gifts_prices = ProductVariant::whereIn('id', $variants)->sum('price');
            $pointFactor = DB::table('voucher_configuration')->first()?->point_factor ?? 3;
            return [
                'serviceCode' => $service_code,
                'voucherAmount' => (float)$this->multiply($this->getConditionValue($setup['conditions'], 'amount', 0)),
                "available_stock" => !empty($variants),
                "consumed_points" => round($selected_gifts_prices / $pointFactor, 2),
                "consumed_price" => (float)$selected_gifts_prices,
            ];
        }

        $sku_list_id = $this->getConditionValue($setup['conditions'], 'sku_list', null);
        if ($sku_list_id) {
            $SKUList = new SKUList($sku_list_id);
            $SKUList = $SKUList->build();
            $gifts_ids = $SKUList->list()->pluck('id')->toArray();

            $lowest_price = 0.0;
            if (!empty($this->getStock($gifts_ids)->getAvailableVariants())) {
                $lowest_price = ProductVariant::whereIn('id', $this->getStock($gifts_ids)->getAvailableVariants())
                    ->orderBy('price', 'ASC')
                    ->pluck('price')
                    ->first();
            }
            return [
                'serviceCode' => $service_code,
                'voucherAmount' => (float)$this->multiply($this->getConditionValue($setup['conditions'], 'amount', 0)),
                "available_stock" => !empty($this->getStock($gifts_ids)->getAvailableVariants()),
                "lowest_price" => (float)$lowest_price,
            ];
        }
        throw new OfferDoesntHaveVoucherSkuRewardException();
    }

    public function getStock($variant): StockContainer
    {
        return $this->stock->repository()->getStock($variant);
    }

    public function getSelectedGifts()
    {
        $selected_skus = collect(request()->data['attributes']['voucher_sku_selection'] ?? []);

        return $selected_skus->where('offerId', $this->multiplier->getOffer()->uuid)->first()['gifts'] ?? [];
    }

}
