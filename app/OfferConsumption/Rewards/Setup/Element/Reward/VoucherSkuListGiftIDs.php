<?php

namespace App\OfferConsumption\Rewards\Setup\Element\Reward;

use App\Domain\ProductVariant\ProductVariantFilters;
use App\OfferConsumption\Rewards\Exception\OfferDoesntHaveVoucherSkuRewardException;
use App\OfferConsumption\Rewards\Setup\Element\Reward\Concern\HasConditions;
use App\OfferConsumption\Rewards\Setup\Element\Reward\Concern\ParsesVariant;
use App\OfferConsumption\Rewards\Setup\Multiplier;
use App\OfferConsumption\Rewards\Traits\StockValidation;
use App\OfferConsumption\SKUComponent\SKUList;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\DB;

class VoucherSkuListGiftIDs extends Reward
{
    use HasConditions, ParsesVariant, StockValidation;

    const TYPE = 'voucher';

    /**
     * @param Multiplier $multiplier
     */
    public function __construct(Multiplier $multiplier)
    {
        parent::__construct($multiplier);
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::TYPE;
    }

    /**
     * @param $setup
     *
     * @return string[]
     * @throws GuzzleException
     */
    public function getPayload($setup): array
    {

        $sku_list_id = $this->getConditionValue($setup['conditions'], 'sku_list', null);

        if ($sku_list_id)
        {
            $SKUList = new SKUList($sku_list_id);
            $SKUList = $SKUList->build();
            $gifts_ids = $SKUList->list()->pluck('id')->toArray();

            return self::ValidateStock($gifts_ids);
        }
        throw new OfferDoesntHaveVoucherSkuRewardException();
    }
}
