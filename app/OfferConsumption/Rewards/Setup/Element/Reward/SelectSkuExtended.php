<?php

namespace App\OfferConsumption\Rewards\Setup\Element\Reward;

use App\Domain\ProductVariant\ProductVariantFilters;
use App\Domain\ProductVariant\ProductVariantRepository;
use App\OfferConsumption\Rewards\Setup\Element\Reward\Concern\HasConditions;
use App\OfferConsumption\Rewards\Setup\Element\Reward\Concern\ParsesVariant;
use App\OfferConsumption\Rewards\Setup\Multiplier;
use App\OfferConsumption\SKUComponent\SKUList;
use App\OfferConsumption\Stock\Stock;

class SelectSkuExtended extends Reward
{
    use HasConditions, ParsesVariant;

    const TYPE = 'select-sku';

    private ProductVariantRepository $productVariantRepo;
    private Stock $stock;
    private array $paginationConfig;

    public function __construct(
        Multiplier $multiplier,
        ProductVariantRepository $productVariantRepo,
        Stock $stock,
        array $paginationConfig
    )
    {
        parent::__construct($multiplier);

        $this->productVariantRepo = $productVariantRepo;
        $this->paginationConfig = $paginationConfig;
        $this->stock = $stock;
    }

    public function getType()
    {
        return self::TYPE;
    }


    public function getPayload($setup)
    {
        if (empty($this->getConditionValue($setup['conditions'], 'SKUs'))) {
            $sku_list_id = $this->getConditionValue($setup['conditions'], 'sku_list', null);
            if ($sku_list_id) {
                $SKUList = (new SKUList($sku_list_id))->build()->list();
                $variants_stock = $this->getStock($SKUList->pluck('id')->toArray());
                $filters = new ProductVariantFilters([
                    'ids' => $variants_stock->getAvailableVariants()
                ]);

                $paginated_variants = $this->productVariantRepo->paginate($filters, $this->paginationConfig);
                collect($paginated_variants->items())
                    ->each(fn($variant) => $variant['stock'] = $variants_stock->getVariantStock($variant->id));

                return $paginated_variants;
            }
        }
        $ids = array_map(
            function ($item) {
                return trim($item);
            },
            explode(',', $this->getConditionValue($setup['conditions'], 'SKUs'))
        );
        $filters = new ProductVariantFilters([
            'ids' => $this->getStock($ids)->getAvailableVariants()
        ]);

        $variants_stock = $variants_stock = $this->getStock($ids);
        $paginated_variants = $this->productVariantRepo->paginate($filters, $this->paginationConfig);
        collect($paginated_variants->items())
            ->each(fn($variant) => $variant['stock'] = $variants_stock->getVariantStock($variant->id));

        return $paginated_variants;
    }

    public function getStock($variant)
    {
        return $this->stock->repository()->getStock($variant);
    }
}
