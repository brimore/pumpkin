<?php

namespace App\OfferConsumption\Rewards\Setup\Element\Reward;


use App\OfferConsumption\Rewards\Setup\Element\Reward\Concern\HasConditions;
use App\OfferConsumption\Rewards\Setup\Element\Reward\Concern\ParsesVariant;
use App\OfferConsumption\Rewards\Traits\StockValidation;
use App\OfferConsumption\Rewards\Setup\Multiplier;
use App\OfferConsumption\SKUComponent\SKUList;
use App\OfferConsumption\Stock\Stock;
use Illuminate\Support\Facades\DB;

class SelectSku extends Reward
{
    use HasConditions, ParsesVariant, StockValidation;

    const TYPE = 'select-sku';

    private Stock $stock;

    public function __construct(Multiplier $multiplier, Stock $stock)
    {
        parent::__construct($multiplier);

        $this->stock = $stock;
    }

    public function getType()
    {
        return self::TYPE;
    }

    public function getPayload($setup)
    {
        if (empty($this->getConditionValue($setup['conditions'], 'SKUs'))) {
            $sku_list_id = $this->getConditionValue($setup['conditions'], 'sku_list', null);
            if ($sku_list_id) {
                $SKUList = new SKUList($sku_list_id);
                $SKUList = $SKUList->build();
                $gifts_ids = $SKUList->list()->pluck('id')->toArray();
                return [
                    "available_stock" => !empty($this->getStock($gifts_ids)->getAvailableVariants())
                ];
            }
            return [
                "available_stock" => false
            ];
        }
        $ids = array_map(
            function ($item) {
                return trim($item);
            },
            explode(',', $this->getConditionValue($setup['conditions'], 'SKUs'))
        );

        return [
            "available_stock" => !empty($this->getStock($ids)->getAvailableVariants())
        ];
    }

    public function getStock(array $variants)
    {
        return $this->stock->repository()->getStock($variants);
    }
}
