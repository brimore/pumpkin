<?php

namespace App\OfferConsumption\Rewards\Setup\Element\Reward;

use App\Domain\ProductVariant\ProductVariantRepository;
use App\OfferConsumption\Rewards\Exception\InvalidMemberException;
use App\OfferConsumption\Rewards\Setup\Element\Reward\Concern\HasConditions;
use App\OfferConsumption\Rewards\Setup\Element\Reward\Concern\ParsesVariant;
use App\OfferConsumption\Rewards\Setup\Multiplier;
use App\OfferConsumption\Rewards\Traits\StockValidation;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Validation\ValidationException;
use function PHPUnit\Framework\throwException;
use App\OfferConsumption\Stock\Stock;

class GetSku extends Reward
{
    use HasConditions, ParsesVariant;

    private ProductVariantRepository $productVariantRepo;
    private Stock $stock;

    public function __construct(Multiplier $multiplier, ProductVariantRepository $productVariantRepo, Stock $stock)
    {
        parent::__construct($multiplier);

        $this->productVariantRepo = $productVariantRepo;
        $this->stock = $stock;
    }

    public function getType(): string
    {
        return 'variant';
    }


    public function getPayload($setup): ?array
    {
        $variant = $this->productVariantRepo->find(
            $this->getConditionValue($setup['conditions'], 'id')
        );
        if (empty($variant)) return null;
        $parsedVariant = $this->parseVariant($variant);

        $productStock = $this->getStock($parsedVariant)->getVariantStock($variant->id);

        $parsedVariant['out_of_stock'] = $productStock <= 0;

        return $parsedVariant;
    }

    public function getStock($variant)
    {
        return $this->stock->repository()->getStock([$variant['variantId']]);
    }
}
