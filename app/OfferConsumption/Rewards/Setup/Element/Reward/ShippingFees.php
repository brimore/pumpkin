<?php

namespace App\OfferConsumption\Rewards\Setup\Element\Reward;

use App\OfferConsumption\Rewards\Setup\Element\Reward\Concern\HasConditions;

class ShippingFees extends Discount
{
    use HasConditions;

    /**
     * @return string
     */
    public function getType()
    {
        return 'shipping-fees';
    }

    /**
     * @param $setup
     * @return null
     */
    public function getPayload($setup)
    {
        $this->multiplier->setOverwriteMultiplies(1);

        $discount_payload = parent::getPayload($setup);
        $discount_payload['discountPercentageAmount'] = 0;

        return $discount_payload;
    }
}
