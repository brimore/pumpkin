<?php

namespace App\OfferConsumption\Rewards\Setup\Element\Reward\Concern;

use App\Domain\ProductVariant\Model\ProductVariant;

trait ParsesVariant
{
    public function parseVariant(ProductVariant $variant)
    {
        return [
            'variantId' => $variant->id,
            'variantName' => [
                'en' => $variant->name,
                'ar' => $variant->name
            ],
            'variantImage' => null
        ];
    }
}
