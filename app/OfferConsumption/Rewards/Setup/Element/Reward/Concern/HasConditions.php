<?php

namespace App\OfferConsumption\Rewards\Setup\Element\Reward\Concern;

use Illuminate\Support\Arr;

trait HasConditions
{
    public function getConditionValue($conditions, $name, $default = null)
    {
        $condition = Arr::first($conditions, function ($condition) use ($name) {
            return $condition['type'] == 'condition' && $condition['attribute'] == $name;
        });

        return $condition ? $condition['value'] : $default;
    }
}
