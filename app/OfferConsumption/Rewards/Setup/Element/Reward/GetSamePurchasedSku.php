<?php

namespace App\OfferConsumption\Rewards\Setup\Element\Reward;

use App\Domain\ProductVariant\ProductVariantRepository;
use App\OfferConsumption\Cart\Variant;
use App\OfferConsumption\Rewards\Setup\Element\Reward\Concern\HasConditions;
use App\OfferConsumption\Rewards\Setup\Element\Reward\Concern\ParsesVariant;
use App\OfferConsumption\Rewards\Setup\Multiplier;
use App\OfferConsumption\SKUComponent\SKUList;
use App\OfferConsumption\Stock\StockContainer;
use Illuminate\Support\Collection;
use App\OfferConsumption\Stock\Stock;

class GetSamePurchasedSku extends Reward
{
    use HasConditions, ParsesVariant;

    /**
     * @var ProductVariantRepository
     */
    private ProductVariantRepository $productVariantRepo;
    /**
     * @var Stock
     */
    private Stock $stock;

    /**
     * @param Multiplier $multiplier
     * @param ProductVariantRepository $productVariantRepo
     * @param Stock $stock
     */
    public function __construct(Multiplier $multiplier, ProductVariantRepository $productVariantRepo, Stock $stock)
    {
        parent::__construct($multiplier);

        $this->productVariantRepo = $productVariantRepo;
        $this->stock = $stock;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'variants';
    }


    /**
     * @param $setup
     * @return null[]|null
     */
    public function getPayload($setup): ?array
    {

        $rewardSkuList = new SKUList($this->getConditionValue($setup['conditions'], 'sku_list'));

        $conditionSkuListId = $this->multiplier->getOffer()->condition()->first()->criteria[0]['conditions'][0]['value'] ?? 0;
        $conditionSkuList = new SKUList($conditionSkuListId);

        $deservedVariants = $this->getDeservedVariants($conditionSkuList, $rewardSkuList);

        return ['variants' => $rewardSkuList->getSameAsPurchase() ? $deservedVariants : null];
    }

    /**
     * @param $variant
     * @return StockContainer
     */
    public function getStock($variant)
    {
        return $this->stock->repository()->getStock([$variant['variantId']]);
    }

    /**
     * @param $conditionSkuList
     * @param $rewardSkuList
     *
     * @return array
     */
    public function getDeservedVariants($conditionSkuList, $rewardSkuList)
    {
        $updatedCartVariants = Collection::make($this->multiplier->getCart()->getVariants())
            ->map(function (Variant $variant) {
                return ['id' => $variant->getId(), 'price' => $variant->getPrice(), 'quantity' => $variant->getQuantity()];
            });

        $skuListVariants = $conditionSkuList->build()->list();

        $cartVariants = Collection::make(request()->all()['data']['attributes']['cart']);

        $deservedVariants = $cartVariants
            ->groupBy('variantId')
            ->map(function ($variants, $variantID) use ($conditionSkuList, $updatedCartVariants, $skuListVariants, $rewardSkuList) {
                if ($skuListVariants->contains('id', $variantID)) {
                    $variant = $this->productVariantRepo->find($variants->first()['variantId']);

                    if (empty($variant)) return false;

                    $parsedVariant = $this->parseVariant($variant);
                    $productStock = $this->getStock($parsedVariant)->getVariantStock($variant->id);

                    $quantity = $this->getUpdatedQuantity($updatedCartVariants, $variants);

                    $parsedVariant['multiple'] = floor($quantity / $conditionSkuList->getCondition()['value']) * $rewardSkuList->getCondition()['value'];
                    $parsedVariant['out_of_stock'] = $productStock < $parsedVariant['multiple'];

                    return $parsedVariant['multiple'] ? $parsedVariant : false;
                }
                return false;
            })->filter()->toArray();

        $deservedVariants = array_values($deservedVariants);

        return $this->applyCapping($deservedVariants, $rewardSkuList);
    }

    /**
     * @param Collection $updatedCartVariants
     * @param Collection $currentCartVariants
     * @return float
     */
    private function getUpdatedQuantity(Collection $updatedCartVariants, Collection $currentCartVariants): float
    {
        $updatedVariant = $updatedCartVariants->where('id', $currentCartVariants->first()['variantId']);
        $originalPrice = $currentCartVariants->sum('price') * $currentCartVariants->sum('quantity');
        $updatedPrice = $updatedVariant->sum('price') * $updatedVariant->sum('quantity');

        if (!$updatedPrice)
            return 0;

        if ($updatedPrice != $originalPrice) {
            $quantity = $updatedVariant->sum('quantity') - ceil($originalPrice / $updatedPrice);
            return $quantity < 0 ? 0 : $quantity;
        }

        return $updatedVariant->sum('quantity');
    }

    /**
     * @param array $deservedVariants
     * @param $rewardSkuList
     * @return array
     */
    private function applyCapping(array $deservedVariants, $rewardSkuList): array
    {
        $rewardMultiples = $rewardSkuList->getCondition()['value'];
        $limit = $rewardMultiples * $this->multiplier->getMultiple();
        $deservedVariants = Collection::make($deservedVariants)->map(function ($variant) use (&$limit) {
            $variant['multiple'] = $variant['multiple'] >= $limit && $limit >= 0 ? $limit : $variant['multiple'];
            $limit -= $variant['multiple'];

            return $variant;
        })->filter(fn($variant) => $variant['multiple']);

        $this->multiplier->setOverwriteMultiplies($deservedVariants->sum('multiple'));

        return $deservedVariants->toArray();
    }
}
