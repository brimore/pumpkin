<?php

namespace App\OfferConsumption\Rewards\Setup\Element\Reward;

use App\Domain\Member\Model\Member;
use App\Domain\Offer\Model\Offer;
use App\OfferConsumption\Multiples\MultiplesFilters;
use App\OfferConsumption\Parsers\Traits\ParserConfiguration;
use App\OfferConsumption\Rewards\Setup\Element\Reward\Concern\HasConditions;
use App\OfferConsumption\Rewards\Setup\Multiplier;
use App\OfferConsumption\SKUComponent\SKUList;
use App\OfferConsumption\Stock\Stock;
use App\OfferConsumption\Stock\StockContainer;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Voucher extends Reward
{
    use HasConditions, ParserConfiguration;

    /**
     * @var Stock
     */
    private Stock $stock;

    /**
     * @param Multiplier $multiplier
     * @param Stock $stock
     */
    public function __construct(Multiplier $multiplier, Stock $stock)
    {
        parent::__construct($multiplier);

        $this->stock = $stock;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'voucher';
    }


    /**
     * @param $setup
     * @return array
     */
    public function getPayload($setup)
    {
        $applied_on = $this->getConditionValue($setup['conditions'], 'applied-on', 'price-and-points');

        $this->multiplier->setOverwriteMultiplies($this->getMultiplies($setup));

        return [
            'applied_on' => $applied_on,
            'serviceCode' => ($applied_on === 'price-and-points' ? 221042 : 551311),
            'voucherAmount' => (float)($this->getConditionValue($setup['conditions'], 'amount', 0) * $this->multiplier->getMultiple()),
            'consumed' => $this->getConsumption($setup),
            'consumedPoints' => $this->getConsumption($setup) / $this->getPointFactor()
        ];
    }

    /**
     * @param $setup
     * @return float
     */
    public function getCondition($setup): float
    {
        $criteria = $this->multiplier->getOffer()->condition()->first()?->toArray()['criteria'];

        $filters = self::MapCriteria($criteria);
        $filtered_achieve_points = current(array_filter($filters, function ($var) {
            if (!isset($var['condition']))
                return false;
            return ($var['condition'] === 'ACHIEVE_POINTS') || ($var['condition'] === 'BUY_SKU');
        }));

        $condition = $this->getConditionPoints($filtered_achieve_points['conditions']);
        $points = Collection::make($condition)->where('type', 'points')->first()['value'] ?? 0;

        return (float)$points * $this->getPointFactor();
    }

    /**
     * @param $setup
     * @return float
     */
    public function getConsumption($setup): float
    {
        $cart = $this->getAchievedPoints($setup, true);
        $condition = $this->getCondition($setup);
        $voucher = (float)$this->getConditionValue($setup['conditions'], 'amount', 0);
        $multiply = $this->multiplier->getMultiple();

        return (($cart - $condition * $multiply) < $voucher * $multiply) ? ($cart - $condition * $multiply) : $voucher * $multiply;
    }

    public function getAchievedPoints($setup, $ignore_accumulation = false): float|int
    {
        $criteria = $this->multiplier->getOffer()->condition()->first()?->toArray()['criteria'];

        $filters = self::MapCriteria($criteria);
        $filtered_achieve_points = current(array_filter($filters, function ($var) {
            if (!isset($var['condition']))
                return false;
            return ($var['condition'] === 'ACHIEVE_POINTS') || ($var['condition'] === 'BUY_SKU');
        }));

        $condition = $this->getConditionPoints($filtered_achieve_points['conditions']);

        $condition_values = Collection::make($condition);
        // Get points and get its type (accumulative or per-order).
        $is_accumulative = ($condition_values->where('type', 'accumulation')->first()['value'] ?? null) === 'accumulative';

        $subtotal = $this->multiplier->getCart()->getSubtotal('points');

        $offer_consumption_points = $this->offerConsumptionPoints($this->multiplier->getOffer(), Auth::guard('members')->user());
        $accumulation_pts = $this->getAccumulativePoints($this->multiplier->getOffer(), Auth::guard('members')->user());

        $remaining_pts = ($accumulation_pts - $offer_consumption_points);
        $offer_condition = $this->getCondition($setup) / $this->getPointFactor();

        if ((($remaining_pts >= $offer_condition) && $ignore_accumulation && ($subtotal < $offer_condition))) {
            return ($subtotal + ($offer_condition * $this->multiplier->getMultiple())) * $this->getPointFactor();
        }
        return !$is_accumulative
            ? $subtotal * $this->getPointFactor()
            : ($remaining_pts + $subtotal) * $this->getPointFactor();
    }

    public function getAccumulativePoints(Offer $offer, Member $member): float
    {
        return (float)DB::table('member_points')
            ->where('member_id', $member->id)
            ->whereDate('order_creation_date', '>=', $offer->starts_at)
            ->whereDate('order_creation_date', '<=', $offer->ends_at)
            ->sum('total_points');
    }

    /**
     * @param $setup
     * @return int
     */
    public function getMultiplies($setup): int
    {
        $cart = $this->getAchievedPoints($setup);
        $condition = $this->getCondition($setup);
        $voucher = (float)$this->getConditionValue($setup['conditions'], 'amount', 0);
        $actual_multiplies = floor(($cart - $condition) / ($condition + $voucher)) + 1;

        return $this->multiplier->getOffer()->multiples
            ? min((new MultiplesFilters($this->multiplier->getOffer()))->apply(), $actual_multiplies)
            : $actual_multiplies;
    }

    /**
     * @param $variant
     * @return StockContainer
     */
    public function getStock($variant): StockContainer
    {
        return $this->stock->repository()->getStock([$variant]);
    }

    /**
     * @param $conditions
     * @return array|mixed|null
     */
    private function getConditionPoints($conditions)
    {
        $skuListId = $this->getConditionValue($conditions, 'sku_list', 0);
        if ($skuListId) {
            $condition = ['type' => 'per_order', 'value' => (new SKUList($skuListId))->getCondition()['value']];
        } else {
            $condition = $this->getConditionValue($conditions, 'points', 0);
        }
        return $condition;
    }

    /**
     * @return mixed
     */
    private function getPointFactor()
    {
        return DB::table('voucher_configuration')->first()?->point_factor ?? 3;
    }
}
