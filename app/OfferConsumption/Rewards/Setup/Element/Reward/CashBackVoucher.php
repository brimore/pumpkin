<?php

namespace App\OfferConsumption\Rewards\Setup\Element\Reward;

class CashBackVoucher extends Voucher
{
    /**
     * @return string
     */
    public function getType(): string
    {
        return 'cashback-voucher';
    }

    /**
     * @param $setup
     * @return null
     */

    public function getPayload($setup)
    {
        $voucher_payload = parent::getPayload($setup);
        $voucher_payload['serviceCode'] = 221042;

        return  $voucher_payload;
    }
}
