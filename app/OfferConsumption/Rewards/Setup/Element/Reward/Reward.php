<?php

namespace App\OfferConsumption\Rewards\Setup\Element\Reward;

use App\OfferConsumption\Rewards\Contract\SetupElement;
use App\OfferConsumption\Rewards\Setup\Multiplier;

abstract class Reward implements SetupElement
{
    protected Multiplier $multiplier;

    public function __construct(Multiplier $multiplier)
    {
        $this->multiplier = $multiplier;
    }

    public function parse($setup)
    {
        return [
            'giftType' => $this->getType(),
            'payload' => $this->getPayload($setup),
            'is_accumulative' => $this->multiplier->getOffer()->is_accumulative,
            'multiple' => $this->multiplier->getMultiple()
        ];
    }

    protected function multiply($amount)
    {
        return $amount * $this->multiplier->getMultiple();
    }

    abstract public function getType();

    abstract public function getPayload($setup);
}
