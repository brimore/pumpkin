<?php

namespace App\OfferConsumption\Rewards\Setup\Element\Reward;

use App\OfferConsumption\Rewards\Setup\Element\Reward\Concern\HasConditions;

class MembershipFees extends Discount
{
    use HasConditions;

    /**
     * @return string
     */
    public function getType()
    {
        return 'membership-fees';
    }

    /**
     * @param $setup
     * @return null
     */
    public function getPayload($setup)
    {
        $this->multiplier->setOverwriteMultiplies(1);

        return null;
    }
}
