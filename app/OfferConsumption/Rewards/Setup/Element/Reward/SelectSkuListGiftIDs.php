<?php

namespace App\OfferConsumption\Rewards\Setup\Element\Reward;

use App\OfferConsumption\Rewards\Setup\Element\Reward\Concern\HasConditions;
use App\OfferConsumption\Rewards\Setup\Element\Reward\Concern\ParsesVariant;
use App\OfferConsumption\Rewards\Setup\Multiplier;

class SelectSkuListGiftIDs extends Reward
{
    use HasConditions, ParsesVariant;

    const TYPE = 'select-sku';

    /**
     * @param Multiplier $multiplier
     */
    public function __construct(Multiplier $multiplier)
    {
        parent::__construct($multiplier);
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::TYPE;
    }

    /**
     * @param $setup
     *
     * @return false|string[]
     */
    public function getPayload($setup)
    {
        return explode(',', $this->getConditionValue($setup['conditions'], 'SKUs'));
    }
}
