<?php

namespace App\OfferConsumption\Rewards\Setup\Element\Reward;


use App\OfferConsumption\Rewards\Setup\Element\Reward\Concern\HasConditions;
use App\OfferConsumption\SKUComponent\SKUList;
use Illuminate\Support\Arr;


class Discount extends Reward
{
    use HasConditions;

    public function getType()
    {
        return 'discount';
    }

    public function getPayload($setup)
    {
        $type = $this->getConditionValue($setup['conditions'], 'type', 'percentage');
        $amount = $this->getConditionValue($setup['conditions'], 'amount', 0);
        $discount_amount = (float)($type === 'percentage' ? $amount : $this->multiply($amount));
        $discount_percentage_amount = (float)($type === 'percentage' ? $this->discountCart($setup) * ($amount / 100) : 0);

        if ($type === 'percentage')
            $this->multiplier->setOverwriteMultiplies(1);

        return [
            'serviceCode' => 551311,
            'discountType' => $type,
            'discountAmount' => $discount_amount,
            'discountPercentageAmount' => $discount_percentage_amount
        ];
    }

    public function discountCart($setup)
    {
        $inCondition = collect();
        $conditions = array_filter($setup['conditions'], function ($row) {
            if (isset($row['attribute']) && ($row['attribute'] == 'sku_list')) {
                return $row;
            }
        });
        foreach ($conditions as $condition) {
            $SKUList = new SKUList($condition['value']);
            $inCondition = $SKUList->build()->list();
        }
        $only = $inCondition->pluck('id')->toArray();
        $totalCartPrice = $this->multiplier->getCart()->getSubtotal();
        $totalVariantPrice = $this->multiplier->getCart()->getCartVariantPrice($only);
        return $totalCartPrice < $totalVariantPrice ? $totalCartPrice : $totalVariantPrice;
    }

}

