<?php

namespace App\OfferConsumption\Rewards\Setup\Element;

use App\OfferConsumption\Rewards\Contract\SetupElement;
use App\OfferConsumption\Rewards\Setup\Multiplier;

class FilterGroup implements SetupElement
{
    /**
     * @var Factory
     */
    private Factory $elementFactory;
    /**
     * @var Multiplier
     */
    private Multiplier $multiplier;

    /**
     * @param Factory $elementFactory
     * @param Multiplier $multiplier
     */
    public function __construct(Factory $elementFactory, Multiplier $multiplier)
    {
        $this->elementFactory = $elementFactory;
        $this->multiplier = $multiplier;
    }

    /**
     * @param $setup
     * @return array
     */
    public function parse($setup)
    {
        $parsed = [
            'type' => 'filter-group',
            'rewards' => []
        ];
        foreach ($setup['filters'] as $element) {
            $parsed['rewards'][] = $this->elementFactory->make($element, $this->multiplier)->parse($element);
        }

        return $parsed;
    }
}
