<?php

namespace App\OfferConsumption\Rewards\Setup;

use App\OfferConsumption\Rewards\Setup\Element\Factory;

class Manager
{
    /**
     * @var Factory
     */
    private Factory $setupElementFactory;

    /**
     * @param Factory $setupElementFactory
     */
    public function __construct(Factory $setupElementFactory)
    {
        $this->setupElementFactory = $setupElementFactory;
    }

    /**
     * @param $setup
     * @param Multiplier $multiplier
     * @return mixed
     */
    public function process($setup, Multiplier $multiplier): mixed
    {
        $setup = $this->addRootFilterGroup($setup);

        return $this->setupElementFactory->make($setup, $multiplier)->parse($setup);
    }

    /**
     * @param $rootSetup
     * @return array
     */
    public function addRootFilterGroup($rootSetup): array
    {
        return [
            'type' => 'filter-group',
            'filters' => $rootSetup
        ];
    }
}
