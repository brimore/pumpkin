<?php

namespace App\OfferConsumption\Rewards\Setup;

use App\Domain\Offer\Model\Offer;
use App\OfferConsumption\Cart\Contracts\Cart;
use App\OfferConsumption\Parsers\Factories\ConditionFactory;
use App\OfferConsumption\Parsers\Parser;

class Multiplier
{
    /**
     * @var Offer
     */
    private Offer $offer;

    /**
     * @var Cart
     */
    private Cart $cart;

    /**
     * @var int
     */
    private int $overwrite_multiplies = 0;

    /**
     * @param Offer $offer
     * @param Cart $cart
     */
    public function __construct(Offer $offer, Cart $cart)
    {
        $this->offer = $offer;
        $this->cart = $cart;
    }

    /**
     * @param int $overwrite_multiplies
     */
    public function setOverwriteMultiplies(int $overwrite_multiplies): void
    {
        $this->overwrite_multiplies = $overwrite_multiplies;
    }

    /**
     * @return Cart
     */
    public function getCart(): Cart
    {
        return $this->cart;
    }

    /**
     * @return Offer
     */
    public function getOffer(): Offer
    {
        return $this->offer;
    }

    /**
     * @return float
     */
    public function getMultiple(): float
    {
        if ($this->overwrite_multiplies)
            return $this->overwrite_multiplies;

        if (isset(ConditionFactory::$MULTIPLIES[$this->offer->id]['multiplies']))
            return ConditionFactory::$MULTIPLIES[$this->offer->id]['multiplies'];

        if ($this->offer?->condition?->criteria) {
            $parser = new Parser($this->offer->condition->criteria);
            return $parser->parse(new ConditionFactory($this->cart, $this->offer))->get();
        }

        return 0;
    }
}
