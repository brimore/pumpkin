<?php

namespace App\OfferConsumption\Rewards\Contract;

interface SetupElement
{
    public function parse($setup);
}
