<?php

namespace App\OfferConsumption\Rewards;


use App\Domain\Offer\Model\Offer;
use App\OfferConsumption\Cart\Contracts\Cart;
use App\OfferConsumption\DeservedOffers\DeservedOffer;
use App\OfferConsumption\InteractionGroups\ProcessedDeservedOfferList;
use App\OfferConsumption\Parsers\Traits\ParserConfiguration;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class VoucherConsumption
{
    use ParserConfiguration;

    /**
     * @var ProcessedDeservedOfferList
     */
    public ProcessedDeservedOfferList $deservedOfferList;

    /**
     * @var Cart
     */
    public Cart $cart;

    /**
     * @var $pointFactor
     */
    public $pointFactor;

    /**
     * @param ProcessedDeservedOfferList $deservedOfferList
     * @param Cart $cart
     */
    public function __construct(ProcessedDeservedOfferList $deservedOfferList, Cart $cart)
    {
        $this->deservedOfferList = $deservedOfferList;
        $this->cart = $cart;
        $this->pointFactor = DB::table('voucher_configuration')->first()?->point_factor ?? 3;
    }

    /**
     * @return ProcessedDeservedOfferList
     */
    public function recalculate()
    {
        $totalAmountWithVoucher = 0;
        $currentOffer = Offer::whereUuid($this->extractVoucherOffers()->first()?->getId())->first();
        $accumulatePoints = $currentOffer
            ? $this->memberAccumulativePoints(Auth::guard('members')->user(), $currentOffer) * $this->pointFactor : 0;
        $cartSubtotal = $this->cart->getSubtotal() + $accumulatePoints;
        $totalVoucherAmount = 0;

        $this->extractVoucherOffers()->each(function (DeservedOffer $offer) use (&$totalAmountWithVoucher, &$totalVoucherAmount, &$cartSubtotal) {
            $currentOffer = Offer::whereUuid($offer->getId())->first();
            $voucherAmount = Arr::get($offer->getPayload(), 'deserved_rewards.rewards.0.payload.voucherAmount');
            $multiplies = Arr::get($offer->getPayload(), 'deserved_rewards.rewards.0.multiple');
            $totalAmountWithVoucher += ($currentOffer->condition_points * $multiplies * $this->pointFactor) + ($voucherAmount);
            $totalVoucherAmount += $voucherAmount;
        });

        $this->calculateVoucherConsumption($cartSubtotal, $totalAmountWithVoucher, $totalVoucherAmount);

        return $this->deservedOfferList;
    }

    /**
     * @return Collection
     */
    public function extractVoucherOffers()
    {
        return $this->deservedOfferList->getEntries()->filter(function (DeservedOffer $offer) {
            return Arr::get($offer->getPayload(), 'deserved_rewards.rewards.0.giftType') == 'voucher';
        });
    }

    /**
     * @param  $cartSubtotal
     * @param $totalAmountWithVoucher
     * @param $totalVoucherAmount
     */
    private function calculateVoucherConsumption($cartSubtotal, $totalAmountWithVoucher, $totalVoucherAmount)
    {
        $voucherConsumption = $cartSubtotal >= $totalAmountWithVoucher
            ? $totalVoucherAmount
            : $totalVoucherAmount - ($totalAmountWithVoucher - $cartSubtotal);

        $this->extractVoucherOffers()->map(function (DeservedOffer $offer) use (&$voucherConsumption) {
            $voucherAmount = Arr::get($offer->getPayload(), 'deserved_rewards.rewards.0.payload.voucherAmount');

            $newVoucherConsumption = $voucherConsumption < 0 ? 0 : min($voucherConsumption, $voucherAmount);

            $voucherConsumption -= $voucherAmount;

            $offer->setVoucherConsumption($newVoucherConsumption, ($newVoucherConsumption / $this->pointFactor));

            return $offer;
        });
    }
}
