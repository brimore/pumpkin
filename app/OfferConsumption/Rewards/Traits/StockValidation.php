<?php

namespace App\OfferConsumption\Rewards\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Validation\ValidationException;

trait StockValidation
{

    /**
     * @throws GuzzleException
     */
    private static function ValidateStock(array $variants): array
    {
        if (empty($variants)) {
            return [];
        }
//        $member_id= auth()->guard('members')->id();
//        if($member_id === null)
//        {
//            throw new InvalidMemberException();
//        }
        $client = new Client();
        $endpoint = "https://refactor-testing.brimore.com/api/variants-quantity";
        $postInput = ['variant_ids' => $variants];
        $headers = [];
        $response = $client->request('POST', $endpoint, ['json' => $postInput, 'headers' => $headers]);
        $responseBody = json_decode($response->getBody()->getContents(), true);
        $stock = array_filter($responseBody['data'], function ($variant)
        {
            return $variant['available_quantity'] > 0;
        });

        return (array_map(
            function ($item) {
                return $item["id"];
            },
            $stock
        ));

    }
    /**
     * @throws GuzzleException
     * @throws ValidationException
     */
    private static function getStock($variant)
    {
//        $member_id= auth()->guard('members')->id();
//        if($member_id === null)
//        {
//            throw new InvalidMemberException();
//        }
        $client = new Client();
        $endpoint = "https://refactor-testing.brimore.com/api/variants-quantity";
        $postInput = ['variant_ids' => [ $variant['variantId'] ]];
        $headers = [];
        $response = $client->request('POST', $endpoint, ['json' => $postInput, 'headers' => $headers]);
        $responseBody = json_decode($response->getBody()->getContents(), true);
        $statusCode = $response->getStatusCode();
        $firstkey = 'data';
        $secondkey = 'available_quantity';
        if (array_key_exists($firstkey, $responseBody)) {
            if (isset($responseBody[$firstkey][0]) && array_key_exists($secondkey, $responseBody[$firstkey][0])) {
                return ($responseBody[$firstkey][0][$secondkey]);
            }
        }
        throw ValidationException::withMessages(['stock' => 'Unable to check stock']);
    }

}
