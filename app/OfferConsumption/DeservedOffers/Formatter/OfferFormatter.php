<?php

namespace App\OfferConsumption\DeservedOffers\Formatter;

use App\Domain\Common\HasLocalizedAttribute;
use App\OfferConsumption\Cart\Contracts\Cart;
use App\OfferConsumption\DeservedOffers\Contract\Formatter;
use App\OfferConsumption\DeservedOffers\DeservedOffer;
use App\OfferConsumption\Rewards\Rewards;

class OfferFormatter implements Formatter
{
    use HasLocalizedAttribute;

    private Rewards $rewards;

    public function __construct(Rewards $rewards)
    {
        $this->rewards = $rewards;
    }

    public function format($source, Cart $cart): DeservedOffer
    {
        return new DeservedOffer($source->uuid, 'offer', $this->getPayload($source, $cart));
    }

    public function getPayload($source, Cart $cart): array
    {
        return [
            'id' => $source->uuid,
            'legacy_id' => 5021,
            'title' => $this->formatLocalizedAttribute($source, 'title'),
            'image' => $source->image?->image,
            'brief' => $source->brief,
            'deserved_rewards' => $this->rewards->getOfferRewards($source, $cart)
        ];
    }
}
