<?php

namespace App\OfferConsumption\DeservedOffers\Formatter;

use App\Domain\Common\HasLocalizedAttribute;
use App\Domain\InteractionGroup\Model\InteractionGroup;
use App\OfferConsumption\Cart\Contracts\Cart;
use App\OfferConsumption\DeservedOffers\Contract\Formatter;
use App\OfferConsumption\DeservedOffers\DeservedOffer;

class InteractionGroupFormatter implements Formatter
{
    use HasLocalizedAttribute;

    /**
     * @param InteractionGroup $source
     */
    public function format($source, $cart): DeservedOffer
    {
        return new DeservedOffer($source->uuid, 'selection_group', $this->getPayload($source, $cart));
    }

    /**
     * @param InteractionGroup $source
     */
    public function getPayload($source, Cart $cart): array
    {
        return [
            'title' => $this->formatLocalizedAttribute($source, 'title'),
            'description' => $this->formatLocalizedAttribute($source, 'description'),
            'limit' => $source->limit,
            'offers' => array_map(function (DeservedOffer $deservedOffer) {
                return $deservedOffer->getPayload();
            }, $source->getAppliedOffers())
        ];
    }
}
