<?php

namespace App\OfferConsumption\DeservedOffers\Contract;

use App\OfferConsumption\Cart\Contracts\Cart;
use App\OfferConsumption\DeservedOffers\DeservedOffer;

interface Formatter
{
    public function format($source, Cart $cart): DeservedOffer;

    public function getPayload($source, Cart $cart): array;
}
