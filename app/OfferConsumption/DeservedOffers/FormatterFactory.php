<?php

namespace App\OfferConsumption\DeservedOffers;

use App\Domain\InteractionGroup\Model\InteractionGroup;
use App\Domain\Offer\Model\Offer;
use App\OfferConsumption\DeservedOffers\Contract\Formatter;
use App\OfferConsumption\DeservedOffers\Formatter\InteractionGroupFormatter;
use App\OfferConsumption\DeservedOffers\Formatter\OfferFormatter;

class FormatterFactory
{
    public function make($source): Formatter
    {
        switch (get_class($source)) {
            case Offer::class:
                return app()->make(OfferFormatter::class);
            case InteractionGroup::class:
                return app()->make(InteractionGroupFormatter::class);
        }

        throw new \Exception('Invalid source');
    }
}
