<?php

namespace App\OfferConsumption\DeservedOffers;

use Illuminate\Support\Arr;

class DeservedOffer
{
    private string $id;
    private string $type;
    private array $payload;

    public function __construct($id, $type, array $payload = [])
    {
        $this->id = $id;
        $this->type = $type;
        $this->payload = $payload;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getPayload(): array
    {
        return $this->payload;
    }

    public function setMultiple($newMultiple)
    {
        Arr::set(
            $this->payload,
            'deserved_rewards.rewards.0.multiple',
            $newMultiple
        ); // @ToDo: Handle multiple rewards

        return $this;
    }

    public function setVoucherConsumption($newConsumption, $newConsumedPoints)
    {
        Arr::set(
            $this->payload,
            'deserved_rewards.rewards.0.payload.consumed',
            $newConsumption
        ); // @ToDo: Handle multiple rewards

        Arr::set(
            $this->payload,
            'deserved_rewards.rewards.0.payload.consumedPoints',
            $newConsumedPoints
        ); // @ToDo: Handle multiple rewards

        return $this;
    }

    public function setVoucherAmount($newConsumption)
    {
        Arr::set(
            $this->payload,
            'deserved_rewards.rewards.0.payload.voucherAmount',
            $newConsumption
        ); // @ToDo: Handle multiple rewards

        return $this;
    }
}
