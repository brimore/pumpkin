<?php

namespace App\OfferConsumption\Stock;

use App\OfferConsumption\Stock\Repository\FixedStockRepository;
use App\OfferConsumption\Stock\Repository\LiveStockRepository;

class Stock
{
    protected array $repos = [];

    public function repository($name = null)
    {
        $name = $name ?: config('stock.default_repo');

        return $this->repos[$name] ?? $this->repos[$name] = $this->resolve($name);
    }

    protected function resolve($name)
    {
        switch ($name) {
            case 'live':
                return app()->make(LiveStockRepository::class);
            case 'fixed':
                return new FixedStockRepository();
        }

        throw new \Exception('Invalid repository type');
    }

    public function __call($method, $parameters)
    {
        return $this->repository()->{$method}(...$parameters);
    }
}
