<?php

namespace App\OfferConsumption\Stock\Repository;

use App\OfferConsumption\Stock\Contract\Repository;
use App\OfferConsumption\Stock\StockContainer;

class FixedStockRepository implements Repository
{
    public function getStock($variantIds): StockContainer
    {
        $parsed = array_reduce($variantIds, function ($carry, $newItem) {
            $carry[$newItem] = config('stock.repos.fixed.amount');

            return $carry;
        }, []);

        return new StockContainer($parsed);
    }
}
