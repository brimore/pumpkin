<?php

namespace App\OfferConsumption\Stock\Repository;

use App\OfferConsumption\Rewards\Setup\Stock\RewardStock;
use App\OfferConsumption\Stock\Contract\Repository;
use App\OfferConsumption\Stock\StockContainer;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class LiveStockRepository implements Repository
{
    private Client $client;

    private static $giftsStock = [];

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getStock($variantIds): StockContainer
    {
        try {
            $parsed = array_reduce($this->getData($variantIds), function ($carry, $newItem) {
                $carry[$newItem['id']] = $newItem['available_quantity'];

                return $carry;
            }, []);
        } catch (RequestException $e) {
            $parsed = array_fill_keys(array_keys(array_flip($variantIds)), 0);
        }
        return new StockContainer($parsed);
    }

    public function getData($variantIds)
    {
        if (count(self::$giftsStock) == 0) {
            $stock = $this->client->post(
                config('stock.repos.live.endpoint'),
                ['json' => ['variant_ids' => RewardStock::getGifts()]]
            );
            $response = json_decode($stock->getBody()->getContents(), true);
            self::$giftsStock = $response['data'];
        }

        $getVariantsAvailable = array_filter(self::$giftsStock, function ($item) use ($variantIds) {
            return in_array($item['id'], $variantIds);
        });
        return $getVariantsAvailable;

    }


}
