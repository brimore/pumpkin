<?php

namespace App\OfferConsumption\Stock;


use Illuminate\Support\Collection;

class StockContainer extends Collection
{
    public function getVariantStock($variantId)
    {
        return $this->get($variantId) ?? 0;
    }

    public function getAvailableVariants()
    {
        return array_keys(array_filter($this->items));
    }
}
