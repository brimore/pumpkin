<?php

namespace App\OfferConsumption\Stock\Contract;

use App\OfferConsumption\Stock\StockContainer;

interface Repository
{
    public function getStock($variantIds): StockContainer;
}
