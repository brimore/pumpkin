<?php

namespace App\OfferConsumption\InteractionGroups\SelectionStrategy;

use App\Domain\InteractionGroup\Model\InteractionGroup;
use App\OfferConsumption\Cart\Contracts\Cart;
use App\OfferConsumption\InteractionGroups\Contract\Strategy;
use App\OfferConsumption\InteractionGroups\ProcessedDeservedOfferList;

class HighestPriorityFirst extends AbstractStrategy implements Strategy
{
    public function process(InteractionGroup $interactionGroup, ProcessedDeservedOfferList $list, Cart $cart)
    {
        $orderedEntries = $this->extractOriginalEntries($interactionGroup, $list)->sortBy(function ($entry) {
            return $entry['order'];
        });

        $previousEntry = null;
        foreach ($orderedEntries as $entry) {
            if (!$previousEntry || $entry['order'] == $previousEntry['order']) {
                $list->addEntry($entry['offer']);

                $previousEntry = $entry;
            } else {
                break;
            }
        }
    }
}
