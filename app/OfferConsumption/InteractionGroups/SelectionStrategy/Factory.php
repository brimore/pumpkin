<?php

namespace App\OfferConsumption\InteractionGroups\SelectionStrategy;

use App\Domain\InteractionGroup\Model\InteractionGroup;
use App\OfferConsumption\InteractionGroups\Contract\Strategy;

class Factory
{
    public function make($strategy): Strategy
    {
        switch ($strategy) {
            case InteractionGroup::SELECTION_STRATEGY_HIGHEST_PRIORITY_FIRST:
                return app()->make(HighestPriorityFirst::class);
            case InteractionGroup::SELECTION_STRATEGY_RANDOM:
                return app()->make(Random::class);
            case InteractionGroup::SELECTION_STRATEGY_USER_CAN_SELECT:
                return app()->make(UserCanSelect::class);
            case InteractionGroup::SELECTION_BLOCKING_SUBTOTAL:
                return app()->make(BlockingSubtotalStrategy::class);
        }

        throw new \Exception('Invalid strategy');
    }
}
