<?php

namespace App\OfferConsumption\InteractionGroups\SelectionStrategy;

use App\Domain\InteractionGroup\Model\InteractionGroup;
use App\OfferConsumption\Cart\Contracts\Cart;
use App\OfferConsumption\DeservedOffers\FormatterFactory;
use App\OfferConsumption\InteractionGroups\Contract\Strategy;
use App\OfferConsumption\InteractionGroups\ProcessedDeservedOfferList;

class UserCanSelect extends AbstractStrategy implements Strategy
{
    private FormatterFactory $formatterFactory;

    public function __construct(FormatterFactory $formatterFactory)
    {
        $this->formatterFactory = $formatterFactory;
    }

    public function process(InteractionGroup $interactionGroup, ProcessedDeservedOfferList $list, Cart $cart)
    {
        $offers = $this->extractOriginalEntries($interactionGroup, $list)->map(function ($entry) {
            return $entry['offer'];
        });

        $interactionGroup->setAppliedOffers($offers->toArray());

        $list->addEntry(
            $this->formatterFactory->make($interactionGroup)->format($interactionGroup, $cart)
        );
    }
}
