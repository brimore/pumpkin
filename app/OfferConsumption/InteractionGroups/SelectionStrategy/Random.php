<?php

namespace App\OfferConsumption\InteractionGroups\SelectionStrategy;

use App\Domain\InteractionGroup\Model\InteractionGroup;
use App\OfferConsumption\Cart\Contracts\Cart;
use App\OfferConsumption\InteractionGroups\Contract\Strategy;
use App\OfferConsumption\InteractionGroups\ProcessedDeservedOfferList;

class Random extends AbstractStrategy implements Strategy
{
    public function process(InteractionGroup $interactionGroup, ProcessedDeservedOfferList $list, Cart $cart)
    {
        $offers = $this->extractOriginalEntries($interactionGroup, $list)->map(function ($entry) {
            return $entry['offer'];
        });

        if ($offers->isNotEmpty() && $randomOffer = $offers->random()) {
            $list->addEntry($randomOffer);
        }
    }
}
