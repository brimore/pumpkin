<?php

namespace App\OfferConsumption\InteractionGroups\SelectionStrategy;

use App\Domain\InteractionGroup\Model\InteractionGroup;
use App\Domain\Offer\Model\Offer;
use App\OfferConsumption\Cart\Contracts\Cart;
use App\OfferConsumption\InteractionGroups\Contract\Strategy;
use App\OfferConsumption\InteractionGroups\ProcessedDeservedOfferList;
use App\OfferConsumption\Parsers\Traits\ParserConfiguration;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BlockingSubtotalStrategy extends AbstractStrategy implements Strategy
{
    use ParserConfiguration;

    public function process(InteractionGroup $interactionGroup, ProcessedDeservedOfferList $list, Cart $cart)
    {
        $orderedEntries = $this->extractOriginalEntries($interactionGroup, $list)->sortBy(function ($entry) {
            return $entry['order'];
        });

        $currentOffer = Offer::whereUuid($orderedEntries->first()['offer']->getId())->first();
        $accumulativePoints = $this->memberAccumulativePoints(Auth::guard('members')->user(), $currentOffer);
        $cartPoints = $cart->getSubtotal('points') + $accumulativePoints;
        $pointFactor = DB::table('voucher_configuration')->first()?->point_factor ?? 3;
        $cartSubtotal = $cartPoints * $pointFactor;
        foreach ($orderedEntries as $entry) {
            $conditionPoints = $entry['model']->condition_points;

            $currentMultiple = Arr::get($entry['offer']->getPayload(), 'deserved_rewards.rewards.0.multiple');
            $currentVoucherAmount = Arr::get($entry['offer']->getPayload(), 'deserved_rewards.rewards.0.payload.voucherAmount');
            
            $rewardVoucherAmount = $currentVoucherAmount / $currentMultiple;
            $conditionPrice = $conditionPoints * $pointFactor;
            $conditionWithVoucher = $conditionPrice;

            $appliedOnPoints = Arr::get($entry['offer']->getPayload(), 'deserved_rewards.rewards.0.payload.applied_on');
            if($appliedOnPoints === 'price-and-points'){
                $conditionWithVoucher += $rewardVoucherAmount;
            }

            $finalMultiple = 0;
            for ($i = 0; $i < $currentMultiple; $i++) {
                $latestSubtotal = $cartSubtotal;
                if (($cartSubtotal -= $conditionWithVoucher) < 0) {
                    if ($latestSubtotal >= $conditionPrice) $finalMultiple += 1;
                    break;
                }

                $finalMultiple += 1;
            }

            if ($finalMultiple > 0) {
                if ($rewardVoucherAmount * $finalMultiple !== $currentVoucherAmount) {
                    $entry['offer']->setVoucherAmount($finalMultiple * $rewardVoucherAmount);
                }

                $list->addEntry($entry['offer']->setMultiple($finalMultiple));
            }
        }
    }
}
