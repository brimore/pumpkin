<?php

namespace App\OfferConsumption\InteractionGroups\SelectionStrategy;

use App\Domain\InteractionGroup\Model\InteractionGroup;
use App\OfferConsumption\InteractionGroups\ProcessedDeservedOfferList;
use Illuminate\Support\Collection;

abstract class AbstractStrategy
{
    public function extractOriginalEntries(InteractionGroup $interactionGroup, ProcessedDeservedOfferList $list)
    {
        $entries = new Collection();

        foreach ($interactionGroup->offers as $offer) {
            if ($entry = $list->getEntry($offer->uuid)) {
                $list->removeEntry($offer->uuid);

                $entries->push([
                    'order' => $offer->pivot->order,
                    'offer' => $entry,
                    'model' => $offer
                ]);
            }
        }

        return $entries;
    }
}
