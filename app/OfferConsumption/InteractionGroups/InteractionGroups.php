<?php

namespace App\OfferConsumption\InteractionGroups;

use App\Domain\InteractionGroup\Model\InteractionGroup;
use App\OfferConsumption\Cart\Contracts\Cart;
use App\OfferConsumption\DeservedOffers\DeservedOffer;
use App\OfferConsumption\InteractionGroups\Contract\Repository;
use App\OfferConsumption\InteractionGroups\Filters\InteractionGroupFilters;
use App\OfferConsumption\InteractionGroups\SelectionStrategy\Factory;
use Illuminate\Support\Collection;

class InteractionGroups
{
    private Repository $interactionGroupRepo;
    private Factory $strategyFactory;

    public function __construct(Repository $interactionGroupRepo, Factory $strategyFactory)
    {
        $this->interactionGroupRepo = $interactionGroupRepo;
        $this->strategyFactory = $strategyFactory;
    }

    public function processOfferList(Collection $offers, Cart $cart)
    {
        $offerIds = $this->extractOfferIds($offers);

        $interactionGroups = $this->interactionGroupRepo->getInteractionGroups(
            new InteractionGroupFilters(['offer-uuids' => $offerIds])
        );

        $processedList = new ProcessedDeservedOfferList($offers);
        foreach ($interactionGroups as $interactionGroup) {
            $this->processInteractionGroup($interactionGroup, $processedList, $cart);
        }

        return $processedList;
    }

    protected function processInteractionGroup(InteractionGroup $interactionGroup, ProcessedDeservedOfferList $list, Cart $cart)
    {
        $strategy = $this->strategyFactory->make($interactionGroup->selection_strategy);

        $strategy->process($interactionGroup, $list, $cart);
    }

    protected function extractOfferIds(Collection $offers)
    {
        return $offers->map(function (DeservedOffer $offer) {
            return $offer->getId();
        })->toArray();
    }
}
