<?php

namespace App\OfferConsumption\InteractionGroups;

use App\OfferConsumption\DeservedOffers\DeservedOffer;
use Illuminate\Support\Collection;

class ProcessedDeservedOfferList
{
    private Collection $entries;

    public function __construct(Collection $entries)
    {
        $this->setEntries($entries);
    }

    protected function setEntries(Collection $entries)
    {
        $this->entries = $entries;
    }

    public function getEntries()
    {
        return $this->entries;
    }

    public function getEntry($uuid)
    {
        return $this->entries->first(function (DeservedOffer $deservedOffer) use ($uuid) {
            return $deservedOffer->getId() == $uuid;
        });
    }

    public function addEntry(DeservedOffer $offer)
    {
        $this->entries->push($offer);
    }

    public function addEntries($offers)
    {
        $this->entries->merge($offers);
    }

    public function removeEntry($id)
    {
        $this->entries = $this->entries->filter(function (DeservedOffer $offer) use ($id) {
            return $offer->getId() != $id;
        });
    }
}
