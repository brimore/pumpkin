<?php

namespace App\OfferConsumption\InteractionGroups\Contract;

use App\OfferConsumption\InteractionGroups\Filters\InteractionGroupFilters;
use Illuminate\Database\Eloquent\Collection;

interface Repository
{
    public function getInteractionGroups(InteractionGroupFilters $filters): Collection;
}
