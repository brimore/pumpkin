<?php

namespace App\OfferConsumption\InteractionGroups\Contract;

use App\Domain\InteractionGroup\Model\InteractionGroup;
use App\OfferConsumption\Cart\Contracts\Cart;
use App\OfferConsumption\InteractionGroups\ProcessedDeservedOfferList;

interface Strategy
{
    public function process(InteractionGroup $interactionGroup, ProcessedDeservedOfferList $list, Cart $cart);
}
