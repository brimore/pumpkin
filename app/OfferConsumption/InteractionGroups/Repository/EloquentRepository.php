<?php

namespace App\OfferConsumption\InteractionGroups\Repository;

use App\Domain\Common\Filters\HasFilteredBuilder;
use App\Domain\InteractionGroup\Model\InteractionGroup;
use App\OfferConsumption\InteractionGroups\Contract\Repository;
use App\OfferConsumption\InteractionGroups\Filters\InteractionGroupFilters;
use Illuminate\Database\Eloquent\Collection;

class EloquentRepository implements Repository
{
    use HasFilteredBuilder;

    public function getInteractionGroups(InteractionGroupFilters $filters): Collection
    {
        return $this->getFilteredBuilder(InteractionGroup::query(), $filters)->get();
    }
}
