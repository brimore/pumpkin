<?php

namespace App\OfferConsumption\InteractionGroups\Filters;

use App\Domain\Common\Filters\Filters;
use Illuminate\Database\Eloquent\Builder;

class InteractionGroupFilters extends Filters
{
    public function offerUuids(Builder $builder, $uuids)
    {
        return $builder->whereHas('offers', function (Builder $builder) use ($uuids) {
            return $builder->whereIn('offers.uuid', $uuids);
        });
    }
}
