<?php

namespace App\OfferConsumption\SKUComponent;

use App\Domain\Sheet\Model\Sheet;
use App\Domain\SKUList\Model\SkuList as SKUListModel;
use App\Imports\SKUsImport;
use App\OfferConsumption\Parsers\Traits\ParserConfiguration;
use Illuminate\Contracts\Pagination\LengthAwarePaginator as LengthAwarePaginatorAlias;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel as ReaderType;
use Maatwebsite\Excel\Facades\Excel;

class SKUList
{
    use ParserConfiguration;

    /**
     * @var Builder
     */
    private Builder $builder;

    /**
     * @var SKUListModel|NULL
     */
    private SKUListModel|null $sku_list;

    /**
     * @var array
     */
    private array $condition;

    /**
     * @var string
     */
    private string $logical_operator = 'AND';

    /**
     * @param int $sku_list_id
     */
    public function __construct(int $sku_list_id)
    {
        $this->sku_list = SKUListModel::find($sku_list_id);
    }

    /**
     * @param Builder|null $builder
     * @return $this
     */
    public function build(Builder|null $builder = null): self
    {
        $this->builder = $builder ?? DB::table('product_variants as pv')
                ->select(['pv.id', 'pv.code', 'p.category_id', 'p.brand_id'])
                ->join('products as p', 'p.id', '=', 'pv.product_id');

        $criteria = $this->sku_list?->criteria ?? [];
        foreach ($criteria as $condition) {
            switch ($condition['type']) {
                case 'logical-operator':
                    $this->logical_operator = $condition['value'];
                    break;
                case 'condition':
                    $this->logical_operator=isset($condition['operator']) && $condition['operator'] =='not_in' ?"not":"and";
                    if (method_exists($this, $condition['attribute']))
                        call_user_func([$this, $condition['attribute']], $condition);
                    break;
            }
        }
        return $this;
    }

    /**
     * @return Builder
     */
    public function getQuery(): Builder
    {
        return $this->builder;
    }

    /**
     * @return array
     */
    public function getCondition(): array
    {
        return $this->sku_list?->condition ?? [];
    }

    /**
     * @return bool
     */
    public function getSameSKUs(): bool
    {
        return $this->sku_list?->same_skus ?? false;
    }

    /**
     * @return bool
     */
    public function getSameAsPurchase(): bool
    {
        return $this->sku_list?->same_as_purchase ?? false;
    }

    /**
     * @return Collection
     */
    public function list(): Collection
    {
        return $this->builder->get();
    }

    /**
     * @param int $per_page
     *
     * @return LengthAwarePaginatorAlias
     */
    public function paginate(int $per_page = 15): LengthAwarePaginatorAlias
    {
        return $this->builder->paginate($per_page);
    }


    /**
     * @param array $condition
     *
     * @return Builder
     */
    private function list_of_skus(array $condition): Builder
    {
        $file_path = storage_path('app/public/' . Sheet::find($condition['value'])->file);
        $SKUs = Excel::toCollection(
            new SKUsImport, $file_path, null, ReaderType::CSV
        )->first()->except(0)->pluck(0)->toArray();

        return $this->builder->whereIn('code', $SKUs, $this->logical_operator);
    }

    /**
     * @param array $condition
     *
     * @return Builder
     */
    private function brands(array $condition): Builder
    {
        $is_not_in = $condition['operator'] !== 'in';

        return $this->builder
            ->whereIn('brand_id', $condition['value'], $this->logical_operator, $is_not_in);
    }

    /**
     * @param array $condition
     *
     * @return Builder
     */
    private function categories(array $condition): Builder
    {
        $query_method = in_array($this->logical_operator, ['and', 'AND']) ? 'where' : 'orWhere';
        $is_not_in = $condition['operator'] !== 'in';

        return $this->builder
            ->join('categories as cat', 'cat.id', '=', 'p.category_id')
            ->$query_method(function ($q) use ($condition, $is_not_in) {
                $q->whereIn('cat.id', $condition['value'], 'and', $is_not_in)
                    ->whereIn('cat.parent_id', $condition['value'], (!$is_not_in ? 'or' : 'and'), $is_not_in);
            });
    }
}
