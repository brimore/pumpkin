<?php

namespace App\OfferConsumption\Parsers\Traits;

use App\Domain\Member\Model\Member;
use App\Domain\Offer\Model\Offer;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

trait ParserConfiguration
{
    /**
     * @param array $criteria
     * @return array
     */
    public static function MapCriteria(array $criteria): array
    {
        $mapped_criteria = [];
        collect($criteria)->each(function ($value) use (&$mapped_criteria) {
            if ($value['type'] === 'filter-group') {
                collect($value['config'] ?? $value['criteria'])->each(function ($filter) use (&$mapped_criteria) {
                    $mapped_criteria[] = $filter;
                });
            } else {
                $mapped_criteria[] = $value;
            }
        });

        return $mapped_criteria;
    }

    /**
     * @param string $type
     * @param string $value
     * @param null $default
     *
     * @return string
     */
    public static function GetOperator(string $type, string $value, $default = NULL): string
    {
        $operators = [
            'comparison' => [
                "less_than" => '<',
                "greater_than" => '>',
                "equal" => '=',
                "between" => 'between',
                "greater_than_or_equal" => '>=',
                "less_than_or_equal" => '<=',
                "not_equal" => '!='
            ],
            'logical' => [
                'AND' => 'where',
                'OR' => 'orWhere'
            ]
        ];

        return $operators[$type][$value] ?? $default;
    }

    /**
     * @param Offer $offer
     * @param Authenticatable $member
     * @param null $from
     * @param null $to
     *
     * @return float
     */
    public function offerConsumptionCount(Offer $offer, Authenticatable $member, $from = null, $to = null): float
    {
        $member_points = DB::table('member_offer_consumption')
            ->where('offer_id', $offer->uuid)
            ->where('member_id', $member->id);

        if ($from)
            $member_points->where('created_at', '>=', Carbon::parse($from)->format('Y-m-d H:i:s'));
        if ($to)
            $member_points->where('created_at', '<=', Carbon::parse($to)->format('Y-m-d H:i:s'));

        return $member_points->sum('quantity');
    }


    /**
     * @param Offer $offer
     * @param Member $member
     *
     * @return float
     */
    public function offerConsumptionPoints(Offer $offer, Member $member): float
    {
        if ($offer->accumulationGroup) {
            $offers = Offer::query()
                ->where('accumulation_group_id', $offer->accumulationGroup->id)
                ->whereDate('starts_at', '=', $offer->starts_at)
                ->whereDate('ends_at', '=', $offer->ends_at)
                ->get();
        } else {
            $offers = new Collection([$offer]);
        }

        $consumed_points = 0;
        foreach ($offers as $item) {
            $consumed_points += $this->offerConsumptionCount($item, $member) * $item->condition_points;
        }

        return $consumed_points;
    }

    /**
     * @param Member $member
     * @param Offer $offer
     * @param int $condition_points
     *
     * @return int
     */
    public function memberAccumulativePoints(Authenticatable $member, Offer $offer): int
    {
        $consumed_points = $this->offerConsumptionPoints($offer, $member);

        $total_points = DB::table('member_points')
            ->where('member_id', Auth::guard('members')->id())
            ->whereDate('order_creation_date', '>=', $offer->starts_at)
            ->whereDate('order_creation_date', '<=', $offer->ends_at)
            ->sum('total_points');

        return $total_points - $consumed_points;
    }
}
