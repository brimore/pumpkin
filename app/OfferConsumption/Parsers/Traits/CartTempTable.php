<?php

namespace App\OfferConsumption\Parsers\Traits;

use App\OfferConsumption\Cart\Variant;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

trait CartTempTable
{
    /**
     * Fill cart variants table with member's cart variants.
     */
    private function insertVariantsToTempCart($table = 'cart_variants')
    {
        DB::table($table)->insert(
            $this->getMappedVariantsFromDB(
                $this->mapCartVariants()
            )->toArray()
        );
    }

    /**
     * @return Collection
     */
    private function mapCartVariants(): Collection
    {
        return Collection::make($this->cart->getVariants())->map(
            fn(Variant $variant) => [
                'id' => $variant->getId(),
                'price' => $variant->getPrice(),
                'quantity' => $variant->getQuantity(),
                'points' => $variant->getPoint()
            ]
        );
    }

    /**
     * @return Collection
     */
    private function getMappedVariantsFromDB(Collection $variants): Collection
    {
        $subtotal = $this->cart->getSubtotal();
        $subtotalInPoints = $this->cart->getSubtotal('points');
        return DB::table('product_variants as pv')
            ->select(['pv.id', 'pv.code', 'p.category_id', 'p.brand_id'])
            ->join('products as p', 'p.id', '=', 'pv.product_id')
            ->whereIn('pv.id', $variants->pluck('id')->toArray())
            ->get()->map(function ($variant) use ($variants, $subtotal, $subtotalInPoints) {
                $cart_variant = $variants->where('id', $variant->id)->first();
                $quantity = $variants->where('id', $variant->id)->groupBy('id')->sum(function ($row) {
                    return $row->sum('quantity');
                });
                return [
                    'id' => $variant->id,
                    'SKU' => $variant->code,
                    'category_id' => $variant->category_id,
                    'brand_id' => $variant->brand_id,
                    'quantity' => $quantity,
                    'price' => $cart_variant['price'],
                    'points' => $cart_variant['points'],
                    'total_price' => $cart_variant['price'] * $quantity,
                    'total_points' => $cart_variant['points'] * $quantity,
                    'subtotal_price' => $subtotal,
                    'subtotal_points' => $subtotalInPoints,
                ];
            });
    }

    /**
     * Create temp cart table to query it.
     */
    private function createTempCartTable($table = 'cart_variants')
    {
        $connection = config('database.default');
        $collation = config("database.connections.{$connection}.collation");
        DB::unprepared("CREATE TEMPORARY TABLE {$table} (
                            `id` BIGINT UNSIGNED,
                            `SKU` VARCHAR(255),
                            `category_id` BIGINT UNSIGNED,
                            `brand_id` BIGINT UNSIGNED,
                            `quantity` INT(11),
                            `price` DOUBLE (20,2),
                            `points` DOUBLE (20,2),
                            `total_price` DOUBLE (20,2),
                            `total_points` DOUBLE (20,2),
                            `subtotal_price` DOUBLE (20,2),
                            `subtotal_points` DOUBLE (20,2)
                ) DEFAULT COLLATE {$collation}");
    }

    /**
     * Create temp cart table for sub-query to avoid re-open same temp table issue.
     *
     * @return string
     */
    private function createRandomTempCartTable(): string
    {
        $temp_table_name = "H" . Str::random('10');
        // At first, we have to create a temporary cart's table to query all conditions easily.
        $this->createTempCartTable($temp_table_name);
        // Then, we have to manipulate our cart variants to append important data to each variant
        // then insert all of them to our temporary cart's table.
        $this->insertVariantsToTempCart($temp_table_name);

        return $temp_table_name;
    }

    /**
     * Drop temp cart table.
     */
    private function dropTempCartTable($table = 'cart_variants')
    {
        DB::unprepared("DROP TEMPORARY TABLE {$table}");
    }
}
