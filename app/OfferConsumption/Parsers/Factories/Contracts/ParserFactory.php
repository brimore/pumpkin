<?php

namespace App\OfferConsumption\Parsers\Factories\Contracts;

use App\OfferConsumption\Parsers\Filters\Contracts\FiltersBuilder;

interface ParserFactory
{
    /**
     * @param array $filter
     * @return FiltersBuilder
     */
    public function setFilter(array $filter): FiltersBuilder;

    /**
     * @param string $operator
     */
    public function setLogicalOperator(string $operator): void;

    /**
     * @return mixed
     */
    public function get(): mixed;
}
