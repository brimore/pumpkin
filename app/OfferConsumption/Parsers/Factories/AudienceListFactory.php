<?php

namespace App\OfferConsumption\Parsers\Factories;

use App\Domain\Member\Model\Member;
use App\OfferConsumption\Parsers\Factories\Contracts\ParserFactory;
use App\OfferConsumption\Parsers\Filters\AudienceListFilters;
use App\OfferConsumption\Parsers\Filters\Contracts\FiltersBuilder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class AudienceListFactory implements ParserFactory
{
    /**
     * @var Builder
     */
    private Builder $builder;

    /**
     * @var string $logical_operator
     */
    private string $logical_operator = 'AND';

    /**
     * @param string $operator
     */
    public function setLogicalOperator(string $operator): void
    {
        $this->logical_operator = $operator;
    }

    /**
     * @param array $filter
     *
     * @return FiltersBuilder
     */
    public function setFilter(array $filter): FiltersBuilder
    {
        $builder_instantiated = isset($this->builder);

        $this->builder = $builder_instantiated
            ? $this->builder
            : Member::select('members.*')->join('events', 'events.member_id', 'members.id');

        if (Auth::guard('members')->check() && !$builder_instantiated)
            $this->builder->where('members.id', Auth::guard('members')->id());

        return new AudienceListFilters($filter, $this->builder, $this->logical_operator);
    }

    /**
     * @return Builder
     */
    public function get(): Builder
    {
        return $this->builder->groupBy('member_id');
    }
}
