<?php

namespace App\OfferConsumption\Parsers\Factories;

use App\Domain\Member\Model\Member;
use App\Domain\Offer\Model\Offer;
use App\OfferConsumption\Cart\Contracts\Cart;
use App\OfferConsumption\Multiples\MultiplesFilters;
use App\OfferConsumption\Parsers\Factories\Contracts\ParserFactory;
use App\OfferConsumption\Parsers\Filters\ConditionFilters;
use App\OfferConsumption\Parsers\Filters\Contracts\FiltersBuilder;
use App\OfferConsumption\Parsers\Traits\CartTempTable;
use App\OfferConsumption\Parsers\Traits\ParserConfiguration;
use App\OfferConsumption\SKUComponent\SKUList;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ConditionFactory implements ParserFactory
{
    use ParserConfiguration, CartTempTable;

    /**
     * @var Cart
     */
    private Cart $cart;

    /**
     * @var Offer
     */
    private Offer $offer;

    /**
     * @var Builder
     */
    private Builder $builder;

    /**
     * @var string $logical_operator
     */
    private string $logical_operator = 'AND';

    /**
     * @var array $multiplies
     */
    public static array $MULTIPLIES = [];

    /**
     * @param Cart $cart
     */
    public function __construct(Cart $cart, Offer $offer)
    {
        $this->cart = $cart;
        $this->offer = $offer;
    }

    /**
     * @param string $operator
     */
    public function setLogicalOperator(string $operator): void
    {
        $this->logical_operator = $operator;
    }

    /**
     * @param array $filter
     *
     * @return FiltersBuilder
     */
    public function setFilter(array $filter): FiltersBuilder
    {
        if (!isset($this->builder)) {
            // At first, we have to create a temporary cart's table to query all conditions easily.
            $this->createTempCartTable();
            // Then, we have to manipulate our cart variants to append important data to each variant
            // then insert all of them to our temporary cart's table.
            $this->insertVariantsToTempCart();
            // Finally, we have to specify multiplies for current offer.
            self::$MULTIPLIES[$this->offer->id] = [];
        }

        $this->builder = $this->builder ?? DB::table('cart_variants');

        return new ConditionFilters($this->cart, $this->offer, $filter, $this->builder, $this->logical_operator);
    }

    /**
     * @return float
     */
    public function get(): float
    {
        $cart_items = $this->builder->get();

        $this->dropTempCartTable();

        if (!$cart_items->count())
            return 0;

        $this->calculateMultipliesForEachCondition($cart_items);

        $currentOfferMultiplies = self::$MULTIPLIES[$this->offer->id]['multiplies'];
        self::$MULTIPLIES[$this->offer->id]['multiplies'] = $this->offer->multiples
            ? min((new MultiplesFilters($this->offer, $currentOfferMultiplies))->apply(), $currentOfferMultiplies)
            : $currentOfferMultiplies;

        return self::$MULTIPLIES[$this->offer->id]['multiplies'];
    }

    /**
     * @param $conditions
     * @return mixed
     */
    private function getActualMultiplies($conditions): mixed
    {
        if ($operator_index = array_keys($conditions, 'AND')) {
            $conditions = $this->applyLogicalOperatorAndRecalculateConditions($operator_index[0], $conditions);
        } elseif ($operator_index = array_keys($conditions, 'OR')) {
            $conditions = $this->applyLogicalOperatorAndRecalculateConditions($operator_index[0], $conditions);
        }

        if (count($conditions) >= 3)
            $conditions = $this->getActualMultiplies(array_values($conditions));

        return $conditions[0] ?? $conditions ?? [];
    }

    /**
     * @param $operator_index
     * @param $conditions
     *
     * @return array
     */
    private function applyLogicalOperatorAndRecalculateConditions($operator_index, $conditions): array
    {
        $conditions[$operator_index] = $this->performOperatorLogic(
            [$conditions[$operator_index - 1], $conditions[$operator_index], $conditions[$operator_index + 1]]
        );

        Arr::forget($conditions, [$operator_index - 1, $operator_index + 1]);
        return array_values($conditions);
    }

    /**
     * @param $conditions
     *
     * @return mixed
     */
    private function performOperatorLogic($conditions): mixed
    {
        if (in_array('AND', $conditions)) {
            return Collection::make($conditions)
                ->filter(fn($item) => !in_array($item, ['OR', 'AND']))
                ->sortBy(fn($item) => $item['payload']['multiplies'] ?? $item['multiplies'])->first();
        }

        return Collection::make($conditions)
            ->filter(fn($item) => !in_array($item, ['OR', 'AND']))
            ->sortByDesc(fn($item) => $item['payload']['multiplies'] ?? $item['multiplies'])->first();
    }

    /**
     *
     * /**
     * @param Collection $filtered_variants
     */
    private function calculateMultipliesForEachCondition(Collection $filtered_variants): void
    {
        $offer_multiplies = self::$MULTIPLIES[$this->offer->id];
        foreach ($offer_multiplies as $filter_index => $filter) {
            switch ($filter['type']) {
                // Flatten all filter-group conditions to get the final layer with final actual multiplies.
                case 'filter-group':
                    // Flatten filter-group to get the final layer.
                    foreach ($filter['payload'] as $group_index => $filter_payload) {
                        $offer_multiplies[$filter_index]['payload'][$group_index] =
                            $this->calculateFilterGroupMultiplies($filter_payload, $filtered_variants);
                    }
                    // Get actual filter-group multiplies from the final node in the filter-group.
                    $offer_multiplies[$filter_index] =
                        $this->getActualMultiplies($offer_multiplies[$filter_index]['payload']);
                    break;
                case 'filter':
                    // Get filter-conditions multiplies.
                    $offer_multiplies[$filter_index] =
                        $this->calculateFilterMultiplies($filter['payload'], $filtered_variants);
                    break;
                case 'operator':
                    // Get only keyword (OR || AND).
                    $offer_multiplies[$filter_index] = $filter['payload'];
                    break;
            }
        }

        // Get the final multiplies to result from the final flattened layer from all conditions.
        self::$MULTIPLIES[$this->offer->id] = $this->getActualMultiplies($offer_multiplies)['payload'] ?? [];
    }

    /**
     * @param mixed $filter_payload
     * @param Collection $filtered_variants
     *
     * @return mixed
     */
    private function calculateFilterGroupMultiplies(mixed $filter_payload, Collection $filtered_variants): mixed
    {
        return match ($filter_payload['type']) {
            'conditions' => [
                'type' => 'conditions',
                'payload' => $this->calculateMultipliesFromConditions(
                    $filter_payload['payload'], $filtered_variants
                )
            ],
            default => $filter_payload['payload']
        };
    }

    /**
     * @param $payload
     * @param Collection $filtered_variants
     *
     * @return array
     */
    private function calculateFilterMultiplies($payload, Collection $filtered_variants): array
    {
        return [
            'type' => 'filter',
            'payload' => $this->calculateMultipliesFromConditions(
                $payload, $filtered_variants
            )
        ];
    }

    /**
     * @param mixed $conditions
     * @param Collection $filtered_variants
     *
     * @return mixed
     */
    private function calculateMultipliesFromConditions(mixed $conditions, Collection $filtered_variants): mixed
    {
        if (isset($conditions['type']))
            return $this->calculateMultiplies($conditions, $filtered_variants);

        foreach ($conditions as $condition_index => $condition)
            $conditions[$condition_index] = is_array($condition)
                ? $this->calculateMultipliesFromConditions($condition, $filtered_variants)
                : $condition;

        return $this->getActualMultiplies($conditions);
    }

    /**
     * @param mixed $conditions
     * @param Collection $filtered_variants
     *
     * @return array
     */
    private function calculateMultiplies(mixed $conditions, Collection $filtered_variants): array
    {
        return match ($conditions['type']) {
            'quantity' => [
                'type' => 'quantity',
                'value' => $conditions['value'],
                'multiplies' => $this->calculateMultipliesPerQuantity($conditions, $filtered_variants)
            ],
            'points' => [
                'type' => 'points',
                'value' => $conditions['value'],
                'multiplies' => $this->calculateMultipliesPerPoints($conditions, $filtered_variants)
            ],
            'percentage' => [
                'type' => 'points',
                'value' => $conditions['value'],
                'multiplies' => $this->calculateMultipliesPerPercentage($conditions, $filtered_variants)
            ]
        };
    }

    /**
     * @param mixed $conditions
     * @param Collection $filtered_variants
     * @return float|mixed
     */
    private function calculateMultipliesPerPoints(mixed $conditions, Collection $filtered_variants): mixed
    {
        $condition_values = Collection::make($conditions['value']);
        // Get filtered variants if SKU list exists.
        if (isset($conditions['sku_list_id'])) {
            $skus_list = new SKUList($conditions['sku_list_id']);
            $filtered_variants =
                $filtered_variants->whereIn('SKU', $skus_list->build()->getQuery()->get()->pluck('code'));
        }

        // Get points and get its type (accumulative or per-order).
        $condition_points = $condition_values->where('type', 'points')->first()['value'] ?? $condition_values[0] ?? 0;
        $is_accumulative = ($condition_values->where('type', 'accumulation')->first()['value'] ?? null) === 'accumulative';

        // Get accumulative-points if exists.
        $accumulative_points = $is_accumulative
            ? $this->accumulativePoints(Auth::guard('members')->user(), $this->offer) : 0;

        $cart_subtotal = $this->cart->getSubtotal('points');
        return isset($skus_list) && $skus_list->getSameSKUs()
            ? $filtered_variants
                ->map(fn($variant) => ($condition_points ? floor((min($variant->total_points, $cart_subtotal) + $accumulative_points) / $condition_points) : 0))
                ->sum()
            : ($condition_points ? floor((min($filtered_variants->sum('total_points'), $cart_subtotal) + $accumulative_points) / $condition_points) : 0);
    }

    /**
     * @param mixed $conditions
     * @param Collection $filtered_variants
     * @return float|mixed
     */
    private function calculateMultipliesPerQuantity(mixed $conditions, Collection $filtered_variants): mixed
    {
        $condition = $conditions['value'];
        // Get filtered variants.
        $skus_list = new SKUList($conditions['sku_list_id']);
        $currentCartVariants = Collection::make(request('data')['attributes']['cart']);
        $filtered_variants = $filtered_variants
            ->whereIn('SKU', $skus_list->build()->getQuery()->get()->pluck('code'))
            ->map(function ($variant) use ($currentCartVariants) {
                $cartPrice = $currentCartVariants->where('variantId', $variant->id)
                    ->sum(fn($item) => $item['price'] * $item['quantity']);

                if (!$variant->total_price) {
                    $variant->quantity = 0;
                } elseif ($cartPrice != $variant->total_price) {
                    $quantity = $variant->quantity - ceil($cartPrice / $variant->total_price);
                    $variant->quantity = $quantity < 0 ? 0 : $quantity;
                }

                return $variant;
            });

        return $skus_list->getSameSKUs()
            ? $filtered_variants
                ->map(fn($variant) => ($condition ? floor($variant->quantity / $condition) : 0))
                ->sum()
            : ($condition ? floor($filtered_variants->sum('quantity') / $condition) : 0);
    }

    /**
     * @param mixed $conditions
     * @param Collection $filtered_variants
     * @return bool|mixed
     */
    private function calculateMultipliesPerPercentage(mixed $conditions, Collection $filtered_variants): mixed
    {
        $condition = $conditions['value'];
        $subtotal = round($this->cart->getSubtotal('points', true));
        // Get filtered variants.
        $skus_list = new SKUList($conditions['sku_list_id']);
        $filtered_variants =
            $filtered_variants->whereIn('SKU', $skus_list->build()->getQuery()->get()->pluck('code'));

        return $skus_list->getSameSKUs()
            ? $filtered_variants
                ->map(fn($variant) => (int)round($variant->total_points / $subtotal * 100) >= $condition)
                ->sum()
            : (round($filtered_variants->sum('total_points')) / $subtotal * 100) >= $condition;
    }

    /**
     * @param Member $member
     * @param Offer $offer
     * @param int $condition_points
     *
     * @return int
     */
    public function accumulativePoints(Authenticatable $member, Offer $offer): int
    {
        $consumed_points = $this->offerConsumptionPoints($offer, $member);

        $total_points = DB::table('member_points')
            ->where('member_id', Auth::guard('members')->id())
            ->whereDate('order_creation_date', '>=', $offer->starts_at)
            ->whereDate('order_creation_date', '<=', $offer->ends_at)
            ->sum('total_points');

        return $total_points - $consumed_points;
    }
}
