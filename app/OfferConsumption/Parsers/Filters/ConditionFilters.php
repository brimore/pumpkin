<?php

namespace App\OfferConsumption\Parsers\Filters;

use App\Domain\Offer\Model\Offer;
use App\OfferConsumption\Cart\Contracts\Cart;
use App\OfferConsumption\Parsers\Traits\CartTempTable;
use App\OfferConsumption\SKUComponent\SKUList;
use App\OfferConsumption\Parsers\Factories\ConditionFactory;
use App\OfferConsumption\Parsers\Filters\Contracts\FiltersBuilder;
use App\OfferConsumption\Parsers\MemberActions\ConditionActions\AchievePointsAction;
use App\OfferConsumption\Parsers\MemberActions\ConditionActions\BuySkusAction;
use App\OfferConsumption\Parsers\Traits\ParserConfiguration;
use Exception;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;

class ConditionFilters implements FiltersBuilder
{
    use ParserConfiguration, CartTempTable;

    /**
     * @var Builder
     */
    private Builder $builder;

    /**
     * @var array
     */
    private array $action;

    /**
     * @var string $logical_operator
     */
    private string $logical_operator;

    /**
     * @var Offer $offer
     */
    private Offer $offer;


    /**
     * @var Cart $cart
     */
    private Cart $cart;

    /**
     * @const
     */
    private const ACTIONS = [
        'ACHIEVE_POINTS' => AchievePointsAction::class,
        'BUY_SKU' => BuySkusAction::class
    ];

    /**
     * @param Cart $cart
     * @param Offer $offer
     * @param array $action
     * @param Builder $builder
     * @param string $logical_operator
     */
    public function __construct(Cart $cart, Offer $offer, array $action, Builder $builder, string $logical_operator)
    {
        $this->cart = $cart;
        $this->offer = $offer;
        $this->action = $action;
        $this->builder = $builder;
        $this->logical_operator = $logical_operator;
    }

    /**
     * @return Builder
     */
    public function parse(): Builder
    {
        match ($this->action['type']) {
            'filter-group' => $this->filter_group($this->action['criteria']),
            default => $this->filter($this->action)
        };
        $this->getMultiplyConditions($this->action['criteria'] ?? $this->action);

        return $this->builder;
    }

    /**
     * @param array $criteria
     *
     * @return Builder
     */
    public function filter_group(array $criteria): Builder
    {
        $query_method = self::GetOperator('logical', $this->logical_operator, 'where');

        $this->builder->whereExists(function ($query) use ($criteria, $query_method) {
            $query->select('*')
                ->from($this->createRandomTempCartTable())
                ->$query_method(function ($filter_group_query) use ($criteria) {
                    // Filter Group Queries.
                    $filter_group_query->where(function ($filters_query) use ($criteria) {
                        $this->buildFiltersQuery($criteria, $filters_query);
                    });
                });
        }, $this->logical_operator);

        return $this->builder;
    }

    /**
     * @param array $criteria
     *
     * @return Builder
     */
    public function filter(array $criteria): Builder
    {
        $query_method = self::GetOperator('logical', $this->logical_operator, 'where');

        $this->builder->whereExists(function (Builder $query) use ($criteria, $query_method) {
            $query->select('*')
                ->from($this->createRandomTempCartTable())
                ->$query_method(function ($filter_query) use ($criteria) {
                    // Filter Queries
                    return App::make(
                        self::ACTIONS[Str::upper($criteria['condition'])],
                        ['action' => $criteria, 'builder' => $filter_query, 'offer' => $this->offer]
                    )->build();
                });
        }, $this->logical_operator);

        return $this->builder;
    }

    /**
     * @param array $filters
     * @param $filters_query
     */
    private function buildFiltersQuery(array $filters, $filters_query): void
    {
        $query_method = 'where';
        foreach ($filters as $filter) {
            match ($filter['type']) {
                'logical-operator' => $query_method = self::GetOperator('logical', $filter['value'], 'where'),
                'filter' => $filters_query->{$query_method}(function ($condition_query) use ($filter) {
                    return App::make(
                        self::ACTIONS[Str::upper($filter['condition'])],
                        ['action' => $filter, 'builder' => $condition_query, 'offer' => $this->offer]
                    )->build();
                })
            };
        }
    }

    /**
     * @param array $filters
     *
     * @return void
     */
    private function getMultiplyConditions(array $filters): void
    {
        // Set logical operator between [filters or filter-groups].
        if (!empty(ConditionFactory::$MULTIPLIES[$this->offer->id]))
            ConditionFactory::$MULTIPLIES[$this->offer->id][] = [
                'type' => 'operator', 'payload' => $this->logical_operator
            ];

        if (isset($filters['conditions'])) {
            // Set multiply conditions for a single filter.
            ConditionFactory::$MULTIPLIES[$this->offer->id][] = [
                'type' => 'filter',
                'payload' => $this->setMultiplyCalculationConditions($filters['conditions'])
            ];
        } else {
            // Set multiply conditions for a filter-group.
            foreach ($filters as $filter) {
                $multiplies[] = match ($filter['type']) {
                    'filter' => [
                        'type' => 'conditions',
                        'payload' => $this->setMultiplyCalculationConditions($filter['conditions'])
                    ],
                    default => ['type' => 'operator', 'payload' => $filter['value']]
                };
            }
            ConditionFactory::$MULTIPLIES[$this->offer->id][] = [
                'type' => 'filter-group', 'payload' => $multiplies
            ];
        }
    }

    /**
     * Set mapped multiply conditions to get handled later.
     * Also, mapping conditions to be like: [ [100,'OR',200] , 'AND', [100]]
     *
     * @param $conditions
     * @return array
     */
    private function setMultiplyCalculationConditions($conditions): array
    {
        $multiplies = [];
        foreach ($conditions as $index => $condition) {
            if ($condition['type'] === 'logical-operator') {
                // Get operator's left and right values.
                $prev_condition_value = isset($conditions[$index - 1])
                    ? $this->getMultiplyConditionValue($conditions[$index - 1]) : null;
                $next_condition_value = $this->getMultiplyConditionValue($conditions[$index + 1]);

                // Divide conditions to smallest logical-operator set that can be handled to be like [100,'OR',200].
                if ($prev_condition_value) {
                    $multiplies[] = [$prev_condition_value, $condition['value'], $next_condition_value];
                } else {
                    $multiplies[] = $condition['value'];
                    $multiplies[] = [$next_condition_value];
                }

                // Then, remove divided elements from whole conditions to do the same cycle on the remaining conditions.
                Arr::forget($conditions, [$index - 1, $index, $index + 1]);
            }
        }

        // Handle single condition case.
        if (empty($multiplies))
            $multiplies = $this->getMultiplyConditionValue($condition);

        return $multiplies;
    }

    /**
     * Get condition value based on condition-attribute type.
     *
     * @param $condition
     * @return array
     * @throws Exception
     */
    private function getMultiplyConditionValue($condition): array
    {
        switch ($condition['attribute']) {
            case 'points':
                return ['type' => 'points', 'value' => $condition['value']];
            case 'sku_list':
                $list = new SkuList($list_id = $condition['value']);
                $list_condition = !empty($list->getCondition())
                    ? $list->getCondition()
                    : throw new Exception('Invalid SKU-LIST Identifier');
                return ['type' => $list_condition['type'], 'value' => $list_condition['value'], 'sku_list_id' => $list_id];
            default:
                throw new Exception('Invalid Condition-Attribute Value');
        }
    }
}
