<?php

namespace App\OfferConsumption\Parsers\Filters;

use App\OfferConsumption\Parsers\Filters\Contracts\FiltersBuilder;
use App\OfferConsumption\Parsers\MemberActions\AudienceListActions\PlacedOrderAction;
use App\OfferConsumption\Parsers\MemberActions\AudienceListActions\SignedUpAction;
use App\OfferConsumption\Parsers\Traits\ParserConfiguration;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AudienceListFilters implements FiltersBuilder
{
    use ParserConfiguration;

    /**
     * @var Builder
     */
    private Builder $builder;

    /**
     * @var array
     */
    private array $action;

    /**
     * @var string $logical_operator
     */
    private string $logical_operator;

    /**
     * @const
     */
    private const ACTIONS = [
        'SIGNED_UP' => SignedUpAction::class,
        'PLACED_ORDER' => PlacedOrderAction::class
    ];

    /**
     * @param array $action
     * @param Builder $builder
     * @param string $logical_operator
     */
    public function __construct(array $action, Builder $builder, string $logical_operator)
    {
        $this->action = $action;
        $this->builder = $builder;
        $this->logical_operator = $logical_operator;
    }

    /**
     * @return Builder
     */
    public function parse(): Builder
    {
        switch ($this->action['type']) {
            case 'filter-group':
                $this->filter_group($this->action['config']);
                break;
            default:
                $this->filter($this->action);
                break;
        }

        return $this->builder;
    }

    /**
     * @param array $criteria
     *
     * @return Builder
     */
    public function filter_group(array $criteria): Builder
    {
        return $this->builder->whereExists(function ($query) use ($criteria) {
            $query->select('member_id')
                ->from('events')
                ->whereRaw(DB::raw('member_id = members.id'))
                ->where(function ($filter_group_query) use ($criteria) {
                    // Filter Group Queries.
                    $filter_group_query->where(function ($filters_query) use ($criteria) {
                        $this->buildFilterGroupQuery($criteria, $filters_query);
                    });
                });

            if (Auth::guard('members')->check()) $query->where('member_id', Auth::guard('members')->id());
        }, $this->logical_operator);
    }

    /**
     * @param array $criteria
     *
     * @return Builder
     */
    public function filter(array $criteria): Builder
    {
        return $this->builder->whereExists(function ($query) use ($criteria) {
            $query->select('member_id')
                ->from('events')
                ->whereRaw(DB::raw('member_id = members.id'))
                ->where(function ($filter_query) use ($criteria) {
                    // Filter Queries
                    return $this->buildFilterQuery($criteria, $filter_query);
                });

            if (Auth::guard('members')->check()) $query->where('member_id', Auth::guard('members')->id());
        }, $this->logical_operator);
    }

    /**
     * @param array $filters
     * @param $filters_query
     */
    private function buildFilterGroupQuery(array $filters, $filters_query): void
    {
        $query_method = 'where';
        foreach ($filters as $filter) {
            switch ($filter['type']) {
                case 'logical-operator':
                    $query_method = self::GetOperator('logical', $filter['value'], 'where');
                    break;
                case 'filter':
                    // Filter Queries
                    $filters_query->{$query_method}(function ($condition_query) use ($filter) {
                        return $this->buildFilterQuery($filter, $condition_query);
                    });
                    break;
            }
        }
    }

    /**
     * @param array $criteria
     * @param $filter_query
     *
     * @return mixed
     */
    private function buildFilterQuery(array $criteria, $filter_query): mixed
    {
        return App::make(
            self::ACTIONS[Str::upper($criteria['event'])],
            ['action' => $criteria, 'builder' => $filter_query]
        )->build();

    }
}
