<?php

namespace App\OfferConsumption\Parsers\Filters\Contracts;

interface FiltersBuilder
{
    /**
     * @return mixed
     */
    public function parse(): mixed;

    /**
     * @param array $criteria
     *
     * @return mixed
     */
    public function filter_group(array $criteria): mixed;

    /**
     * @param array $criteria
     *
     * @return mixed
     */
    public function filter(array $criteria): mixed;
}
