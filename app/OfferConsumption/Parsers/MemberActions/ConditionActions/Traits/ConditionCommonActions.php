<?php

namespace App\OfferConsumption\Parsers\MemberActions\ConditionActions\Traits;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\Str;

trait ConditionCommonActions
{
    /**
     * @return Builder
     */
    public function build(): Builder
    {
        foreach ($this->action as $filter => $value)
            !(method_exists($this, $filter) && $value) ?: call_user_func([$this, $filter], $value);

        return $this->builder;
    }

    /**
     * @param $conditions
     * @return Builder
     */
    public function conditions($conditions): Builder
    {
        $this->builder->where(function ($query) use ($conditions) {
            $logical_operator = 'and';
            foreach ($conditions as $condition) {
                switch ($condition['type']) {
                    case 'logical-operator':
                        $logical_operator = Str::lower($condition['value']);
                        break;
                    case 'condition':
                        if (method_exists($this, $condition['attribute']))
                            call_user_func([$this, $condition['attribute']], $query, $logical_operator, $condition);
                        break;
                }
            }
        });

        return $this->builder;
    }
}
