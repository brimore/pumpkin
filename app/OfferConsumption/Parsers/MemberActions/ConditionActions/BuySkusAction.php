<?php

namespace App\OfferConsumption\Parsers\MemberActions\ConditionActions;

use App\Domain\Offer\Model\Offer;
use App\OfferConsumption\Parsers\MemberActions\ConditionActions\Traits\ConditionCommonActions;
use App\OfferConsumption\Parsers\MemberActions\Contracts\MemberActionsBuilder;
use App\OfferConsumption\Parsers\Traits\ParserConfiguration;
use App\OfferConsumption\SKUComponent\SKUList;
use Illuminate\Database\Query\Builder;

class BuySkusAction implements MemberActionsBuilder
{
    use ParserConfiguration, ConditionCommonActions;

    /**
     * @var Builder
     */
    private Builder $builder;

    /**
     * @var array
     */
    private array $action;

    /**
     * @param array $action
     * @param Builder $builder
     * @param Offer $offer
     */
    public function __construct(array $action, Builder $builder, Offer $offer)
    {
        $this->action = $action;
        $this->builder = $builder;
        $this->offer = $offer;
    }

    /**
     * @param Builder $query
     * @param string $logical_operator
     * @param array $condition
     *
     * @return Builder
     */
    public function sku_list(Builder $query, string $logical_operator, array $condition): Builder
    {
        $list = new SKUList($condition['value']);

        return $query->whereIn(
            'SKU',
            fn($sub_query) => $list->build(
                $sub_query
                    ->select('code')
                    ->from('product_variants')
                    ->join('products as p', 'p.id', '=', 'product_variants.product_id')
            )->getQuery(),
            $logical_operator
        );
    }
}
