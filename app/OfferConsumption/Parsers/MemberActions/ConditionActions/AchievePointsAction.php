<?php

namespace App\OfferConsumption\Parsers\MemberActions\ConditionActions;

use App\Domain\Offer\Model\Offer;
use App\OfferConsumption\Parsers\MemberActions\ConditionActions\Traits\ConditionCommonActions;
use App\OfferConsumption\Parsers\MemberActions\Contracts\MemberActionsBuilder;
use App\OfferConsumption\Parsers\Traits\ParserConfiguration;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AchievePointsAction implements MemberActionsBuilder
{
    use ParserConfiguration, ConditionCommonActions;

    /**
     * @var Builder
     */
    private Builder $builder;

    /**
     * @var array
     */
    private array $action;

    /**
     * @param array $action
     * @param Builder $builder $builder
     * @param Offer $offer
     */
    public function __construct(array $action, Builder $builder, Offer $offer)
    {
        $this->action = $action;
        $this->builder = $builder;
        $this->offer = $offer;
    }

    /**
     * @param Builder $query
     * @param string $logical_operator
     * @param array $condition
     *
     * @return Builder
     */
    public function brands(Builder $query, string $logical_operator, array $condition): Builder
    {
        $operator = self::GetOperator('comparison', $condition['operator'], '=');

        return is_array($condition['value'])
            ? $query->whereIn("brand_id", $condition['value'], $logical_operator)
            : $query->where("brand_id", $operator, $condition['value'], $logical_operator);
    }

    /**
     * @param Builder $query
     * @param string $logical_operator
     * @param array $condition
     *
     * @return Builder
     */
    public function categories(Builder $query, string $logical_operator, array $condition): Builder
    {
        $operator = self::GetOperator('comparison', $condition['operator'], '=');

        return is_array($condition['value'])
            ? $query->whereIn("category_id", $condition['value'], $logical_operator)
            : $query->where("category_id", $operator, $condition['value'], $logical_operator);
    }

    /**
     * @param Builder $query
     * @param string $logical_operator
     * @param array $condition
     *
     * @return Builder
     */
    public function points(Builder $query, string $logical_operator, array $condition): Builder
    {
        $operator = self::GetOperator('comparison', $condition['operator'], '=');

        $condition_values = Collection::make($condition['value']);

        // Get points and get its type (accumulative or per-order).
        $points = $condition_values->where('type', 'points')->first()['value'] ?? 0;
        $is_accumulative = ($condition_values->where('type', 'accumulation')->first()['value'] ?? null) === 'accumulative';

        return !$is_accumulative
            ? $query->where('subtotal_points', $operator, round($points), $logical_operator)
            : $query->where(function (Builder $nested_query) use ($points) {
                $subtotal = round(DB::table('cart_variants')->first()->subtotal_points);
                $offer_consumption_points =
                    $this->offerConsumptionCount($this->offer, Auth::guard('members')->user()) * $points;

                // Accumulation Query.
                $nested_query
                    ->selectRaw("IFNULL(SUM(total_points) + $subtotal - $offer_consumption_points, $subtotal)")
                    ->from('member_points')
                    ->where('member_id', Auth::guard('members')->id())
                    ->whereDate('order_creation_date', '>=', $this->offer->starts_at)
                    ->whereDate('order_creation_date', '<=', $this->offer->ends_at)
                    ->limit(1);
            }, $operator, round($points), $logical_operator);
    }

    /**
     * @param Builder $query
     * @param string $logical_operator
     * @param array $condition
     *
     * @return Builder
     */
    public function SKUs(Builder $query, string $logical_operator, array $condition): Builder
    {
        $operator = self::GetOperator('comparison', $condition['operator'], '=');

        return is_array($condition['value'])
            ? $query->whereIn("SKU", $condition['value'], $logical_operator)
            : $query->where("SKU", $operator, $condition['value'], $logical_operator);
    }
}
