<?php

namespace App\OfferConsumption\Parsers\MemberActions\AudienceListActions;

use App\Domain\Sheet\Model\Sheet;
use App\Imports\MembersImport;
use App\OfferConsumption\Parsers\MemberActions\AudienceListActions\Traits\AudienceListCommonActions;
use App\OfferConsumption\Parsers\MemberActions\Contracts\MemberActionsBuilder;
use App\OfferConsumption\Parsers\Traits\ParserConfiguration;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Excel as ReaderType;

class SignedUpAction implements MemberActionsBuilder
{
    use ParserConfiguration, AudienceListCommonActions;

    /**
     * @var QueryBuilder|EloquentBuilder
     */
    private QueryBuilder|EloquentBuilder $builder;

    /**
     * @var array
     */
    private array $action;

    /**
     * @param array $action
     * @param QueryBuilder|EloquentBuilder $builder
     */
    public function __construct(array $action, QueryBuilder|EloquentBuilder $builder)
    {
        $this->builder = $builder;
        $this->action = $action;
    }

    /**
     * @param $value
     * @return QueryBuilder|EloquentBuilder
     */
    public function from($value): QueryBuilder|EloquentBuilder
    {
        return $this->builder
            ->whereDate('member_creation_date', '>=', Carbon::parse($value));
    }

    /**
     * @param $value
     * @return QueryBuilder|EloquentBuilder
     */
    public function to($value): QueryBuilder|EloquentBuilder
    {
        return $this->builder
            ->whereDate('member_creation_date', '<=', Carbon::parse($value));
    }

    /**
     * @param QueryBuilder|EloquentBuilder $query
     * @param string $logical_operator
     * @param array $condition
     *
     * @return QueryBuilder|EloquentBuilder
     */
    public function member_sheet(QueryBuilder|EloquentBuilder $query, string $logical_operator, array $condition): QueryBuilder|EloquentBuilder
    {
        $sheet = Sheet::find($condition['value']);

        if ($sheet) {
            $file_path = storage_path('app/public/' . Sheet::find($condition['value'])->file);
            $member_codes = Excel::toCollection(
                new MembersImport, $file_path, null, ReaderType::CSV
            )->first()->except(0)->pluck(0)->toArray();
        }

        return $query->whereIn('code', ($member_codes ?? [0]), $logical_operator);
    }

}
