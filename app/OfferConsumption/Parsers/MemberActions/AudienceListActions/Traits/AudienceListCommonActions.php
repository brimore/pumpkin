<?php

namespace App\OfferConsumption\Parsers\MemberActions\AudienceListActions\Traits;

use App\Domain\EventType\Model\EventType;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Support\Str;

trait AudienceListCommonActions
{
    /**
     * @return QueryBuilder|EloquentBuilder
     */
    public function build(): QueryBuilder|EloquentBuilder
    {
        foreach ($this->action as $filter => $value)
            !(method_exists($this, $filter) && $value) ?: call_user_func([$this, $filter], $value);

        return $this->builder;
    }

    /**
     * @param $value
     * @return QueryBuilder|EloquentBuilder
     */
    public function event($value): QueryBuilder|EloquentBuilder
    {
        if (collect($this->action['conditions'] ?? [])->contains('attribute', 'number_of_orders'))
            return $this->builder;

        $event_type_id = EventType::whereName($value)->first()->id;

        return $this->builder->where('event_type_id', '=', $event_type_id);
    }

    /**
     * @param $conditions
     * @return QueryBuilder|EloquentBuilder
     */
    public function conditions($conditions): QueryBuilder|EloquentBuilder
    {
        return $this->builder->where(function ($query) use ($conditions) {
            $logical_operator = 'and';
            foreach ($conditions as $condition) {
                switch ($condition['type']) {
                    case 'logical-operator':
                        $logical_operator = Str::lower($condition['value']);
                        break;
                    case 'condition':
                        if (method_exists($this, $condition['attribute']))
                            call_user_func([$this, $condition['attribute']], $query, $logical_operator, $condition);
                        break;
                }
            }
        });
    }
}
