<?php

namespace App\OfferConsumption\Parsers\MemberActions\AudienceListActions;

use App\Domain\Offer\Model\Offer;
use App\OfferConsumption\Parsers\MemberActions\AudienceListActions\Traits\AudienceListCommonActions;
use App\OfferConsumption\Parsers\MemberActions\Contracts\MemberActionsBuilder;
use App\OfferConsumption\Parsers\Traits\ParserConfiguration;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class PlacedOrderAction implements MemberActionsBuilder
{
    use ParserConfiguration, AudienceListCommonActions;

    /**
     * @var QueryBuilder|EloquentBuilder
     */
    private QueryBuilder|EloquentBuilder $builder;

    /**
     * @var array
     */
    private array $action;

    /**
     * @var
     */
    private $from_date;

    /**
     * @var
     */
    private $to_date;

    /**
     * @param array $action
     * @param QueryBuilder|EloquentBuilder $builder
     * @param Offer $offer
     */
    public function __construct(array $action, QueryBuilder|EloquentBuilder $builder, Offer $offer)
    {
        $this->builder = $builder;
        $this->action = $action;
        $this->offer = $offer;
    }

    /**
     * @param $value
     * @return QueryBuilder|EloquentBuilder
     */
    public function from($value): QueryBuilder|EloquentBuilder
    {
        $this->from_date = $value;

        return collect($this->action['conditions'] ?? [])->contains('attribute', 'number_of_orders')
            ? $this->builder
            : $this->builder->whereDate('order_creation_date', '>=', Carbon::parse($value));
    }

    /**
     * @param $value
     * @return QueryBuilder|EloquentBuilder
     */
    public function to($value): QueryBuilder|EloquentBuilder
    {
        $this->to_date = $value;

        return collect($this->action['conditions'] ?? [])->contains('attribute', 'number_of_orders')
            ? $this->builder
            : $this->builder->whereDate('order_creation_date', '<=', Carbon::parse($value));
    }

    /**
     * @param QueryBuilder|EloquentBuilder $query
     * @param string $logical_operator
     * @param array $condition
     *
     * @return QueryBuilder|EloquentBuilder
     */
    public function source(QueryBuilder|EloquentBuilder $query, string $logical_operator, array $condition): QueryBuilder|EloquentBuilder
    {
        $operator = self::GetOperator('comparison', $condition['operator'], '=');

        return $query->where('order_source', $operator, $condition['value'], $logical_operator);
    }

    /**
     * @param QueryBuilder|EloquentBuilder $query
     * @param string $logical_operator
     * @param array $condition
     *
     * @return QueryBuilder|EloquentBuilder
     */
    public function total_points(QueryBuilder|EloquentBuilder $query, string $logical_operator, array $condition): QueryBuilder|EloquentBuilder
    {
        $operator = self::GetOperator('comparison', $condition['operator'], '=');
//        dd($operator);

        $condition_values = Collection::make($condition['value']);

        // Get points and get its type (accumulative or per-order).
        $points = $condition_values->where('type', 'total_points')->first()['value'] ?? 0;
        $is_accumulative = ($condition_values->where('type', 'accumulation')->first()['value'] ?? null) === 'accumulative';

        //For Between operator but for now we removed it from placed_order operators
        if (is_array($points) && $operator == 'between') {

            $upper_limit = ($points[0] > $points[1]) ? $points[0] : $points[1];
            $lower_limit = ($points[0] > $points[1]) ? $points[1] : $points[0];

            return !$is_accumulative
                ? $query->whereIn('members.id', function (Builder $nested_query) use ($operator, $points, $lower_limit, $upper_limit) {
                    $this->actualOrdersQuery($nested_query, $this->from_date, $this->to_date)
                        ->select('member_id')
                        ->whereBetween('total_points', [$lower_limit, $upper_limit])
                        ->groupBy('member_id');
                }, $logical_operator)
                : $query->whereIn('members.id', function (Builder $nested_query) use ($operator, $points, $upper_limit, $lower_limit) {
                    // Accumulation Query.
                    $this->actualOrdersQuery($nested_query, $this->from_date, $this->to_date)
                        ->select('member_id')
                        ->groupBy('member_id')
                        ->havingRaw("SUM(total_points) $operator $lower_limit and $upper_limit");
                }, $logical_operator);
        }

        return !$is_accumulative
            ? $query->whereIn('members.id', function (Builder $nested_query) use ($operator, $points) {
                $this->actualOrdersQuery($nested_query, $this->from_date, $this->to_date)
                    ->select('member_id')
                    ->where('total_points', $operator, $points)
                    ->groupBy('member_id');
            }, $logical_operator)
            : $query->whereIn('members.id', function (Builder $nested_query) use ($operator, $points) {
                // Accumulation Query.
                $this->actualOrdersQuery($nested_query, $this->from_date, $this->to_date)
                    ->select('member_id')
                    ->groupBy('member_id')
                    ->havingRaw("SUM(total_points) $operator $points");
            }, $logical_operator);
    }

    /**
     * @param QueryBuilder|EloquentBuilder $query
     * @param string $method
     * @param array $condition
     *
     * @return QueryBuilder|EloquentBuilder
     */
    public function number_of_orders(QueryBuilder|EloquentBuilder $query, string $logical_operator, array $condition): QueryBuilder|EloquentBuilder
    {
        $operator = self::GetOperator('comparison', $condition['operator'], '=');

        return
            $query->whereIn('member_id', function (Builder $nested_query) use ($condition, $operator) {
                $operator = !$condition['value'] && $operator == '=' ? '>' : $operator;

                $this->actualOrdersQuery($nested_query, $this->from_date, $this->to_date)
                    ->groupBy('member_id')
                    ->havingRaw("COUNT(*) $operator {$condition['value']}");
            }, $logical_operator, (!$condition['value'] && $operator == '='));
    }

    /**
     * @param QueryBuilder|EloquentBuilder $query
     * @param string $logical_operator
     * @param array $condition
     * @return QueryBuilder|EloquentBuilder
     */
    public function offers(QueryBuilder|EloquentBuilder $query, string $logical_operator, array $condition): QueryBuilder|EloquentBuilder
    {
        $is_not_query = $condition['operator'] === 'not_in';

        return $query->whereIn('member_id', function (Builder $nested_query) use ($condition) {
            $nested_query
                ->select('member_id')
                ->from('member_offer_consumption')
                ->whereIn('offer_id', $condition['value']);

            if (Auth::guard('members')->check())
                $nested_query->where('member_id', Auth::guard('members')->id());
        }, $logical_operator, $is_not_query);
    }

    /**
     * @param $query
     * @param $from_date
     * @param $to_date
     *
     * @return mixed
     */
    private function actualOrdersQuery($query, $from_date, $to_date): mixed
    {
        $query->select('member_id')->from('member_points');

        if (Auth::guard('members')->check())
            $query->where('member_id', Auth::guard('members')->id());

        if ($from_date)
            $query->whereDate('order_creation_date', '>=', Carbon::parse($from_date));
        if ($to_date)
            $query->whereDate('order_creation_date', '<=', Carbon::parse($to_date));

        return $query;
    }
}
