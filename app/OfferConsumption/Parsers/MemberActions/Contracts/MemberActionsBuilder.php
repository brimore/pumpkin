<?php

namespace App\OfferConsumption\Parsers\MemberActions\Contracts;

interface MemberActionsBuilder
{
    /**
     * @return mixed
     */
    public function build(): mixed;
}
