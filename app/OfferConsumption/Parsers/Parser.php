<?php

namespace App\OfferConsumption\Parsers;

use App\OfferConsumption\Parsers\Factories\Contracts\ParserFactory;
use App\OfferConsumption\Parsers\Traits\ParserConfiguration;

class Parser
{
    use ParserConfiguration;

    /**
     * @var array
     */
    protected array $criteria;

    /**
     * Parser constructor.
     *
     * @param array $criteria
     */
    public function __construct(array $criteria)
    {
        $this->criteria = $criteria;
    }

    /**
     * Applies parsing based on current criteria.
     *
     * @return mixed
     */
    public function parse(ParserFactory $factory): ParserFactory
    {
        foreach ($this->criteria as $filter) {
            krsort($filter);
            switch ($filter['type']) {
                case 'logical-operator':
                    $factory->setLogicalOperator($filter['value']);
                    break;
                default:
                    $factory->setFilter($filter)->parse();
                    break;
            }
        }

        return $factory;
    }
}
