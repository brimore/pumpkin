<?php

namespace App\OfferConsumption\Multiples;

use App\Domain\Offer\Model\Offer;
use App\OfferConsumption\Parsers\Traits\ParserConfiguration;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

abstract class MultiplesParser
{
    use ParserConfiguration;

    /**
     * @var Offer $offer
     */
    protected Offer $offer;

    /**
     * @var array $personal_cap
     */
    protected array $personal_cap = [];

    /**
     * @var $currentMultiples
     */
    protected $currentMultiples;

    /**
     * @param Offer $offer
     */
    public function __construct(Offer $offer, $currentMultiples = 0)
    {
        $this->offer = $offer;
        $this->currentMultiples = $currentMultiples;
    }

    /**
     * @return float
     */
    public function apply(): float
    {
        foreach ($this->offer->multiples as $multiple) {
            $per_order = $multiple['per_order'] ?? false;
            if (method_exists($this, $multiple['type'])) {
                call_user_func([$this, $multiple['type']], $multiple['conditions'], Auth::guard('members')->user());
                $previously_received = !$per_order
                    ? $this->offerConsumptionCount($this->offer, Auth::guard('members')->user()) : 0;
                $this->personal_cap[count($this->personal_cap) - 1] = max((last($this->personal_cap) - $previously_received), 0);
            }
        }

        return !empty($this->personal_cap) ? max(min($this->personal_cap), 0) : 0;
    }
}
