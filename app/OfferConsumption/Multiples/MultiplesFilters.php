<?php

namespace App\OfferConsumption\Multiples;

use App\Domain\Offer\Model\Offer;
use App\Domain\Sheet\Model\Sheet;
use App\OfferConsumption\Parsers\Traits\ParserConfiguration;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MultiplesFilters extends MultiplesParser
{
    use ParserConfiguration;

    /**
     * @param $condition
     *
     * @return void
     */
    public function cap($condition): void
    {
        $this->personal_cap[] = (int)$condition['value'];
    }

    /**
     * @param $condition
     *
     * @return void
     */
    public function consumed_offer_count($condition): void
    {
        $offer = Offer::whereUuid($condition['offer_id'])->withTrashed()->first();

        if (isset($condition['fraction']) && $condition['fraction']) {
            $order_points = DB::table('member_offer_consumption')
                    ->join('member_points', 'member_offer_consumption.order_id', '=', 'member_points.order_id', 'inner')
                    ->select('member_points.total_points')
                    ->where('member_offer_consumption.offer_id', '=', $offer->uuid)
                    ->where('member_offer_consumption.member_id', Auth::guard('members')->user()->id)
                    ->first()?->total_points ?? 0;
            $condition = $offer->condition_points;

            $fraction_multiples = $order_points / $condition;
            if ($fraction_multiples < 1) {
                $this->personal_cap[] = 0;
            } else {
                $this->personal_cap[] = sprintf('%.1f', $fraction_multiples);
            }
        } else
            $this->personal_cap[] = $offer
                ? $this->offerConsumptionCount($offer, Auth::guard('members')->user(), $condition['from'], $condition['to'])
                : 0;
    }

    /**
     * @param $condition
     * @param Authenticatable $member
     *
     * @return void
     */
    public function order_amount($condition, Authenticatable $member): void
    {
        $parsed_value = explode('/', $condition['formula'])[1];

        $order_query = DB::table('member_points')
            ->selectRaw('sum(total_points/?) as multiples', [$parsed_value])
            ->where('member_id', '=', $member->id)
            ->whereBetween('order_creation_date', [Carbon::parse($condition['from']), Carbon::parse($condition['to'])])
            ->where('total_points', '>=', $parsed_value)
            ->first()->multiples;

        if (isset($condition['fraction']) && $condition['fraction'])
            $order_query = sprintf('%.1f', $order_query);
        else $order_query = floor($order_query);

        $order_query = $order_query ?: 0;

        $this->personal_cap[] = $order_query;
    }

    /**
     * @param $condition
     * @param Authenticatable $member
     * @return void
     */
    public function sheet($condition, Authenticatable $member): void
    {
        $sheet = Sheet::find($condition['value']);

        if ($sheet)
            $this->personal_cap[] =
                $sheet->listData()->where('module_id', $member->code)->first()?->extra_attributes['quantity'];
    }

    /**
     * @param $condition
     * @return void
     */
    public function offer_consumption($condition): void
    {
        $offerConsumption = DB::table('member_offer_consumption')
            ->where('offer_id', $condition['offer_id'])->sum('quantity');

        $multiplies = ((int)$condition['cap']) - ((int)$offerConsumption);

        $this->personal_cap[] = max($multiplies, 0);
    }

    /**
     * @param $condition
     * @return void
     */
    public function offer_consumption_per_member($condition): void
    {
        $offerConsumption = DB::table('member_offer_consumption')
            ->select(DB::raw('COUNT(DISTINCT member_id) as distinct_members'))
            ->where('offer_id', $condition['offer_id'])
            ->first()->distinct_members;

        $multiplies = ((int)$condition['cap']) - $offerConsumption;

        $this->personal_cap[] = max($multiplies, 0) ? $this->currentMultiples : 0;
    }
}
