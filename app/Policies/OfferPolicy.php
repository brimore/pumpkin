<?php
namespace App\Policies;



use App\Domain\Offer\Model\Offer;
use App\Models\User;
use LaravelJsonApi\Core\Store\LazyRelation;

class OfferPolicy
{

    /**
     * Authorize a user to update a post.
     *
     * @param User|null $user
     * @param Offer $offer
     * @param LazyRelation $relation
     * @return bool
     */
    public function updateAudienceList( ?User $user, Offer $offer, LazyRelation $relation): bool
    {
        /** @var User|null $audience_list */
        $audience_list = $relation->get();
        return (bool)$audience_list;
    }
    /**
     * Authorize a user to view a post.
     *
     * @param User|null $user
     * @param Offer $offer
     * @return bool
     */
    public function viewAudienceList( ?User $user, Offer $offer): bool
    {

        return true;
    }
}
