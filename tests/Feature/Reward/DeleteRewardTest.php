<?php

namespace Tests\Feature\Reward;

use App\Domain\Reward\Model\Reward;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use LaravelJsonApi\Testing\TestResponse;
use Tests\TestCase;
class DeleteRewardTest extends TestCase
{
    /** @test */
    public function an_authorized_user_can_delete_reward(): void
    {
        // At first, we will create and act as authorized member.
        $this->actingAsMember();

        // Then, we will create fake reward.
        $reward = $this->createFakeReward();

        // then, we will hit delete endpoint .
        $response = $this->hitDeleteRewardEndpoint($reward->id);

        // Response should be 204.
        $response->assertNoContent();

        // Assert that database doesn't have the reward.
        $this->assertDatabaseCount('rewards', 0);
        $this->assertDatabaseMissing('rewards', $reward->toArray());
    }
    /** @test */
    public function a_guest_cannot_delete_reward(): void
    {
        // At first, we will create fake reward.
        $reward = $this->createFakeReward();

        // Then, we will hit delete endpoint .
        $response = $this->hitDeleteRewardEndpoint($reward->id);

        // Response should be 401.
        $response->assertStatus(401);

        // Assert that database has the reward.
        $this->assertDatabaseCount('rewards', 1);
        $this->assertDatabaseHas('rewards', ['id' => $reward->id]);
    }
    /**
     * @return Collection|Model
     */
    private function createFakeReward(): Collection|Model
    {
        return Reward::factory()->create();
    }

    /**
     * @param int $id
     * @return TestResponse
     */
    private function hitDeleteRewardEndpoint(int $id): TestResponse
    {
        return $this
            ->jsonApi()
            ->delete(route('v1.rewards.destroy', $id));
    }
}
