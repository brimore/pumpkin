<?php

namespace Tests\Feature\Reward;

use App\Domain\Reward\Model\Reward;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use LaravelJsonApi\Testing\TestResponse;
use Tests\TestCase;

class ListRewardTest extends TestCase
{
    /** @test */
    public function an_authorized_user_can_list_rewards(): void
    {
        // At first, we will create and act as authorized member.
        $this->actingAsMember();

        // Then, we will create fake data of rewards.
        $rewards = $this->createFakeRewards(5);

        // At least, we will hit the list endpoint.
        $response = $this->hitListRewardEndpoint();

        // Response should be 200.
        $response->assertStatus(200);

        // Assert we have fetched all rewards.
        $response->assertFetchedMany($rewards);
    }
    /** @test */
    public function a_guest_cannot_list_rewards(): void
    {
        // At first, we will create fake data of rewards.
        $this->createFakeRewards(5);

        // Then, we will hit the list endpoint.
        $response = $this->hitListRewardEndpoint();

        // Response should be 401.
        $response->assertStatus(401);
    }
    /**
     * @return TestResponse
     */
    private function hitListRewardEndpoint(): TestResponse
    {
        return $this
            ->jsonApi()
            ->expects('rewards')
            ->get(route('v1.rewards.index'));
    }
    /**
     * @param $count
     * @return Collection|Model
     */
    private function createFakeRewards($count): Collection|Model
    {
        return Reward::factory()->count($count)->create();
    }

}
