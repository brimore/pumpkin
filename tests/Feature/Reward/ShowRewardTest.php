<?php

namespace Tests\Feature\Reward;

use App\Domain\Reward\Model\Reward;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use LaravelJsonApi\Testing\TestResponse;
use Tests\TestCase;


class ShowRewardTest extends TestCase
{
    /** @test */
    public function a_guest_cannot_show_reward(): void
    {
        // At first, we will create a reward that will be shown.
        $reward = $this->createFakeRewards();

        // Then, we will hit show endpoint.
        $response = $this->hitShowRewardEndpoint($reward->id);

        // Response should be 401.
        $response->assertStatus(401);
    }

    /** @test */
    public function an_authorized_user_can_show_reward(): void
    {
        // At first, we will create and act as authorized member.
        $this->actingAsMember();

        // Then, we will create a reward that will be shown.
        $reward = $this->createFakeRewards();

        // At least, we will hit the show endpoint.
        $response = $this->hitShowRewardEndpoint($reward->id);

        // Response should be 200.
        $response->assertStatus(200);

        // assert that we have fetched one condition.
        $response->assertFetchedOne($reward);
    }
    /**
     * @param int $id
     * @return TestResponse
     */
    private function hitShowRewardEndpoint(int $id): TestResponse
    {
        return $this
            ->jsonApi()
            ->expects('rewards')
            ->get(route('v1.rewards.show',$id));
    }
    /**
     * @return Collection|Model
     */
    private function createFakeRewards(): Collection|Model
    {
        return Reward::factory()->create();
    }
}
