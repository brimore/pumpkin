<?php

namespace Tests\Feature\Reward;

use App\Domain\Reward\Model\Reward;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use LaravelJsonApi\Testing\TestResponse;
use Tests\TestCase;

class StoreRewardTest extends TestCase
{
    /** @test */
    public function an_authorized_user_can_create_reward(): void
    {
        // At first, we will create and act as authorized member.
        $this->actingAsMember();

        // Then, we will create fake data of reward and hit store endpoint.
        list($reward, $expected_data) = $this->createFakeRewards();

        //then, we will hit the store endpoint.
        $response = $this->hitStoreRewardEndpoint($expected_data);

        // Response should be 201.
        $response->assertCreated();

        // Assert that database has the reward.
        $reward_db = Reward::select(['id', 'name'])->whereId($response->getId())->first()->toArray();
        $this->assertEquals($reward_db, [
            'id' => $response->getId(),
            'name' => $reward['name']
        ]);
        $this->assertDatabaseCount('rewards', 1);
    }
    /** @test */
    public function a_guest_cannot_create_reward(): void
    {
        // At first, we will create fake data of reward and hit store endpoint.
        list($reward, $expected_data) = $this->createFakeRewards();

        // Then, we will hit the store endpoint.
        $response = $this->hitStoreRewardEndpoint($expected_data);

        // Response should be 401.
        $response->assertStatus(401);

        // Assert that database doesn't have the condition.
        $this->assertDatabaseCount('rewards', 0);
        $this->assertDatabaseMissing('rewards', $reward);
    }
    /**
     * @param array $data
     * @return TestResponse
     */
    private function hitStoreRewardEndpoint(array $data): TestResponse
    {
        return $this
            ->jsonApi()
            ->withData($data)
            ->post(route('v1.rewards.store'));
    }

    /**
     * @return array
     */
    private function createFakeRewards(): array
    {
        $reward = Reward::factory()->make()->toArray();
        $expected_data = $this->prepareJsonAPIDataObject('rewards', $reward,null);
        return array($reward, $expected_data);
    }
}
