<?php

namespace Tests\Feature\Reward;

use App\Domain\Reward\Model\Reward;
use LaravelJsonApi\Testing\TestResponse;
use Tests\TestCase;

class UpdateRewardTest extends TestCase
{
    /** @test */
    public function an_authorized_user_can_update_reward(): void
    {
        // At first, we will create and act as authorized member.
        $this->actingAsMember();

        // Then, we will create a reward that will be updated later.
        $reward = Reward::factory()->create();

        // Then, we will make fake data of reward and hit update endpoint.
        $expected_data = $this->createFakeReward($reward->id);

        // then, we will hit the update endpoint.
        $response = $this->hitUpdateRewardEndpoint($expected_data, $reward->id);

        // Response should be 200.
        $response->assertStatus(200);

        // Assert that database has the updated condition.
        $reward_db = Reward::select(['id', 'name'])->whereId($reward->id)->first()->toArray();
        $this->assertEquals($reward_db, [
            'id' => $reward->id,
            'name' => $expected_data['attributes']['name']
        ]);
        $this->assertNotEquals($reward_db, [
            'id' => $reward->id,
            'name' => $reward->name
        ]);
        $this->assertDatabaseCount('rewards', 1);
    }
    /** @test */
    public function a_guest_cannot_update_reward(): void
    {
        // At first, we will create a reward that will be updated later.
        $reward= Reward::factory()->create();

        // Then, we will create fake data of reward and hit update endpoint.
        $expected_data = $this->createFakeReward($reward->id);

        // then, we will hit the update endpoint.
        $response = $this->hitUpdateRewardEndpoint($expected_data, $reward->id);

        // Response should be 401.
        $response->assertStatus(401);
        $reward_db = Reward::select(['id', 'name'])->whereId($reward->id)->first()->toArray();
        $this->assertNotEquals($reward_db, [
            'id' => $reward->id,
            'name' => $expected_data['attributes']['name']
        ]); 

        $this->assertEquals($reward_db, [
            'id' => $reward->id,
            'name' =>json_decode($reward->getAttributes()['name'], true)
            ]);
    }
    /**
     * @param array $data
     * @param string $id
     * @return TestResponse
     */
    private function hitUpdateRewardEndpoint(array $data, string $id): TestResponse
    {
        return $this
            ->jsonApi()
            ->withData($data)
            ->patch(route('v1.rewards.update', $id));
    }

    /**
     * @param $id
     * @return array
     */
    private function createFakeReward($id): array
    {
        $reward = Reward::factory()->make()->toArray();

        return $this->prepareJsonAPIDataObject('rewards', $reward, $id);
    }
}
