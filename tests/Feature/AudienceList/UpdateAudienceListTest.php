<?php

namespace Tests\Feature\AudienceList;

use App\Domain\AudienceList\Model\AudienceList;
use LaravelJsonApi\Testing\TestResponse;
use Tests\TestCase;
class UpdateAudienceListTest extends TestCase
{
    /** @test */
    public function an_authorized_user_can_update_name_audience_list(): void
    {
        // At first, we will create and act as authorized member.
        $this->actingAsMember();

        // Then, we will create fake data of audience list and hit update endpoint.
        list($audience_list, $expected_data, $id) = $this->createFakeAudienceList();

        //update expected data
        $expected_data = $this->updateExpectedData($expected_data, 'name');
        // At least, we will hit the update endpoint.
        $response = $this->hitUpdateAudienceEndpoint($expected_data, $id);

        // Response should be 200.
        $response->assertStatus(200);
        // Assert that database has the audience list.
        $audience_list_db = AudienceList::select(['id', 'name', 'config'])
            ->whereId($response->getId())->first()->toArray();
        $this->assertEquals($audience_list_db, [
            'id' => $response->getId(),
            'name' => $expected_data['attributes']['name'],
            'config' => $expected_data['attributes']['config']
        ]);
        $this->assertDatabaseCount('audience_lists', 1);
    }
    /** @test */
    public function an_authorized_user_can_update_config_audience_list(): void
    {
        // At first, we will create and act as authorized member.
        $this->actingAsMember();

        // Then, we will create fake data of audience list and hit update endpoint.
        list($audience_list, $expected_data, $id) = $this->createFakeAudienceList();

        //update expected data
        $expected_data = $this->updateExpectedData($expected_data, 'config');
        // At least, we will hit the update endpoint.
        $response = $this->hitUpdateAudienceEndpoint($expected_data, $id);

        // Response should be 200.
        $response->assertStatus(200);

        // Assert that database has the audience list.
        $audience_list_db = AudienceList::select(['id', 'name', 'config'])
            ->whereId($response->getId())->first()->toArray();
        $this->assertEquals($audience_list_db, [
            'id' => $response->getId(),
            'name' => $expected_data['attributes']['name'],
            'config' => $expected_data['attributes']['config']
        ]);
        $this->assertDatabaseCount('audience_lists', 1);
    }
    /** @test
     * @dataProvider missingParameterDataProvider
     */
    public function an_authorized_user_cannot_update_config_with_missing_parameter_audience_list($missingParameter): void
    {
        // At first, we will create and act as authorized member.
        $this->actingAsMember();

        // Then, we will create fake data of audience list and hit update endpoint.
        list($audience_list, $expected_data, $id) = $this->createFakeAudienceList();

        //update expected data
        $expected_data = $this->updateExpectedData($expected_data, 'config',true);
        // At least, we will hit the update endpoint.
        $response = $this->hitUpdateAudienceEndpoint($expected_data, $id);

        // Response should be 422.
        $response->assertStatus(422);

    }
    /** @test */
    public function a_guest_cannot_update_audience_list(): void
    {
        // we will create fake data of audience list and hit update endpoint.
        list($audience_list, $expected_data, $id) = $this->createFakeAudienceList();

        //update expected data
        $expected_data = $this->updateExpectedData($expected_data, 'name');
        // we will hit the update endpoint.
        $response = $this->hitUpdateAudienceEndpoint($expected_data, $id);
        // Response should be 401.
        $response->assertStatus(401);

    }
    /** @test
     * @dataProvider missingParameterDataProvider
     */
    public function an_authorized_user_cannot_update_config_unmatched_filters_operators_audience_list($missingParameter): void
    {
        // At first, we will create and act as authorized member.
        $this->actingAsMember();

        // Then, we will create fake data of audience list and hit update endpoint.
        list($audience_list, $expected_data, $id) = $this->createFakeAudienceList();

        //update expected data
        $expected_data = $this->updateExpectedData($expected_data, 'config',true, true);
        // At least, we will hit the update endpoint.
        $response = $this->hitUpdateAudienceEndpoint($expected_data, $id);

        // Response should be 422.
        $response->assertStatus(422);

    }

    /**
     * @param array $data
     * @param string $id
     * @return TestResponse
     */
    private function hitUpdateAudienceEndpoint(array $data, string $id): TestResponse
    {
        return $this
            ->jsonApi()
            ->withData($data)
            ->patch(route('v1.audience-lists.update',$id));
    }

    /**
     * @return array
     */
    private function createFakeAudienceList(): array
    {
        $audience_list = AudienceList::factory()->create();
        $id = (string)$audience_list->getRouteKey();
        unset($audience_list['id'],$audience_list['uuid'],$audience_list['updated_at'],$audience_list['created_at']);
        $audience_list = $audience_list->toArray();
        $expected_data = $this->prepareJsonAPIDataObject('audience-lists', $audience_list, $id);
        return array($audience_list, $expected_data, $id);
    }

    /**
     * @param array $expected_data
     * @param string $key
     * @param bool $unset
     * @return array
     */
    private function updateExpectedData(array $expected_data, string $key, ?bool $unset=false, ?bool $unmatched=false): array
    {
        if ($unmatched) {
            $expected_data['attributes']['config'] = [
                [
                    "type" => "filter",
                    "event" => "SIGNED_UP",
                    "from" => null,
                    "to" => null,
                    "conditions" => [
                        [
                            "type" => "condition",
                            "attribute" => "gender",
                            "operator" => "=",
                            "value" => "male"
                        ],
                        [
                            "type" => "logical-operator",
                            "value" => "AND"
                        ],
                        [
                            "type" => "condition",
                            "attribute" => "age",
                            "operator" => ">",
                            "value" => 50
                        ]

                    ]],
//                [
//                    "type" => "logical-operator",
//                    "value" => "OR"
//                ],
                [
                    "type" => "filter-group",
                    "config" => [
                        [
                            "type" => "filter",
                            "event" => "PLACED_ORDER",
                            "from" => null,
                            "to" => null,
                            "conditions" => [
                                [
                                    "type" => "condition",
                                    "attribute" => "amount",
                                    "operator" => ">=",
                                    "value" => 1000
                                ]
                            ]
                        ]
                    ]
                ]
            ];
            return $expected_data;
        }
        if ($key ===  'name') {

            foreach (get_locales() as $locale) {
                $expected_data['attributes'][$key][$locale] = 'updated name in '. $locale;
            }
        }
        elseif ($key ===  'config'){
           if($unset){
            unset($expected_data['attributes'][$key][0]['conditions'][0]['operator']);
           } else{
               $expected_data['attributes'][$key] = [
                   [
                       "type" => "filter",
                       "event" => "SIGNED_UP",
                       "from" => null,
                       "to" => null,
                       "conditions" => [
                           [
                               "type" => "condition",
                               "attribute" => "gender",
                               "operator" => "=",
                               "value" => "male"
                           ],
                           [
                               "type" => "logical-operator",
                               "value" => "AND"
                           ],
                           [
                               "type" => "condition",
                               "attribute" => "age",
                               "operator" => ">",
                               "value" => 50
                           ]

                       ]],
                   [
                       "type" => "logical-operator",
                       "value" => "OR"
                   ],
                   [
                       "type" => "filter-group",
                       "config" => [
                           [
                               "type" => "filter",
                               "event" => "PLACED_ORDER",
                               "from" => null,
                               "to" => null,
                               "conditions" => [
                                   [
                                       "type" => "condition",
                                       "attribute" => "amount",
                                       "operator" => ">=",
                                       "value" => 1000
                                   ]
                               ]
                           ]
                       ]
                   ]
               ];
           }
        }
        return $expected_data;
    }

    public function missingParameterDataProvider()
    {
        return [
            'missing attribute' => ['attribute'],
            'missing operator' => ['operator'],
            'missing type' => ['type'],
            'missing value' => ['value']
        ];
    }
}
