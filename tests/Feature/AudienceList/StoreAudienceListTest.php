<?php

namespace Tests\Feature\AudienceList;

use App\Domain\AudienceList\Model\AudienceList;
use LaravelJsonApi\Testing\TestResponse;
use Tests\TestCase;

class StoreAudienceListTest extends TestCase
{
    /** @test */
    public function an_authorized_user_can_create_audience_list(): void
    {
        // At first, we will create and act as authorized member.
        $this->actingAsMember();

        // Then, we will create fake data of audience list and hit store endpoint.
        list($audience_list, $expected_data) = $this->createFakeAudienceList();

        // At least, we will hit the store endpoint.
        $response = $this->hitStoreAudienceEndpoint($expected_data);

        // Response should be 201.
        $response->assertCreated();

        // Assert that database has the audience list.
        $audience_list_db = AudienceList::select(['id', 'name', 'config'])
            ->whereId($response->getId())->first()->toArray();
        $this->assertEquals($audience_list_db, [
            'id' => $response->getId(),
            'name' => $audience_list['name'],
            'config' => $audience_list['config']
        ]);
        $this->assertDatabaseCount('audience_lists', 1);
    }
    /** @test */
    public function a_guest_cannot_create_audience_list(): void
    {
        // At first, we will create fake data of audience list and hit store endpoint.
        list($audience_list, $expected_data) = $this->createFakeAudienceList();

        // Then, we will hit the store endpoint.
        $response = $this->hitStoreAudienceEndpoint($expected_data);

        // Response should be 401.
        $response->assertStatus(401);

        // Assert that database doesn't have the audience list.
        $this->assertDatabaseCount('audience_lists', 0);
        $this->assertDatabaseMissing('audience_lists', $audience_list);
    }

    /**
     * @param array $data
     * @return TestResponse
     */
    private function hitStoreAudienceEndpoint(array $data): TestResponse
    {
        return $this
            ->jsonApi()
            ->withData($data)
            ->post(route('v1.audience-lists.store'));
    }

    /**
     * @return array
     */
    private function createFakeAudienceList(): array
    {
        $audience_list = AudienceList::factory()->make()->toArray();
        $expected_data = $this->prepareJsonAPIDataObject('audience-lists', $audience_list);
        return array($audience_list, $expected_data);
    }
}
