<?php

namespace Tests\Feature\AudienceList;

use App\Domain\AudienceList\Model\AudienceList;
use LaravelJsonApi\Testing\TestResponse;
use Tests\TestCase;

class DeleteAudienceListTest extends TestCase
{
    /** @test */
    public function an_authorized_user_can_delete_audience_list(): void
    {
        // At first, we will create and act as authorized member.
        $this->actingAsMember();

        // Then, we will create fake data of audience list and hit delete endpoint.
        list($audience_list, $id) = $this->createFakeAudienceList();

        // At least, we will hit the 'delete endpoint' .
        $response = $this->hitDeleteAudienceEndpoint($id);

        // Response should be 204.
        $response->assertNoContent();

        // Assert that database doesn't have the audience list.
        $this->assertDatabaseMissing('audience_lists', [
            'id' => $audience_list->getKey(),
        ]);
    }

    /** @test */
    public function an_authorized_user_entered_wrong_id_to_delete_audience_list(): void
    {
        // At first, we will create and act as authorized member.
        $this->actingAsMember();

        // Then, we will create fake data of audience list and hit delete endpoint.
        list($audience_list, $id) = $this->createFakeAudienceList();


        // At least, we will hit the 'delete endpoint' .
        $response = $this->hitDeleteAudienceEndpoint(7);

        // Response should be 204.
        $response->assertNotFound();

        // Assert that database has the audience list.
        $audience_list_db = AudienceList::select(['id'])
            ->whereId($id)->first()->toArray();
        $this->assertEquals($audience_list_db, compact('id'));
        $this->assertDatabaseCount('audience_lists', 1);
    }

    /** @test */
    public function a_guest_cannot_delete_audience_list(): void
    {
        // At first, we will create fake data of audience list and hit store endpoint.
        list($audience_list, $id) = $this->createFakeAudienceList();

        // Then, we will hit the 'delete endpoint'.
        $response = $this->hitDeleteAudienceEndpoint($id);

        // Response should be 401.
        $response->assertStatus(401);

        // Assert that database doesn't have the audience list.
        $this->assertDatabaseCount('audience_lists', 1);
        $audience_list = $audience_list->toArray();
        $this->assertDatabaseMissing('audience_lists', $audience_list);
    }

    /**
     * @return array
     */
    private function createFakeAudienceList(): array
    {
        $audience_list = AudienceList::factory()->create();
        $id = $audience_list->getRouteKey();

        return array($audience_list, $id);
    }

    /**
     * @param int $id
     * @return TestResponse
     */
    private function hitDeleteAudienceEndpoint(int $id): TestResponse
    {
        return $this
            ->jsonApi()
            ->delete(route('v1.audience-lists.destroy', $id));
    }

}
