<?php

namespace Tests\Feature\AudienceList;

use App\Domain\AudienceList\Model\AudienceList;
use LaravelJsonApi\Testing\TestResponse;
use Tests\TestCase;

class ShowAudienceListTest extends TestCase
{
    /** @test */
    public function a_guest_cannot_show_an_audience_list(): void
    {
        // number of audience lists that we will create it
        $count = 1;
        // we will create fake  audience list.
        [$expected_data, $id] = $this->createFakeAudienceList($count);
        // we will hit the show endpoint.
        $response = $this->hitShowAudienceEndpoint($id);
        // Response should be 401.
        $response->assertStatus(401);

    }
    /** @test */
    public function an_authorized_user_can_show_an_audience_list(): void
    {
        // At first, we will create and act as authorized member.
        $this->actingAsMember();
        // number of audience lists that we will create it
        $count = 5;
        // we will create fake  audience list.
        [$expected_data, $id] = $this->createFakeAudienceList($count);
        // we will hit the show endpoint.
        $response = $this->hitShowAudienceEndpoint($id);
        // Response should be 200.
        $response->assertStatus(200);
        // assert that we have fetched one audience list
        $response->assertFetchedOne($expected_data);
    }
    /** @test */
    public function an_authorized_user_cannot_show_an_audience_list_that_is_not_available(): void
    {
        // At first, we will create and act as authorized member.
        $this->actingAsMember();
        // number of audience lists that we will create it
        $count = 5;
        // we will create fake data of audience list.
        [$expected_data, $id] = $this->createFakeAudienceList($count);
        // wrong id that will be sent to endpoint
        $wrong_id = $count + 5;
        // we will hit the show endpoint.
        $response = $this->hitShowAudienceEndpoint($wrong_id);
        // assert that response status is 404 : not found
        $response->assertStatus(404);
    }
    /**
     * @param string $id
     * @return TestResponse
     */
    private function hitShowAudienceEndpoint(string $id): TestResponse
    {
        return $this
            ->jsonApi()
            ->expects('audience-lists')
            ->get(route('v1.audience-lists.show',$id));
    }
    /**
     * @return array
     */
    private function createFakeAudienceList($count): array
    {
        $audience_list = AudienceList::factory()->count($count)->create();
        $id = (string)$audience_list[0]['id'];
        $expected_data = AudienceList::find($id);
        return array($expected_data, $id);
    }
}
