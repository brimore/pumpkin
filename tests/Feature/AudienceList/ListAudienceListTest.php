<?php

namespace Tests\Feature\AudienceList;

use App\Domain\AudienceList\Model\AudienceList;
use LaravelJsonApi\Testing\TestResponse;
use Tests\TestCase;

class ListAudienceListTest extends TestCase
{
    /** @test */
    public function a_guest_cannot_List_audience_list(): void
    {
        // we will create fake audience lists.
        [$audience_list] = $this->createFakeAudienceList(3);
        // we will hit the show endpoint.
        $response = $this->hitListAudienceEndpoint();
        // Response should be 401.
        $response->assertStatus(401);

    }
    /** @test */
    public function an_authorized_user_can_list_audience_list(): void
    {
        // At first, we will create and act as authorized member.
        $this->actingAsMember();
        // we will create fake data of audience list.
        [$audience_list] = $this->createFakeAudienceList(5);
        // we will hit the list endpoint.
        $response = $this->hitListAudienceEndpoint();
        // Response should be 200.
        $response->assertStatus(200);
        // assert we have fetched more than one audience list
        $response->assertFetchedMany($audience_list);
    }
    /**
     * @return TestResponse
     */
    private function hitListAudienceEndpoint(): TestResponse
    {
        return $this
            ->jsonApi()
            ->expects('audience-lists')
            ->get(route('v1.audience-lists.index'));
    }

    /**
     * @param $count
     * @return array
     */
    private function createFakeAudienceList($count): array
    {
        $audience_list = AudienceList::factory()->count($count)->create();
        return array($audience_list);
    }
}
