<?php

namespace Tests\Feature\Condition;

use App\Domain\Condition\Model\Condition;
use App\Domain\Offer\Model\Offer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use LaravelJsonApi\Testing\TestResponse;
use Tests\TestCase;

class StoreConditionTest extends TestCase
{
    /** @test */
    public function an_authorized_user_can_create_condition(): void
    {
        // At first, we will create and act as authorized member.
        $this->actingAsMember();

        // Then, we will create fake data of condition and hit store endpoint.
        list($condition, $expected_data) = $this->createFakeCondition();

        // At least, we will hit the store endpoint.
        $response = $this->hitStoreConditionEndpoint($expected_data);

        // Response should be 201.
        $response->assertCreated();

        // Assert that database has the condition.
        $condition_db = Condition::select(['id', 'criteria'])->whereId($response->getId())->first()->toArray();
        $this->assertEquals($condition_db, [
            'id' => $response->getId(),
            'criteria' => $condition['criteria']
        ]);
        $this->assertDatabaseCount('conditions', 1);
    }

    /** @test */
    public function a_guest_cannot_create_condition(): void
    {
        // At first, we will create fake data of condition and hit store endpoint.
        list($condition, $expected_data) = $this->createFakeCondition();

        // Then, we will hit the store endpoint.
        $response = $this->hitStoreConditionEndpoint($expected_data);

        // Response should be 401.
        $response->assertStatus(401);

        // Assert that database doesn't have the condition.
        $this->assertDatabaseCount('conditions', 0);
        $this->assertDatabaseMissing('conditions', $condition);
    }

    /**
     * @param array $data
     * @return TestResponse
     */
    private function hitStoreConditionEndpoint(array $data): TestResponse
    {
        return $this
            ->jsonApi()
            ->withData($data)
            ->post(route('v1.conditions.store'));
    }

    /**
     * @return array
     */
    private function createFakeCondition(): array
    {
        $condition = Condition::factory()->make()->toArray();
        $expected_data = $this->prepareJsonAPIDataObject('conditions', $condition,null);
        return array($condition, $expected_data);
    }
}
