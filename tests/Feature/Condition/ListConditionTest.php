<?php

namespace Tests\Feature\Condition;

use App\Domain\Condition\Model\Condition;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use LaravelJsonApi\Testing\TestResponse;
use Tests\TestCase;

class ListConditionTest extends TestCase
{
    /** @test */
    public function an_authorized_user_can_list_conditions(): void
    {
        // At first, we will create and act as authorized member.
        $this->actingAsMember();

        // Then, we will create fake data of conditions.
        $conditions = $this->createFakeConditions(5);

        // At least, we will hit the list endpoint.
        $response = $this->hitListConditionsEndpoint();

        // Response should be 200.
        $response->assertStatus(200);

        // Assert we have fetched all conditions.
        $response->assertFetchedMany($conditions);
    }

    /** @test */
    public function a_guest_cannot_list_conditions(): void
    {
        // At first, we will create fake data of conditions.
        $this->createFakeConditions(5);

        // Then, we will hit the list endpoint.
        $response = $this->hitListConditionsEndpoint();

        // Response should be 401.
        $response->assertStatus(401);
    }

    /**
     * @return TestResponse
     */
    private function hitListConditionsEndpoint(): TestResponse
    {
        return $this
            ->jsonApi()
            ->expects('conditions')
            ->get(route('v1.conditions.index'));
    }

    /**
     * @param $count
     * @return Collection|Model
     */
    private function createFakeConditions($count): Collection|Model
    {
        return Condition::factory()->count($count)->create();
    }
}
