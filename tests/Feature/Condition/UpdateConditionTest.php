<?php

namespace Tests\Feature\Condition;

use App\Domain\Condition\Model\Condition;
use LaravelJsonApi\Testing\TestResponse;
use Tests\TestCase;

class UpdateConditionTest extends TestCase
{
    /** @test */
    public function an_authorized_user_can_update_condition(): void
    {
        // At first, we will create and act as authorized member.
        $this->actingAsMember();

        // Then, we will create a condition that will be updated later.
        $condition = Condition::factory()->create();

        // Then, we will make fake data of condition and hit update endpoint.
        $expected_data = $this->createFakeCondition($condition->id);

        // At least, we will hit the update endpoint.
        $response = $this->hitUpdateConditionEndpoint($expected_data, $condition->id);

        // Response should be 200.
        $response->assertStatus(200);

        // Assert that database has the updated condition.
        $condition_db = Condition::select(['id', 'criteria'])->whereId($condition->id)->first()->toArray();
        $this->assertEquals($condition_db, [
            'id' => $condition->id,
            'criteria' => $expected_data['attributes']['criteria']
        ]);
        $this->assertNotEquals($condition_db, [
            'id' => $condition->id,
            'criteria' => $condition->criteria
        ]);
        $this->assertDatabaseCount('conditions', 1);
    }

    /** @test */
    public function a_guest_cannot_update_condition(): void
    {
        // At first, we will create a condition that will be updated later.
        $condition = Condition::factory()->create();

        // Then, we will create fake data of condition and hit update endpoint.
        $expected_data = $this->createFakeCondition($condition->id);

        // At least, we will hit the update endpoint.
        $response = $this->hitUpdateConditionEndpoint($expected_data, $condition->id);

        // Response should be 401.
        $response->assertStatus(401);

        $condition_db = Condition::select(['id', 'criteria'])->whereId($condition->id)->first()->toArray();
        $this->assertNotEquals($condition_db, [
            'id' => $condition->id,
            'criteria' => $expected_data['attributes']['criteria']
        ]);
        $this->assertEquals($condition_db, [
            'id' => $condition->id,
            'criteria' => $condition->criteria
        ]);
    }

    /**
     * @param array $data
     * @param string $id
     * @return TestResponse
     */
    private function hitUpdateConditionEndpoint(array $data, string $id): TestResponse
    {
        return $this
            ->jsonApi()
            ->withData($data)
            ->patch(route('v1.conditions.update', $id));
    }

    /**
     * @param $id
     * @return array
     */
    private function createFakeCondition($id): array
    {
        $condition = Condition::factory()->make()->toArray();

        return $this->prepareJsonAPIDataObject('conditions', $condition, $id);
    }
}
