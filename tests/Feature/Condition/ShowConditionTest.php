<?php

namespace Tests\Feature\Condition;

use App\Domain\Condition\Model\Condition;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use LaravelJsonApi\Testing\TestResponse;
use Tests\TestCase;

class ShowConditionTest extends TestCase
{
    /** @test */
    public function a_guest_cannot_show_condition(): void
    {
        // At first, we will create a condition that will be shown.
        $condition = $this->createFakeCondition();

        // Then, we will hit show endpoint.
        $response = $this->hitShowConditionEndpoint($condition->id);

        // Response should be 401.
        $response->assertStatus(401);
    }

    /** @test */
    public function an_authorized_user_can_show_condition(): void
    {
        // At first, we will create and act as authorized member.
        $this->actingAsMember();

        // Then, we will create a condition that will be shown.
        $condition = $this->createFakeCondition();

        // At least, we will hit the show endpoint.
        $response = $this->hitShowConditionEndpoint($condition->id);

        // Response should be 200.
        $response->assertStatus(200);

        // assert that we have fetched one condition.
        $response->assertFetchedOne($condition);
    }

    /**
     * @param int $id
     * @return TestResponse
     */
    private function hitShowConditionEndpoint(int $id): TestResponse
    {
        return $this
            ->jsonApi()
            ->expects('conditions')
            ->get(route('v1.conditions.show', $id));
    }

    /**
     * @return Collection|Model
     */
    private function createFakeCondition(): Collection|Model
    {
        return Condition::factory()->create();
    }
}
