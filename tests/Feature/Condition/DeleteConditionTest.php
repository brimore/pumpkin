<?php

namespace Tests\Feature\Condition;

use App\Domain\AudienceList\Model\AudienceList;
use App\Domain\Condition\Model\Condition;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use LaravelJsonApi\Testing\TestResponse;
use Tests\TestCase;

class DeleteConditionTest extends TestCase
{
    /** @test */
    public function an_authorized_user_can_delete_condition(): void
    {
        // At first, we will create and act as authorized member.
        $this->actingAsMember();

        // Then, we will create fake condition.
        $condition = $this->createFakeCondition();

        // At least, we will hit delete endpoint .
        $response = $this->hitDeleteConditionEndpoint($condition->id);

        // Response should be 204.
        $response->assertNoContent();

        // Assert that database doesn't have the condition.
        $this->assertDatabaseCount('conditions', 0);
        $this->assertDatabaseMissing('conditions', $condition->toArray());
    }

    /** @test */
    public function a_guest_cannot_delete_condition(): void
    {
        // At first, we will create fake condition.
        $condition = $this->createFakeCondition();

        // Then, we will hit delete endpoint .
        $response = $this->hitDeleteConditionEndpoint($condition->id);

        // Response should be 401.
        $response->assertStatus(401);

        // Assert that database has the condition.
        $this->assertDatabaseCount('conditions', 1);
        $this->assertDatabaseHas('conditions', ['id' => $condition->id]);
    }

    /**
     * @return Collection|Model
     */
    private function createFakeCondition(): Collection|Model
    {
        return Condition::factory()->create();
    }

    /**
     * @param int $id
     * @return TestResponse
     */
    private function hitDeleteConditionEndpoint(int $id): TestResponse
    {
        return $this
            ->jsonApi()
            ->delete(route('v1.conditions.destroy', $id));
    }

}
