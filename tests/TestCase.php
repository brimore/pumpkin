<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use LaravelJsonApi\Testing\MakesJsonApiRequests;
use App\Models\User;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\RefreshDatabase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use MakesJsonApiRequests;
    use RefreshDatabase;

    /**
     * @return User
     */
    protected function actingAsMember(): User
    {
        /* @var User $user */
        $user = User::factory()->create();
        Passport::actingAs($user);

        return $user;
    }

    /**
     * @param string $type
     * @param array $attributes
     * @param string|null $id
     *
     * @return array
     */
    protected function prepareJsonAPIDataObject(string $type, array $attributes, ?string $id = null, ?string $uri = null): array
    {
        $mapped_data = compact('type', 'attributes');
        if ($id) {
            $mapped_data['id'] = (string)$id;
        }
        if ($uri) {
            $mapped_data['relationships'] = [
                'members' => [
                    'links' => [
                        'related' => "{$uri}/members",
                        'self' => "{$uri}/relationships/members"
                    ]
                ],
                'offers' => [
                    'links' => [
                        'related' => "{$uri}/offers",
                        'self' => "{$uri}/relationships/offers"
                    ]
                ]
            ];
            $mapped_data['links'] = [
                'self' => $uri,
            ];
        }
        return $mapped_data;
    }
}
